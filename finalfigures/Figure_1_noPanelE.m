%
% plot figure 1
% trial details and behavioral performance
%

close all
clearvars

addpath('../matlab/');
COMP = 2;
Step0_setup;

snamef = '_Fig_1.png';

load Behavior.mat
[nsub, nconds] = size(Behavior.Correct);

% load example sentence
load adj_1_beautiful_audio.mat
load adj_1_beautiful_video.mat
% segmented text
load Segmented_adj.mat
Text = SegmentedText{1};
clear SegmentedText
% load stimulus analysis results
load StimAnalysisData.mat


% load features
cfg = [];
AllFeatures = createFeatures_v3(cfg,DIR);

AudFeat = {'aud_env','aud_slope','aud_pitch'};
VisFeat = {'lip_area','lip_slope','lip_width'};
AllLabels = cat(2,AudFeat, VisFeat);
Features = sort_features(AllFeatures,AllLabels);


%% parameters
% plot positions
nrowsp = 7;
ncolsp = 7;
pos.img = [1,2,3];
pos.lip = [8,9,10,15,16,17];
pos.env = [22,23,24,29,30,31];
pos.sen = [36,37,38,43,44,45];
pos.per = [4,5,11,12,18,19,25,26];
pos.pow = [6,7,13,14,20,21,27,28];
pos.lco = [32,33,39,40,46,47];
pos.rco = [34,35,41,42,48,49];
%pos.cmi = [30,36,42];
pos.txt = [33,34,35,40,41,42,47,48,49];

% get colors from function
Colors = get_ColorPalette;

% plot style 
style = figstyle;


% audio preproc.
fs = 22050; % sound sample rate
% cut sound to exactly 7 s length
sound = sound(1:7*fs,1);
sound = sound-mean(sound);
L = length(sound);

% load stimuli
stim = Features.ChosenFeat{1}{1}';
stim = stim-mean(stim);
fsstim = 50; %fs/(L/Ls);

% cut stimuli to exactly 7 s length
stim = stim(1:round(7*fsstim),:);
Ls = length(stim);
t = [1:L]./fs;
dt = [1:L/Ls:L]./fs;

% scale
sound = sound./max(sound);
stim = stim./max(stim);

% sort auditory features
env = stim(:,1);
envslope = stim(:,2);
pitch = stim(:,3);
% sort visual features
lips = stim(:,4);
lipslope = stim(:,5);
distx = stim(:,6);

% adjust magnitude and offset for visual purpose
env = env .* 1.2 + 0.24; 
pitch = pitch - 2.0;
distx = distx - 2.0;

% video preproc
fps = 25;
% handpicked for this specific sentence
lipsleft = 78; % small lips
lipsmid = 196; % big lips
lipsright = 282; % medium lips
nframes = 3;
marea = [565 430 112 200]; % x y height width

idxlips = [lipsleft, lipsmid, lipsright];
idxframe = round([lipsleft, lipsmid, lipsright]./(fsstim/fps));
mouth = vidFrames(marea(2):marea(2)+marea(3)-1,marea(1):marea(1)+marea(4)-1,:,idxframe);


%% plot sentence example
%figure('Position',[175 80, 1600 889]);
figure('Units','inches','Position',[8 5 7.2 4.0]);

% video frames ------------------------------------------------------------
for iframe = 1:nframes
    HH(iframe) = subplot(nrowsp,ncolsp,pos.img(iframe));
    image(sq(mouth(:,:,:,iframe)));
    pbaspect([16 9 1]);
    xticks([]); yticks([]);
    xticklabels([]); yticklabels([]);
    HH(iframe).Position = HH(iframe).Position + [-0.02, 0.02, 0, 0];
end


drawnow



% lip features ------------------------------------------------------------
HH(4) = subplot(nrowsp,ncolsp,pos.lip);
set(gca,'ColorOrder',Colors.VisSingle,'NextPlot','replacechildren');
hold on
plot(dt,lips,'-','LineWidth',style.LineWidth);
plot(dt,lipslope,'-','LineWidth',style.LineWidth);
plot(dt,distx,'-','LineWidth',style.LineWidth);
plot((idxlips-1)./fsstim, lips(idxlips),'k.','MarkerSize',style.MarkerSize);
xlim([0,t(end)]);
ylim([-3,2]);
HH(4).LineWidth = style.LineWidth;
ckoffsetAxes(HH(4));

% draw annotations from video frames to points
annx = [0.75,3.4,6; (idxlips-1)./fsstim];
anny = [2.55,2.55,2.55; lips(idxlips)'];
plot(annx,anny,'k:','LineWidth',style.LineWidth);
HH(4).Clipping = 'off'; % plot lines outside of axis boundaries

% text feature labels on top
text(0.0,-3.6,'LipFeat:', 'FontSize',style.FontSize,'FontWeight','bold');
text(1.5,-3.6,'lip area','Color',Colors.VisSingle(1,:),'FontSize',style.FontSize,'FontWeight','bold');
text(3.0,-3.6,'lip slope','Color',Colors.VisSingle(2,:),'FontSize',style.FontSize,'FontWeight','bold');
text(4.5,-3.6,'lip width','Color',Colors.VisSingle(3,:),'FontSize',style.FontSize,'FontWeight','bold');

% axes properties
HH(4).Position = HH(4).Position + [-0.02,-0.0,0,0.02];
ax = gca;
ax.XAxis.Visible = 'off';
yticks([-1,0,1]);

a = get(gca,'YTickLabel');
set(gca,'YTickLabel',a,'FontSize',style.FontSize,'FontWeight','bold');

drawnow



% wavefile and auditory features ------------------------------------------
HH(5) = subplot(nrowsp,ncolsp,pos.env);
set(gca,'ColorOrder',Colors.AudSingle,'NextPlot','replacechildren');
hold on
plot(t,sound,'k-','LineWidth',0.5,'Color',[0.8,0.8,0.8]);
plot(dt,env,'-','LineWidth',style.LineWidth);
plot(dt,envslope,'-','LineWidth',style.LineWidth);
plot(dt,pitch,'-','LineWidth',style.LineWidth);
hold off
xlim([0,t(end)]);
ylim([-3,2]);
xlabel('Time (s)', 'FontWeight','bold');
HH(5).LineWidth = style.LineWidth;
ckoffsetAxes(HH(5));

% text feature labels on top
text(0.0,2,'AudFeat:', 'FontSize',style.FontSize,'FontWeight','bold');
text(1.5,2,'aud env','Color',Colors.AudSingle(1,:),'FontSize',style.FontSize,'FontWeight','bold');
text(3.0,2,'aud slope','Color',Colors.AudSingle(2,:),'FontSize',style.FontSize,'FontWeight','bold');
text(4.5,2,'aud pitch','Color',Colors.AudSingle(3,:),'FontSize',style.FontSize,'FontWeight','bold');

% axes properties
HH(5).Position = HH(5).Position + [-0.02,-0.07,0,0.02];
yticks([-1,0,1]);

a = get(gca,'YTickLabel');
set(gca,'YTickLabel',a,'FontSize',style.FontSize,'FontWeight','bold');

drawnow



% write text aligned to time on x axis ------------------------------------
sentence = erase(Text.sentence, ',');
sentence = split(sentence);
Timings =  Text.Word_timing;
% instead of creating a new axis, write just below HH(5)
yoffset = -8.0;
% interleave every second word
ix = Timings(:,1);
iy = mod([1:length(sentence)],3) * 0.5;
tt = text(ix,iy+yoffset,sentence, 'FontSize',style.TextSize,'FontAngle','italic','FontWeight','bold');

drawnow



% plot performance --------------------------------------------------------
HH(7) = subplot(nrowsp,ncolsp,pos.per);
BehaviorCorrect = Behavior.Correct .* 100;
boxplot(BehaviorCorrect,'Color',[Colors.AcondAfeat;Colors.VcondVfeat], ...
    'widths', 0.2, 'symbol', '', 'notch', 'off');
xticklabels([]);
hold on

jitter = 0.1 .* rand(nsub,nconds);
jitter = jitter .* repmat([1,-1],nsub,1);
X = repmat([1:nconds],nsub,1);
X = X + jitter + [0.15, -0.15];
% individual lines
plot(X',BehaviorCorrect','-','LineWidth',0.3,'Color',[0.7,0.7,0.7]);
% individual data points
plot(X(:,1),BehaviorCorrect(:,1),'.','MarkerSize',style.MarkerSize,'Color',Colors.AcondAfeat);
plot(X(:,2),BehaviorCorrect(:,2),'.','MarkerSize',style.MarkerSize,'Color',Colors.VcondVfeat);


xticks([1,2]);
xticklabels([]);
%xlim([0.75,2.25]);
set(gca,'XTickLabel',{'A-only','V-only'},'FontSize',style.FontSize,'FontWeight','bold');
ylabel('Percent Correct');

box off
HH(7).LineWidth = style.LineWidth;
ckoffsetAxes(HH(7));
% position shorter and up aligned
HH(7).Position = HH(7).Position + [0.02,0.06,-0.04,-0.04];
HH(7).XLim = [0.75,2.25];

drawnow



% log power spectra -------------------------------------------------------
% feature names
AudNames = cellfun(@(x) replace(x,'_',' '),AudFeat,'UniformOutput',false);
VisNames = cellfun(@(x) replace(x,'_',' '),VisFeat,'UniformOutput',false);
Aud_indx = [1:3]; Vis_indx = [4:6];


HH(8) = subplot(nrowsp,ncolsp,pos.pow(1:2:end));
set(gca,'ColorOrder',Colors.AudSingle,'NextPlot','replacechildren');
plot(Power.freq, Power.spectra(:,Aud_indx),'LineWidth',style.LineWidth);
hold on
plot(Power.freq,Power.noise,'--','Color',[1,1,1]*0.5,'LineWidth',0.5);
% text annotation 1/f spectrum
text(3,-0.5,'1/f spectrum','Color',[1,1,1]*0.5,'FontSize',style.TextSize);
xlim([0.5,8]);
ylim([-6,1]);
line_start_end = [0.015, 0.3];
line_text_step = 0.05;
[LEGH,OBJH,~,~] = legend(AudNames,'Box','off','Location','southeast','FontSize',style.LegendSize);
legendshrink(HH(8).Position(1:2), LEGH, OBJH, line_start_end, line_text_step);
LEGH.Position = LEGH.Position + [0.06 0.01 0 0];
box off


% axes font size, line thickness
position = HH(8).Position;
position = [position(1)+0.01, HH(7).Position(2), position(3), HH(7).Position(4)-0.01];
xlabel('Frequency (Hz)');
ylabel('Log Power');
HH(8).Position = position;

ax = gca;
ax.XAxis.FontSize = style.FontSize;
ax.XAxis.FontWeight = 'bold';
ax.XAxis.LineWidth = style.LineWidth;
ax.YAxis.FontSize = style.FontSize;
ax.YAxis.FontWeight = 'bold';
ax.YAxis.LineWidth = style.LineWidth;


HH(9) = subplot(nrowsp,ncolsp,pos.pow(2:2:end));
set(gca,'ColorOrder',Colors.VisSingle,'NextPlot','replacechildren');
plot(Power.freq, Power.spectra(:,Vis_indx),'LineWidth',style.LineWidth);
hold on
plot(Power.freq,Power.noise,'--','Color',[1,1,1]*0.5,'LineWidth',0.5);
xlim([0.5,8]);
ylim([-6,1]);
[LEGH,OBJH,~,~] = legend(VisNames,'Box','off','Location','southwest','FontSize',style.LegendSize);
legendshrink(HH(9).Position(1:2), LEGH, OBJH, line_start_end, line_text_step);
LEGH.Position = LEGH.Position + [0 0.01 0 0];
box off

% axes font size, line thickness
position = HH(9).Position;
position = [position(1)+0.01, HH(7).Position(2), position(3), HH(7).Position(4)-0.01];
HH(9).Position = position;

ax = gca;
ax.XAxis.FontSize = style.FontSize;
ax.XAxis.FontWeight = 'bold';
ax.XAxis.LineWidth = style.LineWidth;
ax.YAxis.FontSize = style.FontSize;
ax.YAxis.FontWeight = 'bold';
ax.YAxis.LineWidth = style.LineWidth;

drawnow



% coherence matrix --------------------------------------------------------
stimlabels = cellfun(@(x) replace(x,'_',' '),AllLabels,'UniformOutput',false);

HH(10) = subplot(nrowsp,ncolsp,pos.lco,'visible','off');
imagesc(Correlation.data{1});
caxis([0,1]);
colormap(hot(256));

position = HH(10).Position;
ch = colorbar;
ch.Ticks = [0:0.2:1];
ch.TickLength = 0;
ch.Label.FontSize = style.FontSize;

axis square
box on

xticks([1:numel(stimlabels)]);
yticks([1:numel(stimlabels)]);
xticklabels(stimlabels);
yticklabels(stimlabels);
xtickangle(45);

% remove tick marks, axes properties
HH(10).Position = position + [0.06,-0.01,-0.04,-0.04]; % smaller
header = sprintf('Average %.1g-%.1g Hz Coherence',Correlation.range(1,:));
title(header,'FontSize',style.TextSize);

ax = gca;
ax.XAxis.TickLength = [0,0];
ax.XAxis.FontSize = style.TextSize;
ax.XAxis.FontWeight = 'bold';
ax.XAxis.LineWidth = style.LineWidth;
ax.YAxis.TickLength = [0,0];
ax.YAxis.FontSize = style.TextSize;
ax.YAxis.FontWeight = 'bold';
ax.YAxis.LineWidth = style.LineWidth;
ch.FontSize = style.FontSize;
ch.FontWeight = 'bold';
ch.LineWidth = style.LineWidth;
ch.Position = ch.Position + [-0.01, 0, 0, 0];

drawnow



% coherence matrix right --------------------------------------------------
HH(11) = subplot(nrowsp,ncolsp,pos.rco);
imagesc(Correlation.data{2});
caxis([0,1]);
colormap(hot(256));

axis square
box on

xticks([1:numel(stimlabels)]);
xticklabels(stimlabels);
xtickangle(45);
yticklabels([]);

% remove tick marks, axes properties
HH(11).Position = HH(11).Position + [0.06,-0.01,-0.04,-0.04]; % smaller
header = sprintf('Average %.1g-%.1g Hz Coherence',Correlation.range(2,:));
title(header,'FontSize',style.TextSize);

ax = gca;
ax.XAxis.TickLength = [0,0];
ax.XAxis.FontSize = style.TextSize;
ax.XAxis.FontWeight = 'bold';
ax.XAxis.LineWidth = style.LineWidth;
ax.YAxis.TickLength = [0,0];
ax.YAxis.FontSize = style.TextSize;
ax.YAxis.FontWeight = 'bold';
ax.YAxis.LineWidth = style.LineWidth;

drawnow




% insert text on invisible axis -------------------------------------------
% invisible axis
HH(13) = axes('pos',[0.45, 0.1, 0.3, 0.4],'visible','off','Tag','explanation');

% text parameters
linex = 0.08;
linelen = [0, 0.34];
ticklen = 0.02;
textx = 0.04;
equax = 0.85;
equay = 0.06;

% plot bracket lines
hold on
plot([linex,linex],linelen+0.01,'k-','LineWidth',style.LineWidth);
plot([linex+ticklen,linex],[0.01,0.01],'k-','LineWidth',style.LineWidth);
plot([linex+ticklen,linex],[0.35,0.35],'k-','LineWidth',style.LineWidth);
plot([linex,linex],linelen+0.37,'k-','LineWidth',style.LineWidth);
plot([linex+ticklen,linex],[0.37,0.37],'k-','LineWidth',style.LineWidth);
plot([linex+ticklen,linex],[0.71,0.71],'k-','LineWidth',style.LineWidth);

% text feat group names
text(textx,0.37+0.17,'AudFeat','FontSize',style.FontSize,'FontWeight','bold',...
    'HorizontalAlignment','center','Rotation',90);
text(textx,0.01+0.17,'LipFeat','FontSize',style.FontSize,'FontWeight','bold',...
    'HorizontalAlignment','center','Rotation',90);


% axes properties
xlim([0,1]);
ylim([0,1]);

HH(13).Visible = 'off';

drawnow



% panel labels  -----------------------------------------------------------
HH(14) = axes('pos',[0.05, 0.05, 0.9, 0.9],'visible','off','Tag','panellabel');

text(0.015,1.0,'A','FontSize',style.PanLabelSize,'FontWeight','bold');
text(0.44,1.0,'B','FontSize',style.PanLabelSize,'FontWeight','bold');
text(0.68,1.0,'C','FontSize',style.PanLabelSize,'FontWeight','bold');

text(0.45,0.45,'D','FontSize',style.PanLabelSize,'FontWeight','bold');


% save ---------------
print('-dpng',style.resolution,snamef);

snamesvg = '_Fig_1.pdf';
print('-dpdf','-bestfit','-r1200',snamesvg);


