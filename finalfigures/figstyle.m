function style = figstyle()
% return parameters such as line width and font size for consistency across
% final figures

style.resolution = '-r300'; % 300 dpi
style.resolution_small = '-r200';

style.MarkerSize = 10;
%style.BardotSize = 5;
style.BardotSize = 8;
%style.BardotThickness = 2.5;
style.BardotThickness = 4;
style.LineWidth = 1;
style.SigStarSize = 6;
style.TickLabelSize = 5;

style.FontSize = 6;
style.TextSize = 5;

style.HeaderSize = 8;
style.PanLabelSize = 14;

style.LegendSize = 4;

end
