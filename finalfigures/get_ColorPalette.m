function ColorPalettes = get_ColorPalette()
%
% ColorPalettes = get_ColorPalette()
% create color palettes for final figures, so that the colors are
% consistent across figure
%

% Features color
ColorPalettes.AudSingle = [hex2rgb('c03221'); hex2rgb('545e91'); hex2rgb('7aa190')];
ColorPalettes.VisSingle = [hex2rgb('DE541E'); hex2rgb('D9BBF9'); hex2rgb('7C6A0A')];
% grouped Features color
ColorPalettes.AudGroup = hex2rgb('942123');
ColorPalettes.VisGroup = hex2rgb('54B3B3');

% feature by feature color palette
ColorPalettes.Coherence = [hex2rgb('ce6f38'); hex2rgb('98473e'); hex2rgb('771b29'); ...
                hex2rgb('7a94b0'); hex2rgb('1882a0'); hex2rgb('325680'); ...
                hex2rgb('8dc59c'); hex2rgb('40A068'); hex2rgb('1f7b2f')];

% condition color palette
ColorPalettes.AcondAfeat = hex2rgb('72A98F');
ColorPalettes.AcondVfeat = hex2rgb('dcde9b');
ColorPalettes.VcondAfeat = hex2rgb('73ada0');
ColorPalettes.VcondVfeat = hex2rgb('6D326D');

% meg color (if needed)
ColorPalettes.MEG = hex2rgb('a473d0');

% statistics color palette
ColorPalettes.NonSig = hex2rgb('B3B3B3');

end
