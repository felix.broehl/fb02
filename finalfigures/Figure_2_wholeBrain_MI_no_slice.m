%
% take env and lip tracking data, and create a figure with ROIs and the
% contour of the 95th percentile 
%

%close all
clearvars

addpath('C:\Users\fbroehl\Documents\FB02\matlab');

COMP = 2;
Step0_setup;

% get subject data
load Speech_Entrainment.mat
load sourceatlas_grid.mat
load('ROIVOXELS.mat','roivoxins');
nvox = length(roivoxins);

[ROI,~,~,~] = pROI_brainnetome;


% choose which data to plot
GroupData = MIdiff;

% get template MRI
mri = ft_read_mri(templateT1);
mri = ft_convert_units(mri, 'cm');

% brain structure data
path_to_headmodel = 'Y:\Matlab\fieldtrip-20190905\template\headmodel\standard_mri.mat';
path_to_atlas = 'Y:\Matlab\fieldtrip-20190905\template\atlas\brainnetome\BNA_MPM_thr25_1.25mm.nii';



% get colors from function
Colors = get_ColorPalette;
condcolors = [Colors.AcondAfeat; Colors.VcondVfeat];

% plot style 
style = figstyle;


% colormap
cmap = hot(256);
R1 = ones(1,85);
R2 = ones(1,85);
R3 = ones(1,85);
G1 = linspace(1,0,85);
G2 = linspace(0,1,85);
G3 = ones(1,85);
B1 = linspace(1,0,85);
B2 = zeros(1,85);
B3 = linspace(0,1,85);
wmap = [R1,R2,R3;G1,G2,G3;B1,B2,B3]';


%% atlas
load(path_to_headmodel)
atlas1 = ft_read_atlas(path_to_atlas);
atlaslabel = atlas1.tissuelabel;

% interpolate atlas
cfg = [];
cfg.interpmethod = 'linear'; % was 'nearest' 'linear'
cfg.parameter = 'tissue';
atlas2 = ft_sourceinterpolate(cfg, atlas1, mri);



%% plot ROIs on brain surface

roistim = [1,2];
roifreq = [6,6]; % 6 is 0.5 - 8 Hz band

cslicerange = [80,72];

% save figure name to load and delete
Fignames = [];


azi = [-90,0,90];
elev = [0,0,0];
lightdir = {'left','left','left'};

rmap = [];
rmap(1,:) = [1,1,1];
rmap(2,:) = Colors.AcondAfeat;
rmap(3,:) = Colors.VcondVfeat;


cfg = [];
cfg.interpmethod = 'linear'; % was 'nearest' 'linear'
cfg.parameter = 'fun';
sourcemodel.fun = zeros(sourcemodel.dim);

intrp = ft_sourceinterpolate(cfg,sourcemodel, mri);

roilabel = [-1, 1];
for icond = 1:2
    % get roidata
    funroi = zeros(prod(intrp.dim),1);
    jj = find(ismember(atlas2.tissue,ROI.(ARG.blockname{icond}).label));
    funroi(jj) = 1;

    % smooth and threshold again to fuse subrois
    funroi = reshape(funroi,intrp.dim);
    funroi = smooth3(funroi);
    funroi(funroi<=0.1) = 0;
    funroi(funroi>0.1) = icond;
    funroi = funroi([181:-1:1],:,:); % FLIP
    % interpolate sourcemodel
    intrp.fun = intrp.fun + funroi;
end

% dim 1 is left right

cfg = [];
% cfg.opacitymap = 'rampup';
cfg.method = 'surface';     % 'surface'
cfg.funparameter = 'fun';
%cfg.maskparameter = 'fun';
cfg.funcolormap = rmap;
cfg.funcolorlim = [0, 2];
cfg.projmethod = 'project';
cfg.projvec = [0 2.5];
cfg.projcomb = 'max';
cfg.renderer = 'zbuffer';
cfg.colorbar = 'no';
cfg.camlight = 'no';

% plot all views
for ii = 1:length(azi)
    ft_sourceplot(cfg,intrp);

    view(azi(ii),elev(ii));
    material dull % diffuse reflections

    h = camlight(lightdir{ii});
    h.Color = h.Color * 0.8;

    drawnow
    
    
    snamef = sprintf('_Fig_ROI_%d.png',ii);
    print('-dpng',snamef);
    figname = sprintf('_Fig_ROI_%d',ii);
    savefig(figname);
    Fignames = [Fignames; {figname}];
    
    close 
    
end


%%
% now plot just one ROI at a time on surface as an inlay for other figures
for icond = 1:2
    % color scheme
    condmap = [1,1,1; condcolors(icond,:)];
    
    % get roidata
    funroi = zeros(prod(intrp.dim),1);
    jj = find(ismember(atlas2.tissue,ROI.(ARG.blockname{icond}).label));
    funroi(jj) = 1;

    % smooth and threshold again to fuse subrois
    funroi = reshape(funroi,intrp.dim);
    funroi = smooth3(funroi);
    funroi(funroi<=0.1) = 0;
    funroi(funroi>0.1) = 1;
    funroi = funroi([181:-1:1],:,:); % FLIP
    % interpolate sourcemodel
    intrp.fun = funroi;


    % dim 1 is left right

    cfg = [];
    % cfg.opacitymap = 'rampup';
    cfg.method = 'surface';     % 'surface'
    cfg.funparameter = 'fun';
    cfg.funcolormap = condmap;
    cfg.funcolorlim = [0, 1];
    cfg.projmethod = 'project';
    cfg.projvec = [0 2.5];
    cfg.projcomb = 'max';
    cfg.renderer = 'zbuffer';
    cfg.colorbar = 'no';
    cfg.camlight = 'no';
    


    % plot all views
    ft_sourceplot(cfg,intrp);

    view(azi(icond),elev(icond));
    material dull % diffuse reflections

    h = camlight(lightdir{icond});
    h.Color = h.Color * 0.8;

    drawnow
    
    
    snamef = sprintf('_Fig_ROI_inlay_%d.png',icond);
    print('-dpng',snamef);
    figname = sprintf('_Fig_ROI_inlay_%d',icond);
    savefig(figname);
    
    close 
    
end




    
%% surface plot mi data

azi = [75, 35];
elev = [15, 25];

for icond = 1:ARG.ncond
    % interpolate MI data
    % env in A trials, lip in V trials
    istim = roistim(icond);
    ifreq = roifreq(icond);
    cfg = [];
    cfg.interpmethod = 'linear'; % was 'nearest' 'linear'
    cfg.parameter = 'fun';
    sourcemodel.fun(roivoxins) = sq(GroupData(icond,ifreq,istim,:));
    intrp2 = ft_sourceinterpolate(cfg,sourcemodel, mri);
    intrp2.fun(isnan(intrp2.fun)) = 0;
    intrp2.fun(intrp2.fun<0) = 0;

    cfg = [];
    cfg.surffile = 'surface_white_both.mat';
    cfg.method = 'surface';     % 'surface'
    cfg.funparameter = 'fun';
    %cfg.maskparameter = 'fun';
    cfg.funcolormap = flipud(cmap);
    %cfg.funcoloorlim = 'zeromax';
    cfg.projmethod = 'project';
    cfg.projvec = [0 10];
    cfg.projcomb = 'max';
    cfg.renderer = 'zbuffer';
    cfg.camlight = 'no';
    cfg.colorbar = 'no';
    
    % view from right
    ft_sourceplot(cfg,intrp2);
    view(azi(icond),elev(icond)); 
    material dull % diffuse reflections
    h = camlight('right');
    h.Color = h.Color * 0.8;
    colorbar('Location','EastOutside');
    drawnow
    
    snamef = sprintf('_Fig_2_%s_surface_right.png',ARG.blockname{icond});
    print('-dpng',snamef);
    figname = sprintf('_Fig_2_%d_r',icond);
    savefig(figname);
    Fignames = [Fignames; {figname}];
    
    % view from left
    ft_sourceplot(cfg,intrp2);
    view(-azi(icond),elev(icond)); 
    material dull % diffuse reflections
    h = camlight('left');
    h.Color = h.Color * 0.8;
    colorbar('Location','WestOutside');
    drawnow
    
    snamef = sprintf('_Fig_2_%s_surface_left.png',ARG.blockname{icond});
    print('-dpng',snamef);
    figname = sprintf('_Fig_2_%d_l',icond);
    savefig(figname);
    Fignames = [Fignames; {figname}];
    
end


% sort fignames how they appear in the final figure
Fignames = Fignames([4:7,1:3]);


%% close all figures and create on from files

close all


nfigs = numel(Fignames);
sizefac = 1.0;
sndrowadd = 0.08;

% sub figure panels
subpan = [1,2,7,8,3,6,9];

% create figure and set all subplots
figure('Units','inches','Position',[8 5 5.0 2.8]); % one-and-a-half columns
for ifig = 1:nfigs
    Isubpan = subpan(ifig);
    hh(ifig) = cksubplot(3,3,Isubpan,1.05);
    if ifig < 5
        hh(ifig).Position = hh(ifig).Position - [0.1,0,0,0];
    end
end

cmap = flipud(hot(256));
colormap(cmap);
for ifig = 1:nfigs
    % load saved figures and copy all children
    fh = openfig(Fignames{ifig});
    copyobj(allchild(get(fh,'CurrentAxes')),hh(ifig));
    
    % copy parameters
    caxis(hh(ifig),fh.CurrentAxes.CLim);
    view(hh(ifig),fh.CurrentAxes.View);
    close(fh);
    
    drawnow
end

% top row
axes(hh(1));
axis off; axis vis3d; axis image;
pos = hh(1).Position;
hh(1).Position = pos + [0, -pos(3)*0.8, pos(3)*sizefac, pos(4)*sizefac];
rightmargin = sum(hh(1).Position([1,3]));


axes(hh(2));
axis off; axis vis3d; axis image;
pos = hh(2).Position;
hh(2).Position = pos + [0, -pos(3)*0.8, pos(3)*sizefac, pos(4)*sizefac];


% bottom row
axes(hh(3));
axis off; axis vis3d; axis image;
pos = hh(3).Position;
hh(3).Position = pos + [-0.055, 0, pos(3)*sizefac+sndrowadd, pos(4)*sizefac+sndrowadd];
hh(3).CLim = [0, 0.13];


axes(hh(4));
axis off; axis vis3d; axis image;
pos = hh(4).Position;
hh(4).Position = pos + [-0.03, 0, pos(3)*sizefac+sndrowadd, pos(4)*sizefac+sndrowadd];
hh(4).CLim = [0, 0.13];



% Panel C - ROIs on surface
axes(hh(5));
colormap(hh(5),rmap);
axis off; axis vis3d; axis image;
pos = hh(5).Position;
hh(5).Position = pos + [0, -0.02, 0, 0];

axes(hh(6));
colormap(hh(6),rmap);
axis off; axis vis3d; axis image;

axes(hh(7));
colormap(hh(7),rmap);
axis off; axis vis3d; axis image;
pos = hh(7).Position;
hh(7).Position = pos + [0,  0.02, 0, 0];


% insert colorbars for panel A and B
cbaraxt = axes('pos',[0.325,0.48,0.12,0.2],'visible','off');
colormap(cbaraxt,cmap);
ch(1) = colorbar('south');
ch(1).TickLabels = {'0','','0.25'};
ch(1).FontSize = style.TextSize;
ch(1).FontWeight = 'bold';

cbaraxb = axes('pos',[0.325,0.28,0.12,0.2],'visible','off');
colormap(cbaraxb,cmap);
ch(2) = colorbar('north');
ch(2).TickLabels = {'0','','0.13'};
ch(2).FontSize = style.TextSize;
ch(2).FontWeight = 'bold';



% panel label
panax = axes('pos',[0,0.05,0.9,0.9],'visible','off');

% add panel labels
text(0.035,0.95,'A','FontSize',style.PanLabelSize,...
    'FontWeight','bold','HorizontalAlignment','center');
text(0.035, 0.45,'B','FontSize',style.PanLabelSize,...
    'FontWeight','bold','HorizontalAlignment','center');
text(0.8,0.95,'C','FontSize',style.PanLabelSize,...
    'FontWeight','bold','HorizontalAlignment','center');

% add colorbar unit label
text(0.43,0.48,'MI','FontSize',style.FontSize,'FontWeight','bold',...
    'HorizontalAlignment','center');


% save figure
snamef = '_Fig_2.png';
print('-dpng',style.resolution,snamef);

snamepdf = '_Fig_2.pdf';
print('-dpdf','-r1200',snamepdf);



return




%% find 3D perimeter of ROIs

% DEPRECATED CODE
% but was intereseting at some point...

% whether animate mri
animate = 0;

roistim = [1,2];
roifreq = [1,3];

% MRI data to RGB image matrix
rgbmri = repmat(mri.anatomy,[1,1,1,3]);
rgbmri = rot90(rgbmri);

for icond = 1:ARG.ncond
    % interpolate sourcemodel
    cfg = [];
    cfg.interpmethod = 'linear'; % was 'nearest' 'linear'
    cfg.parameter = 'fun';
    sourcemodel.fun = zeros(size(sourcemodel.pos,1),1);
    intrp = ft_sourceinterpolate(cfg, sourcemodel, mri);
    
    % interpolate MI data
    % env in A trials, lip in V trials
    istim = roistim(icond);
    ifreq = roifreq(icond);
    cfg = [];
    cfg.interpmethod = 'linear'; % was 'nearest' 'linear'
    cfg.parameter = 'fun';
    sourcemodel.fun(roivoxins) = sq(GroupData(icond,ifreq,istim,:));
    intrp2 = ft_sourceinterpolate(cfg,sourcemodel, mri);
    intrp2.fun(isnan(intrp2.fun)) = 0;


    % set roi labels 
    funroi = zeros(prod(intrp.dim),1);
    jj = find(ismember(atlas2.tissue,ROI.(ARG.blockname{icond}).all));
    funroi(jj) = 1;

    % smooth and threshold again to fuse subrois
    funroi = smooth3(reshape(funroi,intrp.dim));
    funroi(funroi>0.1) = 1;
    funroi = rot90(funroi);

    % find connected components, discard smaller ones
    nslices = size(funroi,3);
    for ii = 1:nslices
        funslice = sq(funroi(:,:,ii));
        CC = bwconncomp(funslice,8);
        % take only the largest two components
        if CC.NumObjects > 2
            [~,ccidx] = sort(cellfun(@length,CC.PixelIdxList),'descend');
            CC.PixelIdxList = CC.PixelIdxList(ccidx(1:2));
            CC.NumObjects = numel(CC.PixelIdxList);
        end
        stats{ii} = regionprops(CC,'ConvexHull');
    end
    

    % get MI data ----
    midata = rot90(intrp2.fun);
    shape = size(midata);
    % scale data
    maxMI = max(midata(:)) - min(midata(:));
    minMI = min(midata(:));
    midata = floor((midata - minMI) ./ (maxMI - minMI) .* 255);
    midata = cmap(midata+1,:);
    midata = reshape(midata,[shape,3]);
    opacmap = mean(midata,4);
    
    
    % plot data as animation
    if animate == 1
        figure
        nframes = size(intrp2.fun,3);
        for iframe = 1:nframes
            image(sq(rgbmri(:,:,iframe,:))); 
            image('CData',sq(midata(:,:,iframe,:)),'AlphaData',sq(opacmap(:,:,iframe)));
            hold on
            if ~isempty(stats{iframe})
                a = stats{iframe}(1).ConvexHull; b = stats{iframe}(2).ConvexHull;
                plot(round(a(:,1)),round(a(:,2)),'-','LineWidth',1,'Color',[0,0,1]);
                plot(round(b(:,1)),round(b(:,2)),'-','LineWidth',1,'Color',[0,0,1]);
            end
            pause(0.1);
        end
        close
    end
    
    
    % test single slice
    ii = 80;
    figure
    % image mri and mi data
    image(sq(rgbmri(:,:,ii,:))); 
    image('CData',sq(midata(:,:,ii,:)),'AlphaData',sq(opacmap(:,:,ii)));
    % plot roi convex hull
    hold on
    a = stats{ii}(1).ConvexHull; b = stats{ii}(2).ConvexHull;
    plot(round(a(:,1)),round(a(:,2)),'-','LineWidth',1.5,'Color',[1,0,1]);
    plot(round(b(:,1)),round(b(:,2)),'-','LineWidth',1.5,'Color',[1,0,1]);
    
    axis image
    axis off
    
    drawnow
    
    
end



%% surface ROI
azi = [75, 0];
elev = [15, 25];

cmap = [];
cmap(1,:) = [1,1,1]*0.7;
cmap(2,:) = [1,0,0];
cmap(3,:) = [0,0,1];

% MRI DATA
istim = roistim(icond);
ifreq = roifreq(icond);

cfg = [];
cfg.interpmethod = 'linear'; % was 'nearest' 'linear'
cfg.parameter = 'fun';
sourcemodel.fun = zeros(sourcemodel.dim);

intrp = ft_sourceinterpolate(cfg,sourcemodel, mri);


for icond = 1:2
  
  
  % get roidata
  funroi = zeros(prod(intrp.dim),1);
  jj = find(ismember(atlas2.tissue,ROI.(ARG.blockname{icond}).label));
  funroi(jj) = 1;
  
  % smooth and threshold again to fuse subrois
  funroi = reshape(funroi,intrp.dim);
  funroi = smooth3(funroi);
  funroi(funroi>0.1) = icond;
  funroi = funroi([181:-1:1],:,:); % FLIPP
  % interpolate sourcemodel
  intrp.fun = intrp.fun + funroi;
end

% dim 1 is left right

cfg = [];
% cfg.opacitymap = 'rampup';
cfg.method = 'surface';     % 'surface'
cfg.funparameter = 'fun';
%cfg.maskparameter = 'fun';
cfg.funcolormap = cmap;
cfg.funcolorlim = [0 2];
cfg.projmethod = 'project';
cfg.projvec = [0 2.5];
cfg.projcomb = 'max';
cfg.renderer = 'zbuffer';
cfg.colorbar = 'no';

% view from right
ft_sourceplot(cfg,intrp);

view(-44,18);
material dull % diffuse reflections

h = camlight('left');
h.Color = h.Color * 0.6;

drawnow


