%
% create figure 4, with the unpresented single features and the ratio
% between AudFeat and VisFeat in same conditions
%

close all
clearvars

% save name
snamef = '_Fig_4.png';

% load cmi and ratio data
load Data_PartialLoo_analysis.mat
load Data_CMI_Ratio.mat

neffects = Effects.neffects;

% get colors from function
Colors = get_ColorPalette;
condcolors = [Colors.AcondAfeat; Colors.VcondVfeat];

% plot style 
style = figstyle;

% statistical thresholds
pthrs = [0.01, 0.005, 0.001];


%% parameters
% color
colorcode = Colors.AudGroup;

% effects plot positions
nrows = 2;
ncols = 4;
fxpos = [3,4,7,8];

%% plot

%figure('Position',[680 250 700 600]);
figure('Units','inches','Position',[8 5 5 3.5]);
Adatamax = Effects.data{1:2};
Vdatamax = Effects.data{3:4};
datamax = [repmat(max(Adatamax(:)),1,2), repmat(max(Vdatamax(:)),1,2)];

YLim = [];
% plot single feature analysis --------------------------------------------
for ieff = 1:neffects
    HH(ieff) = cksubplot(nrows,ncols,fxpos(ieff),1.05);
    pos = HH(ieff).Position + [0.02, 0, 0, 0];
    icond = Effects.cond(ieff);
    Data = Effects.data{ieff};
    [~,npred] = size(Data);
    
    % check pval for color
    pcolor = repmat(colorcode,[npred,1]);
    pind = (median(Data,1) < Effects.bootprc{ieff}); % find non-sig
    pcolor(pind,:) = repmat(Colors.NonSig,[sum(pind),1]);
    
    barwithdots(Data,pcolor,[],[],...
        'Dotsize',style.BardotSize,...
        'Thickness',style.BardotThickness);
    hold on
    plot([0.5,npred+0.5],[Effects.bootprc{ieff},Effects.bootprc{ieff}],'--','Color',[1,1,1]*0.6);
    hold off
    
    % plot significance bars 
    maxdata = datamax(ieff);
    maxmi = max(Data(:));
    increment = maxdata * 0.15;
    if ~isempty(Effects.mcomp{ieff})
        % change rows for better visuals in plot
        Multcomp = Effects.mcomp{ieff}([1,3,2],:);
        % get group x values
        groups = Multcomp(:,1:2);
        % get pval threshold
        pval = Multcomp(:,6);
        level = sum(pval < pthrs,2);
        % plot line and stars if pval is < threshold
        iter = 1;
        for icomp = 1:numel(level)
            maxpair = Data(:,Multcomp(icomp,1:2));
            maxpair = max(maxpair(:));
            if level(icomp) > 0
                hold on
                % plot bars
                xval = groups(icomp,:) + 0.15; % plus offset to right
                yval = repmat([maxpair + increment + (iter-1)*increment],1,2);
                plot(xval,yval,'k-','LineWidth',style.LineWidth);
                % add tick marks
                ticklen = maxdata * 0.05;
                plot([xval(1),xval(1)],[yval(1),yval(1)-ticklen],'k-','LineWidth',style.LineWidth);
                plot([xval(2),xval(2)],[yval(1),yval(1)-ticklen],'k-','LineWidth',style.LineWidth);
                % add stars
                sigstar = repmat('*',1,level(icomp));
                text(xval(1)+diff(xval)/2, yval(1)+increment/2.5,sigstar,...
                    'HorizontalAlignment','center',...
                    'FontSize',style.SigStarSize);
                iter = iter + 1;
            end
        end
    else
        % if there is nothing significant, create empty level array
        ngroups = size(Data,2);
        level = zeros(1,ngroups);
    end
    
    % x axis ticks
    xticks([1:npred]);
    %axis square
    box off
    
    % axes aspect ratio
    pbaspect([3.5 3 1]);
    
    % x axis tick labels
    ticklabels = Effects.featLabels{ieff};
    ticklabels = cellfun(@(x) replace(x,'_',' '),ticklabels,'UniformOutput',false);
    xticklabels(ticklabels);
    
    % set y limits
    ylim([0,maxdata*1.1]);
    ylim([ HH(ieff).YAxis.Limits + [0, (sum(level>0)+0.5)*increment] ]);
    
    % set title
    if ieff < 3
        header = sprintf('%.1g - %.1g Hz', Effects.freqrange(Effects.freq(ieff),:));
        thandle(ieff) = title(header,'FontSize',style.HeaderSize);
    end
    if ismember(ieff,[1,3])
        header = sprintf('%s',Effects.condition{Effects.cond(ieff)});
        ylabel(header);
    end
        
    % line width, font size
    HH(ieff).LineWidth = style.LineWidth;
    HH(ieff).FontSize = style.TickLabelSize;
    HH(ieff).FontWeight = 'bold';
    HH(ieff).XTickLabelRotation = 45;
    
    % reset position
    pos = pos + [-0.06,0,0,0]; % shift all plots to left
    HH(ieff).Position = pos;
    
end

% align y axis limits in top panels
alim = deal([HH(1).YAxis.Limits(2), HH(2).YAxis.Limits(2)]);
[HH(1).YAxis.Limits(2), HH(2).YAxis.Limits(2)] = deal(max(alim));
% same for bottom panels
alim = deal([HH(3).YAxis.Limits(2), HH(4).YAxis.Limits(2)]);
[HH(3).YAxis.Limits(2), HH(4).YAxis.Limits(2)] = deal(max(alim));




% plot Aud/Vis cmi ratio --------------------------------------------------
ltick = sprintf('%.1g - %.1g',Ratio.freqrange(1,:));
rtick = sprintf('%.1g - %.1g Hz',Ratio.freqrange(2,:));
ticklabel = {ltick, rtick};

Rroi = Ratio.iroi(:,1);
Acidx = (Rroi == 1);
Vcidx = (Rroi == 2);

% get data, transform and shift
ratiodata = cat(2,Ratio.data{Acidx});
HH(5) = subplot(nrows,ncols,1);
barwithdots(ratiodata,colorcode,[],[],...
    'Dotsize',style.BardotSize,...
    'Thickness',style.BardotThickness);
box off

xticks([1,2]);
xticklabels(ticklabel);
ylim([0,1.6]);


HH(5).Position([2,4]) = HH(1).Position([2,4]);
HH(5).Position = HH(5).Position + [0.15, 0.072, -0.02, -0.15];

% line width, font size
HH(5).LineWidth = style.LineWidth;
HH(5).FontSize = style.TickLabelSize;
HH(5).FontWeight = 'bold';

% text what ratio is displayed
ratiotext = 'AudFeat_{V-only} / AudFeat_{A-only}';
text(1.5,-0.4,ratiotext,'FontSize',style.TextSize,'FontWeight','bold',...
    'HorizontalAlignment','center');



% get data, transform and shift
ratiodata = cat(2,Ratio.data{Vcidx});
HH(6) = subplot(nrows,ncols,5);
barwithdots(ratiodata,colorcode,[],[],...
    'Dotsize',style.BardotSize,...
    'Thickness',style.BardotThickness);
box off

xticks([1,2]);
xticklabels(ticklabel);
ylim([0,1.6]);


HH(6).Position([2,4]) = HH(3).Position([2,4]);
HH(6).Position = HH(6).Position + [0.15, 0.072, -0.02, -0.15];

% line width, font size
HH(6).LineWidth = style.LineWidth;
HH(6).FontSize = style.TickLabelSize;
HH(6).FontWeight = 'bold';

% text what ratio is displayed
ratiotext = 'AudFeat_{V-only} / LipFeat_{V-only}';
text(1.5,-0.4,ratiotext,'FontSize',style.TextSize,'FontWeight','bold',...
    'HorizontalAlignment','center');



% style changes

for ii = [3,4,6]
    HH(ii).Position(2) = HH(ii).Position(2) + 0.06;
end

% adjust title size and position style
for ii = 1:2
    % position is in respect to x and y axis
    thandle(ii).FontSize = style.HeaderSize;
    thandle(ii).Position(2) = thandle(ii).Position(2) * 1.2;
end

% load inlay figures for ROIs
Fignames = {'_Fig_ROI_inlay_1.fig','_Fig_ROI_inlay_2.fig'};
ninlays = length(Fignames);
inlaypos = [0.08,0.55,0.13,0.4;  0.08,0.13,0.13,0.4];

for ii = 1:ninlays
    HI(ii) = axes('pos',inlaypos(ii,:));
    fh = openfig(Fignames{ii});
    copyobj(allchild(get(fh,'CurrentAxes')),HI(ii));

    % copy parameters
    caxis(HI(ii),fh.CurrentAxes.CLim);
    view(HI(ii),fh.CurrentAxes.View);
    close(fh);
    axis off; axis vis3d; axis image;
    colormap(HI(ii),[1,1,1; condcolors(ii,:)]);
end

% now create boxes with curved edges around ROI data
HR = axes('pos',[0.0, 0.0, 1.0, 1.0]); %,'visible','off');
xlim([0,1]); ylim([0,1]);
rect(1) = rectangle('Position',[0.01,0.52,0.98,0.38],'Curvature',0.2,'EdgeColor',condcolors(1,:),'LineWidth',1.5);
rect(2) = rectangle('Position',[0.01,0.08,0.98,0.38],'Curvature',0.2,'EdgeColor',condcolors(2,:),'LineWidth',1.5);
HR.Visible = 'off';


% lables and gang ---------------------------------------------------------

% add panel labels
HH(7) = axes('pos',[0.05,0.05,0.85,0.85],'visible','off');

text(0.11,0.68,['Temporal' newline 'ROI'],'FontSize',style.HeaderSize,'FontWeight','bold',...
    'HorizontalAlignment','center','Color',condcolors(1,:));
text(0.11,0.16,['Occipital' newline 'ROI'],'FontSize',style.HeaderSize,'FontWeight','bold',...
    'HorizontalAlignment','center','Color',condcolors(2,:));


text(0.20,0.95,'A','FontSize',style.PanLabelSize,'FontWeight','bold','HorizontalAlignment','center');
text(0.20,0.44,'B','FontSize',style.PanLabelSize,'FontWeight','bold','HorizontalAlignment','center');
text(0.49,0.95,'C','FontSize',style.PanLabelSize,'FontWeight','bold','HorizontalAlignment','center');
text(0.49,0.44,'D','FontSize',style.PanLabelSize,'FontWeight','bold','HorizontalAlignment','center');




% save figure
print('-dpng',style.resolution,snamef);

snamepdf = '_Fig_4.pdf';
print('-dpdf','-r1200',snamepdf);



