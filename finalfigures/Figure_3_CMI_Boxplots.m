%
% create CMI boxplots with statistical highlighting 
%
% load data with MI values and P values from file
% here we plot only the mean across hemispheres, so the figure is less
% crowded
%

close all
clearvars

snamef = '_Fig_3_Boxplot_CMI_v3';

load CMI_Statistics_v3.mat


alphaZero = 0.01; % color if pval is smaller than that
alphaGroup = [0.01, 0.005, 0.001];

[nrows, nfreq, nsub, ncol] = size(CMIdata);
nhemi = 1;
nvar = 2; % uncorrected and corrected MI values
nstim = ncol/nhemi;


% get colors from function
Colors = get_ColorPalette;
condcolors = [Colors.AcondAfeat; Colors.VcondVfeat];

% plot style 
style = figstyle;

% subplot list
splist = [1:(nrows)*(nfreq+1)];
skipplot = [1:6:splist(end-1)];
splist(skipplot) = [];
splist = reshape(splist,[5,4]);

% labels
cortices = {'Temporal', 'Occipital'};
labelpart = {'left','right'};
stimlabel = {'ENV', 'LIPS'};
Trials = {'A-only', 'V-only'};
dataARG.GroupNames = {'AudFeat','LipFeat'};

% plotting parameters
boxshift = [-1, 1] * 0.15; % shift boxes to left or right by this factor
colorcode = [repmat(Colors.AudGroup,2,1); repmat(Colors.VisGroup,2,1)];


%% plotting


figure('Units','inches','Position',[6 4 7.2 6.0]); % 5 x 4 subplots
iter = 1;
for irow = 1:nrows
    for freq = 1:nfreq
        
        midata = sq(CMIdata(irow,freq,:,:));
        maxpairs = max(midata);
        maxpairs = maxpairs([1,3]);
        maxMI = max(midata(:));
        minMI = min(midata(:));

        HH(iter) = cksubplot(nrows+1,nfreq+1,splist(iter),1.05);
        % save axis position
        pos = HH(iter).Position;

        % check pval for color
        pcolor = colorcode;
        pval = sq(PvalZero(irow,freq,:));
        pind = (pval > alphaZero); % find non-sig
        pcolor(pind,:) = repmat(Colors.NonSig,[sum(pind),1]);

        
        % plot data
        barwithdots(midata,pcolor,[],[],...
            'Dotsize',style.BardotSize,...
            'Thickness',style.BardotThickness);
        xticklabels([]);
        % line through 99th percentile
        hold on
        plot([0,ncol] + 0.5,[RandPrctile(freq),RandPrctile(freq)],'--',...
            'Color',[1,1,1].*0.8, 'LineWidth',style.LineWidth);

        % fit axes
        xlim([0.5,4.6]);
        xticks([0.5,4.6]);
        ymax = max(maxMI(:));
        ymin = min(minMI(:));
        ylim([0; ymax+0.1*ymax]);
        HH(iter).XAxis.TickLength = [0,0];
        
        % open boxes
        box off

        % aspect ratio
        pbaspect([3.5 3 1]);

        % set column text
        if irow == 1 
            header = sprintf('%.1g - %.1g Hz', freqrange(freq,:));
            thandle(freq) = title(header);
        end

        % set trial label
        if freq == 1
            rowtext = sprintf('%s', ARG.rowcon{irow});
            ylabel(rowtext);
            ylabelpos(irow,:) = HH(iter).YLabel.Position;
        end
        
        % match y limits for plots in same ROI
        if mod(irow,2) == 0
            ylimtop = HH(iter-nfreq).YLim;
            ylimbottom = HH(iter).YLim;
            axlims = [ylimtop, ylimbottom];
            % set new axes 
            HH(iter-nfreq).YLim = [min(axlims(:)), max(axlims(:))];
            HH(iter).YLim = [min(axlims(:)), max(axlims(:))];
        end
        
        
        % get p-vals between MI values
        pvals = sq(PvalMI_CMI(irow,freq,:));
        level = sum(pvals' <= alphaGroup',1);
        
        % insert significance bars
        groups = [1:2];
        rshift = [0,2];
        yaxtop = HH(iter).YAxis.Limits(2);
        increment = yaxtop * 0.15;
        for icomp = 1:numel(level)
            if level(icomp) > 0
                % plot bars
                xval = groups + rshift(icomp) + 0.15; % plus offset to right
                yval = repmat([maxpairs(icomp) + increment],1,2);
                plot(xval,yval,'k-','LineWidth',style.LineWidth);
                % add tick marks
                ticklen = yaxtop * 0.05;
                plot([xval(1),xval(1)],[yval(1),yval(1)-ticklen],'k-','LineWidth',style.LineWidth);
                plot([xval(2),xval(2)],[yval(1),yval(1)-ticklen],'k-','LineWidth',style.LineWidth);
                % add stars
                sigstar = repmat('*',1,level(icomp));
                text(xval(1)+diff(xval)/2, yval(1)+increment/2.5,sigstar,...
                    'HorizontalAlignment','center',...
                    'FontSize',style.SigStarSize);
                
                % update y limits
                if yval > yaxtop
                    HH(iter).YAxis.Limits(2) = yaxtop + increment;
                    % adjust other subplot as well, if this is below
                    if mod(irow,2) == 0
                        HH(iter-nfreq).YAxis.Limits(2) = yaxtop + increment;
                    end
                end
                
            end
        end
        

        % reset position, adjust line width and font size
        HH(iter).Position = pos;
        HH(iter).LineWidth = style.LineWidth;
        HH(iter).FontSize = style.HeaderSize;
        HH(iter).FontWeight = 'bold';
        HH(iter).YAxis.TickLength = [0.01,0.1];
        

        iter = iter + 1;

    end
    
end 


% style changes

% move last two rows further below
yshift = [0.05*ones(10,1), 0.1*ones(10,1)];
for ii = 1:20
    HH(ii).Position = HH(ii).Position + [-0.02, -yshift(ii), 0, 0];
    HH(ii).YLabel.FontSize = style.MarkerSize;
    HH(ii).YLabel.Position(1) = HH(ii).YLabel.Position(1) * 1.2;
end

% adjust title size and position style
for ii = 1:nfreq
    % position is in respect to x and y axis
    thandle(ii).FontSize = style.MarkerSize + 2;
    thandle(ii).Position(2) = thandle(ii).Position(2) * 1.35;
end


% load inlay figures for ROIs
Fignames = {'_Fig_ROI_inlay_1.fig','_Fig_ROI_inlay_2.fig'};
ninlays = length(Fignames);
inlaypos = [0.03,0.52,0.1,0.4;  0.03,0.1,0.1,0.4];

for ii = 1:ninlays
    HI(ii) = axes('pos',inlaypos(ii,:));
    fh = openfig(Fignames{ii});
    copyobj(allchild(get(fh,'CurrentAxes')),HI(ii));

    % copy parameters
    caxis(HI(ii),fh.CurrentAxes.CLim);
    view(HI(ii),fh.CurrentAxes.View);
    close(fh);
    axis off; axis vis3d; axis image;
    colormap(HI(ii),[1,1,1; condcolors(ii,:)]);
end




% create two new axes with cortex labels
position = [0, HH(nfreq+1).Position(2), 0.1, 0.43];
HA(1) = axes('pos',position);
txt = [cortices{1} newline ' ROI'];
text(0.8,0.18,txt,'FontSize',style.HeaderSize,'FontWeight','bold','Color',condcolors(1,:),'HorizontalAlignment','center');
HA(1).Visible = 'off';

position = [0, HH(3*nfreq+1).Position(2), 0.1, 0.43];
HA(2) = axes('pos',position);
txt = [cortices{2} newline ' ROI'];
text(0.8,0.18,txt,'FontSize',style.HeaderSize,'FontWeight','bold','Color',condcolors(2,:),'HorizontalAlignment','center');
HA(2).Visible = 'off';


% now create boxes with curved edges around ROI data
HR = axes('pos',[0.0, 0.0, 1.0, 1.0]); %,'visible','off');
xlim([0,1]); ylim([0,1]);
rect(1) = rectangle('Position',[0.01,0.54,0.98,0.38],'Curvature',0.2,'EdgeColor',condcolors(1,:),'LineWidth',2);
rect(2) = rectangle('Position',[0.01,0.12,0.98,0.38],'Curvature',0.2,'EdgeColor',condcolors(2,:),'LineWidth',2);
HR.Visible = 'off';


% MI CMI label at the bottom
HL = axes('pos',[0.17, 0.0, 0.2, 0.15],'visible','off');
xlim([0,5]);

% set x label in bottom left panel
% data explanation
text(1,0.55,'MI','FontSize',style.MarkerSize,'FontWeight','bold',...
    'Color',colorcode(1,:),'HorizontalAlignment','center');
text(2,0.55,'CMI','FontSize',style.MarkerSize,'FontWeight','bold',...
    'Color',colorcode(1,:),'HorizontalAlignment','center');
text(3,0.55,'MI','FontSize',style.MarkerSize,'FontWeight','bold',...
    'Color',colorcode(3,:),'HorizontalAlignment','center');
text(4,0.55,'CMI','FontSize',style.MarkerSize,'FontWeight','bold',...
    'Color',colorcode(3,:),'HorizontalAlignment','center');

% group names
text(1.6,0.35,dataARG.GroupNames{1},'FontSize',style.MarkerSize,...
    'FontWeight','bold','Color',colorcode(1,:),'HorizontalAlignment','center');
text(3.6,0.35,dataARG.GroupNames{2},'FontSize',style.MarkerSize,...
    'FontWeight','bold','Color',colorcode(3,:),'HorizontalAlignment','center');





% save figure
print('-dpng',style.resolution,snamef);

snamepdf = '_Fig_3.pdf';
print('-dpdf','-r1200',snamepdf);




