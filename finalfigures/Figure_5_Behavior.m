%
% load cmi hit rate correlation data and plot
%

close all
clearvars

% load data
load Data_CMI_Hitrate.mat

snamef = '_Fig_5.png';

% choose model
modeltype = 'MI';
model_ind = find(ismember(EffectsARG.methods,modeltype));

% get colors from function
Colors  = get_ColorPalette;
condcolors = [Colors.AcondAfeat; Colors.VcondVfeat];


%% parameter
npred = EffectsARG.npred;
nfreq = EffectsARG.topfreq;

neffects = numel(Effects);

AcondColor = Colors.AcondAfeat;
VcondColor = Colors.VcondVfeat;
CondColorPalette = {AcondColor, VcondColor};

% color
colorcode = Colors.AudGroup;

xlimits = {[0.2:0.4:1.4], [0:0.125:0.5], [0:0.2:0.6], [0.02:0.02:0.08]};
ylimits = {[0.5:0.1:0.9], [0.2:0.2:1], [0.2:0.2:1], [0.2:0.2:1]};

% plot style 
style = figstyle;

% unpack
freqbands = EffectsARG.freqbands;

% subplot list
splist = [2,3,5,6];


%% plot


rval = cellfun(@(x) sqrt(x.model(1)), Effects.Stats(model_ind,:),'UniformOutput',false);
rval = cell2mat(rval);
for freq = 1:nfreq
    fprintf('%.1g - %.1g Hz: \n',EffectsARG.freqbands(freq,:));
    fprintf('full model R2: %.3f \n\n',rval(freq));
end



figure('Units','inches','Position',[8 5 5 4.5]);
iter = 1;
for ipred = 1:npred
    for freq = 1:nfreq
    
        % get data
        X = Effects.Residuals{model_ind,freq}(:,1,ipred);
        Y = Effects.Residuals{model_ind,freq}(:,2,ipred);
        % two linear for partial regression line plot
        beta = Effects.Beta{model_ind,freq}([1,ipred+1]);
        dx = [1, 1; -2, max(X)*2]';
        fx = dx * beta;

        % subplot
        HH(iter) = cksubplot(npred,nfreq+1,splist(iter),1.05);
        HH(iter).Position = HH(iter).Position + [-0.04, 0, -0.02, -0.02];
        plot(dx(:,2),fx,'-','LineWidth',style.LineWidth,'Color',[1,1,1]*0.5);
        hold on
        plot(X,Y,'.','MarkerSize',style.MarkerSize,'Color',colorcode);

        % axes limits and props
        xlim([-2,2]);
        ylim([-2,2]);
        axis square
        box off

        % text beta and pval to plot
        pval = Effects.Stats{model_ind,freq}.B_P(ipred+1);
        beta = beta(2);
        textx = 0.75;
        texty = 1.4;
        yshift = 0.2;
        text(textx,texty+yshift,sprintf('beta=%.2g',beta),...
            'FontSize',style.FontSize,...
            'FontWeight','bold',...
            'HorizontalAlignment','left');
        text(textx,texty-yshift,sprintf('p=%.2g',pval),...
            'FontSize',style.FontSize,...
            'FontWeight','bold',...
            'HorizontalAlignment','left');

        % axes laxout
        HH(iter).LineWidth = style.LineWidth;
        HH(iter).FontWeight = 'bold';
        HH(iter).FontSize = style.FontSize;
        HH(iter).XAxis.TickValues = [-2 0 2];
        HH(iter).YAxis.TickValues = [-2 0 2];

        if iter < 3
            xtitle = sprintf('MI aud env (adjusted)');
            xlabel(xtitle);
        elseif iter > 2
            xtitle = sprintf('MI aud pitch (adjusted)');
            xlabel(xtitle);
        end
        ytitle = sprintf('Comprehension (adjusted)');
        ylabel(ytitle);
        if iter < 3
            header = sprintf('%.1g - %.1g Hz', freqbands(iter,:));
            thandle(iter) = title(header);
        end

        % print statistics to command line
        fprintf('Effect %d \n',iter);
        fprintf('beta: %.3f     p: %.3f     \n',beta,pval);
        fprintf('\n');

        drawnow;
        iter = iter + 1;
    end
end

% shift bottom row up
for ii = 3:4
    HH(ii).Position(2) = HH(ii).Position(2) + 0.06;
end

% style changes

% adjust title size and position style
for ii = 1:nfreq
    % position is in respect to x and y axis
    thandle(ii).FontSize = style.HeaderSize;
    thandle(ii).Position(2) = thandle(ii).Position(2) * 1.35;
end

% load inlay figures for ROIs
Fignames = {'_Fig_ROI_inlay_1.fig','_Fig_ROI_inlay_2.fig'};
ninlays = length(Fignames);
inlaypos = [0.09,0.52,0.13,0.4;  0.09,0.10,0.13,0.4];

for ii = 1:ninlays
    HI(ii) = axes('pos',inlaypos(ii,:));
    fh = openfig(Fignames{ii});
    copyobj(allchild(get(fh,'CurrentAxes')),HI(ii));

    % copy parameters
    caxis(HI(ii),fh.CurrentAxes.CLim);
    view(HI(ii),fh.CurrentAxes.View);
    close(fh);
    axis off; axis vis3d; axis image;
    colormap(HI(ii),[1,1,1; condcolors(ii,:)]);
end

% now create boxes with curved edges around ROI data
HR = axes('pos',[0.0, 0.0, 1.0, 1.0]); %,'visible','off');
xlim([0,1]); ylim([0,1]);
rect(1) = rectangle('Position',[0.01,0.52,0.98,0.38],'Curvature',0.2,'EdgeColor',condcolors(1,:),'LineWidth',2);
rect(2) = rectangle('Position',[0.01,0.08,0.98,0.38],'Curvature',0.2,'EdgeColor',condcolors(2,:),'LineWidth',2);
HR.Visible = 'off';



% add panel labels and cortex labels in color
HH(iter+1) = axes('pos',[0.05,0.05,0.85,0.85],'visible','off');


text(0.125,0.66,['Temporal' newline 'ROI'],'FontSize',style.HeaderSize,'FontWeight','bold',...
    'HorizontalAlignment','center','Color',condcolors(1,:));
text(0.125,0.16,['Occipital' newline 'ROI'],'FontSize',style.HeaderSize,'FontWeight','bold',...
    'HorizontalAlignment','center','Color',condcolors(2,:));

% save 
print('-dpng',style.resolution,snamef);

snamefpdf = '_Fig_5.pdf';
print('-dpdf','-r1200',snamefpdf);

