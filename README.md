
Acoustic speech and lipreading in human MEG.

Bröhl, F., Keitel, A., Kayser, C., 2022. MEG Activity in Visual and Auditory Cortices Represents Acoustic Speech-Related Information during Silent Lip Reading. eNeuro 9, ENEURO.0209-22.2022. https://doi.org/10.1523/ENEURO.0209-22.2022
