%
% plot MI as a function of lags for all dimensions
%

close all
clearvars

COMP = 2;
Step0_setup;

% directories and save name
snamebase = sprintf('%sMI_modalvar_optlag',DIR.mi);


% DATA and PARAMETERS --------------------------------
% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);
goodsub = find([SUBS.exclude]==0);
nsub = length(goodsub);
nstim = 2; % env and lips
nfreq = 5;
nreps = 30;


blocks = [1:4; 9:12];
ncond = size(blocks,1);

cortices = {'auditory', 'visual'};
hemispheres = {'left', 'right'};
nroi = length(cortices);
nhemi = length(hemispheres);

biascorr = false; % if Iboot should be subtracted



% ROI data -------------------------------------------
% get sourceatlas
load sourceatlas_grid.mat
load('ROIVOXELS.mat','roivoxins');
load pROI_voxel.mat
nvox = length(pROI.all.inroivoxins);


% unpack roi voxel indices
leftA = pROI.auditory.left.inroivoxins;
rightA = pROI.auditory.right.inroivoxins;
leftV = pROI.visual.left.inroivoxins;
rightV = pROI.visual.right.inroivoxins;
% intersect with ROI voxel mat
[~, leftA] = intersect(pROI.all.inroivoxins,leftA);
[~, rightA] = intersect(pROI.all.inroivoxins,rightA);
[~, leftV] = intersect(pROI.all.inroivoxins,leftV);
[~, rightV] = intersect(pROI.all.inroivoxins,rightV);




%% load data
filename = sprintf('%s_auditory_sub01.mat',snamebase);
data = load(filename,'ARG','ARGM');
freqrange = data.ARG.freq_range;
lags = data.ARGM.lags*(1/data.ARG.srate);
%lags = [-0.1:0.02:0.5];
%lags = [0.06:0.02:0.5];
nlags = length(lags);

MI = zeros(ncond,nstim,nsub,nfreq,nlags,nvox);
MIRand = zeros(ncond,nstim,nsub,nfreq,nlags,nvox,nreps);

fprintf('load data\n');
tic
for icond = 1:ncond
    for isub2 = 1:length(goodsub)
        isub = goodsub(isub2);
        filename = sprintf('%s_%s_sub%02d.mat',snamebase,cortices{icond},isub);
        data = load(filename);
        
        MI(icond,1,isub2,:,:,:) = data.MI_E;
        MI(icond,2,isub2,:,:,:) = data.MI_L;
        %MIRand(icond,1,isub2,:,:,:,:) = data.MIRand_E;
        %MIRand(icond,2,isub2,:,:,:,:) = data.MIRand_L;
        
    end
end
toc


if biascorr == true
    fprintf('subtract bootstrap data\n');
    MI = MI - sq(mean(MIRand,7));
end


%% plot data

% do plots for each condition, stimulus, freq, roi and hemisphere
% take average across roi voxels and participants

collectStimA = zeros(4,nsub,nfreq,nlags);
collectStimV = zeros(4,nsub,nfreq,nlags);

YLabels = {'left', 'right', 'left', 'right'};
stimlabel = {'ENV', 'LIPS'};

for iroi = 1:nroi
    for icond = 1:ncond
        
        figure('Position',[140 80 1400 900]); % -> 4 figures
        iter = 1;
        
        % get the correct voxels for roi and hemisphere from all voxels
        roivoxL = pROI.(cortices{iroi}).left.inroivoxins;
        roivoxR = pROI.(cortices{iroi}).right.inroivoxins;
        [~, ivox] = intersect(pROI.all.inroivoxins,[roivoxL; roivoxR]); % vox idx
        
        % take stim in cross roi of both trials and store for later use
        if iroi == 1
            fprintf('A cortex\n');
            collectLip = sq(MI(:,2,:,:,:,ivox));
            
            collectStim(1,:,:,:) = sq(mean(mean(MI(:,1,:,:,:,ivox),1),6));
            collectStim(3,:,:,:) = sq(mean(mean(MI(:,2,:,:,:,ivox),1),6));
        elseif iroi == 2
            fprintf('V cortex\n');
            collectEnv = sq(MI(:,1,:,:,:,ivox));
            
            collectStim(2,:,:,:) = sq(mean(mean(MI(:,1,:,:,:,ivox),1),6));
            collectStim(4,:,:,:) = sq(mean(mean(MI(:,2,:,:,:,ivox),1),6));
        end
        
        for istim = 1:nstim
            for freq = 1:nfreq
                midata = sq(mean(MI(icond,istim,:,freq,:,ivox),6))';
                midata = midata ./ max(midata,[],1); % norm each subj.
                midatam = sq(mean(midata,2));
                midatasem = std(midata,[],2)./sqrt(nsub);
                [maxmi,maxidx] = max(midatam);

                hh = subplot(nstim,nfreq,iter);
                plot(lags,midata, 'LineWidth',0.2, 'Color', [0.7,0.7,0.7]);
                hold on
                plot(lags,midatam, 'LineWidth',1.2, 'Color', [0,0,0.5]);
                plot(lags(maxidx),maxmi,'r*');

                % plot mean +/- 1 SEM with filled area
                plot(lags,midatam + midatasem, 'LineWidth',0.8, 'Color', [0,1,1]*0.6);
                plot(lags,midatam - midatasem, 'LineWidth',0.8, 'Color', [0,1,1]*0.6);
                fill([lags, fliplr(lags)],[(midatam-midatasem)',flipud(midatam+midatasem)'], ...
                    [0,1,1], 'LineStyle', 'none', 'FaceAlpha', 0.1);
                hold off

                iter = iter + 1;

                pos = hh.Position;

                % annotate maximum as text
                Yt = hh.YLim(2) * 0.9;
                Xt = hh.XLim(2) * 0.8;
                text(Xt,Yt,num2str(lags(maxidx)),'Color','r');

                % stim labels
                if freq == 1
                    ilabel = stimlabel{istim};
                    ylabel(ilabel);
                end

                % freq labels
                if istim == 1 
                    header = sprintf('%.2f - %.2f Hz', freqrange(freq,:));
                    title(header);
                end

                % xlabel
                if freq == 1 && istim == 4
                    xlabel('ms');
                end

                % reset to original position
                hh.Position = pos;

                drawnow;
            end
        
        end
        
        header = sprintf('%s cortex - %s trials', cortices{iroi}, cortices{icond});
        ckfiguretitle(header);
        
        snamef = sprintf('%s/Timelag_%s_cortex_%s_trials', DIR.cmi, cortices{iroi},cortices{icond});
        %print('-dpng',snamef);
    end
end




%% plot trial averaged peaks
% miBoth = zeros(nstim,nsub,nfreq,nlags);
% 
% miEnv = sq(mean(mean(collectEnv,1),5));
% miLip = sq(mean(mean(collectLip,1),5));
% 
% miBoth(1,:,:,:) = miEnv ./ max(miEnv,[],3);
% miBoth(2,:,:,:) = miLip ./ max(miLip,[],3);

MIall = collectStim;
Optlags = zeros(2*nstim,nfreq);


Labels = {['AudFeat a+v trials' newline 'a cortex'], ['AudFeat a+v trials' newline 'v cortex'], ...
    ['VisFeat a+v trials' newline 'a cortex'], ['VisFeat a+v trials' newline 'v cortex']};

figure('Position',[140 80 1400 900]);
iter = 1;
for irow = 1:2*nstim
    for freq = 1:nfreq
        hk = subplot(2*nstim,nfreq,iter);
        midata = sq(MIall(irow,:,freq,:));
        midata = midata./max(midata,[],2);
        midatam = sq(mean(midata,1));
        midatasem = std(midata,[],1)./sqrt(nsub);
        [maxmi,maxidx] = max(midatam);
        Optlags(irow,freq) = lags(maxidx);
        plot(lags, midata, 'LineWidth',0.2, 'Color', [0.7,0.7,0.7]);
        
        hold on
        plot(lags, midatam, 'LineWidth',1.2, 'Color', [0,0,0.5]);
        plot(lags(maxidx),maxmi,'r*');
        
        % plot mean +/- 1 SEM with filled area
        plot(lags,midatam + midatasem, 'LineWidth',0.8, 'Color', [0,1,1]*0.6);
        plot(lags,midatam - midatasem, 'LineWidth',0.8, 'Color', [0,1,1]*0.6);
        fill([lags, fliplr(lags)],[(midatam-midatasem),fliplr(midatam+midatasem)], ...
            [0,1,1], 'LineStyle', 'none', 'FaceAlpha', 0.1);
        hold off
        
        % remember position
        pos = hk.Position;
        
        % annotate maximum as text
        Yt = hk.YLim(2) * 0.9;
        Xt = hk.XLim(2) * 0.8;
        text(Xt,Yt,num2str(lags(maxidx)),'Color','r');
        
        if freq == 1
            ilabel = Labels{irow};
            ylabel(ilabel);
        end
        
        if irow == 1
            header = sprintf('%.2f - %.2f Hz', freqrange(freq,:));
            title(header);
        end
        
        %xlim([0.06,0.5]);
        iter = iter + 1;
    end
end

snamef = sprintf('%s/Timelag_across_trials', DIR.cmi);
print('-dpng',snamef);




%% create matrix with optlags for each stim, freq band and ROI voxel

% auditory cortex in roivoxins
roivoxAL = pROI.auditory.left.inroivoxins;
roivoxAR = pROI.auditory.right.inroivoxins;
[~, Aivox] = intersect(pROI.all.inroivoxins,[roivoxAL; roivoxAR]); % vox idx

% visual cortex in roivoxins
roivoxVL = pROI.visual.left.inroivoxins;
roivoxVR = pROI.visual.right.inroivoxins;
[~, Vivox] = intersect(pROI.all.inroivoxins,[roivoxVL; roivoxVR]); % vox idx


OptlagsRoivox = zeros(nstim,nfreq,nvox);
OptlagsRoivox_dimord = {'env/lip x freq x voxel'};
for freq = 1:nfreq
    % Env
    OptlagsRoivox(1,freq,Aivox) = Optlags(1,freq);
    OptlagsRoivox(1,freq,Vivox) = Optlags(2,freq);
    % Lip
    OptlagsRoivox(2,freq,Aivox) = Optlags(3,freq);
    OptlagsRoivox(2,freq,Vivox) = Optlags(4,freq);
end


Optlags

snameo = sprintf('%s/matlab/Optlags_MI.mat', DIR.main);
save(snameo, 'Optlags', 'OptlagsRoivox', 'Labels', 'OptlagsRoivox_dimord');

