function behavior = getBehavior(DIR)
%   out = getBehavior(dir)
%
% load data and return behaviour for all participants
%
% input: dir - struct with path to cleaned data files containing trialinfo
%        (i.e. DIR.pre)
%


% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);
goodsub = find([SUBS.exclude]==0);
nsubs = length(goodsub);

conditions = {'auditory', 'visual'};
pgmc = {'a', 'v'};
nconds = length(conditions);
blocks = [1:4; 9:12];
nblocks = size(blocks,2);


%% main
Correct = zeros(nsubs,2); % percent correctly answered trials

for isub2 = 1:nsubs
    isub = goodsub(isub2);
    for icond = 1:nconds
        % load data files with trial info
        resp = {}; blockOrd = []; stimOrd = {};
        for iblock2 = 1:4
            iblock = blocks(icond,iblock2);
            filename = sprintf('%sA_sub%d_block%d_clean', DIR.pre, isub, iblock);
            data = load(filename);
            trialinfo = data.data.trialinfo;
            
            blockOrd(iblock2) = trialinfo(1,5);
            stimOrd{iblock2} = trialinfo(:,3:4);
            resp{iblock2} = trialinfo(:,6);
            
            
            
            
            % load logfiles with reaction times
            rtimes = {};
            logname = sprintf('%sAVentrain_%s_S%d_B%d', DIR.log, conditions{icond}, isub, iblock2);
            log = load(logname,'ARG');
            rtimes{iblock2} = log.ARG.rtime;
            
        end
        blockresp = cellfun(@sum,resp);
        blocklen = cellfun(@length,resp);
        Correct(isub2,icond) = sum(blockresp)/sum(blocklen);
        
        
        % load pgm with stimulus variants
        pgmname = sprintf('%sSub%d_%s', DIR.pgm, isub, pgmc{icond});
        pgm = load(pgmname);
        
        
        
        
        
        
    end
end



behavior.Correct = Correct;
end



%% plot

% boxplot(Correct, 'plotstyle','compact');
% hold on
% 
% % individual data 
% jitter = 0.1 .* rand(nsubs,nconds);
% jitter = jitter .* repmat([1,-1],nsubs,1);
% X = repmat([1:nconds],nsubs,1);
% X = X + jitter + [0.1, -0.1];
% plot(X,Correct,'o','MarkerSize',5);
% 
% % individual lines
% plot(X',Correct','-','MarkerSize',0.1,'Color',[0.5,0.5,0.5]);
%             
% xlim([0.5,2.8]);

