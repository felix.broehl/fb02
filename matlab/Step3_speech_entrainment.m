%
% load envelope and lip area tracking whole-brain data 
% subtract mean of randomized tracking and save to file for final figure
%
% save indices for ROIs and subrois 
%

close all
clearvars

COMP = 2;
Step0_setup;

% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);
goodsub = find([SUBS.exclude]==0);
nsub = length(goodsub);

blocks = [1:4; 9:12]; % 1:4 A only; 9:12 V only
blockname = {'auditory', 'visual'};

% save name and directories
snamebase = sprintf('%sMI_CoreFeatures_',DIR.mi);
sname = sprintf('%sfinalfigures/Speech_Entrainment',DIR.main);

% source and atlas data ---------------------------------------------------
% get sourceatlas
load sourceatlas_grid.mat
load('ROIVOXELS.mat','roivoxins');
nvox = length(roivoxins);




%% define ROI structure from brainnetome atlas
% contains intersected voxel for primary cortices in roivoxins
load ROIVOXELS.mat
load pROI_voxel.mat % -> pROI structure
ARG.pROI = pROI;
allvox = pROI.all.inroivoxins;

audvox = intersect(allvox,[pROI.auditory.left.inroivoxins; pROI.auditory.right.inroivoxins]);
visvox = intersect(allvox,[pROI.visual.left.inroivoxins; pROI.visual.right.inroivoxins]);
roivox = {audvox, visvox};
nroi = numel(roivox); % auditory and visual cortex


% hemisphere separated ROI indices
audvoxR = intersect(allvox,pROI.auditory.right.inroivoxins);
audvoxL = intersect(allvox,pROI.auditory.left.inroivoxins);
visvoxR = intersect(allvox,pROI.visual.right.inroivoxins);
visvoxL = intersect(allvox,pROI.visual.left.inroivoxins);


%% load data from CoreFeature files with large number of repetitions
filename = sprintf('%sauditory_sub01.mat', snamebase);
data = load(filename,'ARG','ARGF');

nconds = 2;
freqbands = data.ARG.freq_range;
nfreq = length(freqbands);
nenv = 2;
nrand = data.ARG.nreps;
MI = zeros(nconds,nsub,nfreq,nenv,nvox);
MIRand = zeros(nconds,nsub,nfreq,nenv,nvox);
MIRandStd = zeros(nconds,nsub,nfreq,nenv,nvox);
MIPrct = zeros(nconds,nsub,nfreq,nenv);

fprintf('this takes some time \n');
tic
for icond = 1:nconds
    % load all MI data and average across 
    for isub2 = 1:nsub
        isub = goodsub(isub2);
        fprintf('loading condition %d, sub %d \n',icond,isub2);
        filename = sprintf('%s%s_sub%02d.mat', ...
            snamebase, blockname{icond}, isub);
        data = load(filename);
        MI(icond,isub2,:,:,:) = data.MI;
        MIRand(icond,isub2,:,:,:) = sq(mean(data.MIRand,4));
        MIRandStd(icond,isub2,:,:,:) = sq(std(data.MIRand,[],4));
        
        tmprand = sq(max(data.MIRand,[],3));
        MIPrct(icond,isub2,:,:) = sq(prctile(tmprand,99,3));
    end
end

dataARG = data.ARG;
ARG.ncond = nconds;
ARG.nenv = nenv;
ARG.freqbands = freqbands;
ARG.blockname = blockname;

% get std across participants
MIStd = std(MI,[],2);
% take average across participants
MIdiff = sq(median(MI - MIRand,2));
% subtract 99th percentile
MIPrc99 = sq(mean(MI - repmat(MIPrct,[1,1,1,1,nvox]),2));
toc

ARG.fromfile = mfilename;

% save to file
save(sname,'MIdiff','MIPrc99','MI','MIStd','MIRand','MIRandStd','ARG',...
    'dataARG');


% find peak MI voxels within ROIs and hemisphere - 4 in total
targetfreq = 6; % 0.5 to 8 Hz
Peaks.targetfreq = targetfreq;
[~,peakAR] = max(MIPrc99(:,targetfreq,:,audvoxR),[],4);
[~,peakAL] = max(MIPrc99(:,targetfreq,:,audvoxL),[],4);
[~,peakVR] = max(MIPrc99(:,targetfreq,:,visvoxR),[],4);
[~,peakVL] = max(MIPrc99(:,targetfreq,:,visvoxL),[],4);

% project back onto roivoxins
% these are therefore also the indices for the sourcemodel grid, because
% roivoxins contains their linear indices
peakAR = roivoxins(audvoxR(sq(peakAR)));
peakAL = roivoxins(audvoxL(sq(peakAL)));
peakVR = roivoxins(visvoxR(sq(peakVR)));
peakVL = roivoxins(visvoxL(sq(peakVL)));

Peaks.sourcemodel = {peakAR, peakAL, peakVR, peakVL};


% to get these grid points in the pROI structure, use:
[~,peakARp] = ismember(peakAR,roivoxins(allvox));
[~,peakALp] = ismember(peakAL,roivoxins(allvox));
[~,peakVRp] = ismember(peakVR,roivoxins(allvox));
[~,peakVLp] = ismember(peakVL,roivoxins(allvox));


Peaks.pROI = cellfun(@(x) reshape(x,[2,2]),{peakARp,peakALp,peakVRp,peakVLp},'UniformOutput',false);

% save to file
sname = 'peakMIindices.mat';
save(sname,'Peaks');




