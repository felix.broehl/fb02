function meg = prepare_sourceMEG(DIR,isub,iblock,newfs,voxsubset)
%
% meg = prepare_sourceMEG(DIR,isub,iblock,newfs)
%

% get subject data
expname = 'AVentrain';
[~, ~, filenames] = subject_database(expname);

% parallel pool
poolobj = manageParpool(2);
numcores = poolobj.NumWorkers;

% load data and log files
ifilt = 2;
fprintf('loading sub %d block %d \n\n', isub, iblock);
load(sprintf('%sA_sub%d_block%d_clean',DIR.pre,isub,iblock),'data');
log = load(sprintf('%s%s_%s_S%d_%s.mat',DIR.log,expname,filenames{1,iblock},isub,filenames{2,iblock}));   % load log file
filt = load(sprintf('%sfilt_lcmv%d_%d_%d',DIR.flt,ifilt,isub,iblock),'filt','filtinfo');

ntrl = length(data.trial);

% preprocessing ---------------------------------------------------
% highpass filter (0.8 Hz) before source projection
cfg             = [];
cfg.hpfilter    = 'yes';
cfg.hpfreq      = 0.8;
cfg.hpfilttype  = 'but';
cfg.hpfiltord   = 4;
cfg.hpfiltdir   = 'twopass';
cfg.detrend     = 'no';
data            = ft_preprocessing(cfg, data);

% lowpass filter (30 Hz) before source projection
cfg             = [];
cfg.lpfilter    = 'yes';
cfg.lpfreq      = 30;
cfg.lpfilttype  = 'but';
cfg.lpfiltord   = 4;
cfg.lpfiltdir   = 'twopass';
cfg.detrend     = 'no';
data            = ft_preprocessing(cfg, data);

% downsample data to new_fs
cfg = [];
cfg.resamplefs = newfs;
data = ft_resampledata(cfg, data);

% cut out shorter segments
cfg = [];
cfg.toilim = [0 8.5]; % max stimlength 8.24 s
data = ft_redefinetrial(cfg, data);


% numerically increase values for beamforming
data2 = cat(3, data.trial{:});
data2 = permute(data2, [3,1,2]);
parfor k = 1:ntrl
    data2(k,:,:) = data2(k,:,:) * 1e12;
end


% source projection -----------------------------------------------
% each trial has now different length!
fprintf('project meg onto source\n');
tic
nvox = size(filt.filt,1);
ntime = size(data2,3);
source_data = zeros(ntrl,nvox,ntime);
weights = filt.filt;
parfor k = 1:ntrl
    source_data(k,:,:) = weights * sq(data2(k,:,:));
end
toc

% select subset of voxel
source_data = source_data(:,voxsubset,:);

% output
meg.source_data = source_data;
meg.log = log.ARG;
meg.trialinfo = data.trialinfo;
meg.fs = newfs;
meg.roilim = cfg.toilim;


end
