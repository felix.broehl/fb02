%
% laod CMI data and look at hemispheric differences and CMI values in the
% sub ROIs
%
% load bootstrap data to compute 95th percentile baseline per hemisphere
% and sub area
%

close all
clearvars

% set up
COMP = 2;
Step0_setup;

a = fileparts(mfilename('fullpath'));
cd(a);

% if all sub areas should also be box plottet
plotall = false;

% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);
goodsub = find([SUBS.exclude]==0);
nsub = length(goodsub);

% load voxel structure
snamebase = sprintf('%sCMI_meanlag',DIR.cmi);
load('ROIVOXELS.mat','roivoxins');
%load pROI_voxel.mat
%nvox = length(pROI.all.inroivoxins);

% load ROI data
[pROI,atlaslabel,~,intrptissue] = pROI_brainnetome; 
roilabel = {'auditory','visual'};
nvox = length(pROI.all.inroivoxins);
nrois = numel(roilabel); 
ncond = nrois;
nfreq = 5;
hemilabel = {'left','right'};
nhemi = length(hemilabel); % left and right hemisphere


% get indices for all sub areas
SubAreaIndices = [];
HemiIndices = [];
for iroi = 1:nrois
    nsubroi = numel(pROI.(roilabel{iroi}).right.label);
    for isubroi = 1:nsubroi
        tmplabel = pROI.(roilabel{iroi}).right.label(isubroi);
        % left plus right hemisphere
        subvox = find(ismember(intrptissue,[tmplabel, tmplabel+1]));
        [~,subvox] = intersect(roivoxins(pROI.all.inroivoxins), subvox);
        SubAreaIndices{isubroi,iroi} = subvox;
    end
    
    % hemispheres
    for ihemi = 1:nhemi
        tmplabel = pROI.(roilabel{iroi}).(hemilabel{ihemi}).label;
        hemivox = find(ismember(intrptissue,[tmplabel]));
        [~,hemivox] = intersect(roivoxins(pROI.all.inroivoxins), hemivox);
        HemiIndices{ihemi,iroi} = hemivox;
    end
end


%% load data
filename = sprintf('%s_auditory_sub01.mat',snamebase);
cdata = load(filename,'ARG','ARGF');
predlen = cdata.ARG.predlen;
GroupNames = cdata.ARG.GroupNames;

% CMI storage
CMI = zeros(ncond,predlen,nsub,nfreq,nvox);
% bootstrap storage
SubAreaBootstrap = zeros(ncond,nsub,predlen,nfreq,nsubroi,nrois);
HemiBootstrap = zeros(ncond,nsub,predlen,nfreq,nhemi,nrois);

for icond = 1:ncond
    for isub2 = 1:length(goodsub)
        isub = goodsub(isub2);

        % load grouped predictor CMI data
        condlabel = roilabel{icond};
        fprintf('load cond. %1d sub %02d \n',icond,isub2);
        filename = sprintf('%s_%s_sub%02d.mat', snamebase,condlabel,isub);
        data = load(filename,'ARG','ARGF','CMI_B1_E','CMI_B1_L',...
            'CMI_C2_E','CMI_C2_L');
        % ENV
        CMI(icond,1,isub2,:,:) = data.CMI_B1_E;
        % LIP
        CMI(icond,2,isub2,:,:) = data.CMI_B1_L;
        
        
        % get bootstrap data for sub areas
        % size(CMI_C2) = [5x369x2000]
        for iroi = 1:nrois
            for isubroi = 1:nsubroi
                tmpidx = SubAreaIndices{isubroi,iroi};
                AudBoot = sq(mean(data.CMI_C2_E(:,tmpidx,:),2));
                LipBoot = sq(mean(data.CMI_C2_L(:,tmpidx,:),2));
                SubAreaBootstrap(icond,isub2,1,:,isubroi,iroi) = sq(prctile(AudBoot,95,2));
                SubAreaBootstrap(icond,isub2,2,:,isubroi,iroi) = sq(prctile(LipBoot,95,2));
            end
        end
        
        % get bootstrap for hemispheres
        for iroi = 1:nrois
            for ihemi = 1:nhemi
                tmpidx = HemiIndices{ihemi,iroi};
                AudBoot = sq(mean(data.CMI_C2_E(:,tmpidx,:),2));
                LipBoot = sq(mean(data.CMI_C2_L(:,tmpidx,:),2));
                HemiBootstrap(icond,isub2,1,:,ihemi,iroi) = sq(prctile(AudBoot,95,2));
                HemiBootstrap(icond,isub2,2,:,ihemi,iroi) = sq(prctile(LipBoot,95,2));
            end
        end
        
    end
end

% average across sub area voxel and then average across participants
SubAreaBootstrap = sq(mean(SubAreaBootstrap,2));
HemiBootstrap = sq(mean(HemiBootstrap,2));


freqrange = data.ARG.freq_range;



%% sort data and plot if desired
rowroi = [1,1,2,2];
rowcon = [1,2,1,2];
nrows = 4;
rowlabel = {'A-only','V-only'};
stimlabel = {'AudFeat','VisFeat'};
cortices = {'Auditory','Visual'};
subroinames = [pROI.primaryroinames(1:4); pROI.primaryroinames(9:12)];


MIarea = zeros(ncond,nrois,predlen,nsub,nfreq,nsubroi);
MIhemi = zeros(ncond,nrois,predlen,nsub,nfreq,nhemi);
for istim = 1:predlen
    % first sort MI data to figure format
    for icond = 1:ncond
        for iroi = 1:nrois
            
            % sort subarea data
            for isubroi = 1:nsubroi
                subvox = SubAreaIndices{isubroi,iroi};
                MIarea(icond,iroi,istim,:,:,isubroi) = sq(mean(CMI(icond,istim,:,:,subvox),5));
            end
            
            % sort hemisphere data
            for ihemi = 1:nhemi
                hemivox = HemiIndices{ihemi,iroi};
                MIhemi(icond,iroi,istim,:,:,ihemi) = sq(mean(CMI(icond,istim,:,:,hemivox),5));
            end
        end
    end

    % plot for each grouped predictor
    if plotall
        figure('Position',[367 263 1038 706]);
        iter = 1;
        for irow = 1:nrows
            for freq = 1:nfreq
                iroi = rowroi(irow); icon = rowcon(irow);
                midata = sq(MIarea(icon,iroi,istim,:,freq,:));

                HH(iter) = subplot(nrows,nfreq,iter);
                pos = HH(iter).Position;
                boxplot(midata);
                HH(iter).Position = pos;

                if freq == 1
                    txt = sprintf('%s trials',rowlabel{icon});
                    ylabel(txt);
                end

                drawnow;
                iter = iter + 1;
            end
        end

        header = stimlabel{istim};
        ckfiguretitle(header);

        % create to new axes with cortex labels
        position = [0, HH(nfreq+1).Position(2), 0.1, 0.43];
        HA(1) = axes('pos',position);
        txt = [cortices{1} ' cortex'];
        text(0.15,0.5,txt,'FontSize',16,'FontWeight','bold','Rotation',90,'HorizontalAlignment','center');
        HA(1).Visible = 'off';

        position = [0, HH(3*nfreq+1).Position(2), 0.1, 0.43];
        HA(2) = axes('pos',position);
        txt = [cortices{2} ' cortex'];
        text(0.15,0.5,txt,'FontSize',16,'FontWeight','bold','Rotation',90,'HorizontalAlignment','center');
        HA(2).Visible = 'off';


        % save figure
        snamef = sprintf('%sCMI_subroi_%s',DIR.cmi, stimlabel{istim});
        %print('-dpng',snamef);
    end
    
end




%% now create table with main effect hemisphere and subarea results 
% then do statistics

% this table only includes data from V-only trials so far

% MIdata = zeros(ncond,nrois,predlen,nsub,nfreq,nsubareas);

FXarea.icond = [1,2];
FXarea.iroi = [1,2];
FXarea.ipred = [1,2];
FXarea.ifreq = [1,2];

ROInames = {'AC','VC'};
subnames{1} = {'A41/42','TE1.0/1.2','A22c','A22r'}';
subnames{2} = {'mOccG','OPC','iOccG','msOccG'}';
heminames = {'left','right'}';

AreaColNames = {'Region','SubArea','AudCMI1','Chisq_pval1','LipCMI1',...
                'Chisq_pval2','AudCMI2','Chisq_pval3','LipCMI2','Chisq_pval4'};
            
HemiColNames = {'Region','Hemisphere','AudCMI1','Z_pval1','LipCMI1',...
                'Z_pval2','AudCMI2','Z_pval3','LipCMI2','Z_pval4'};


dferr = nsub*nsubroi - nsubroi;
dfcol = nsubroi-1;

AMcomp = [];
LMcomp = [];
ZvalHemi = [];
ChisqArea = [];
PvalsArea = [];

TableAreas = cell(ncond,1);
TableHemispheres = cell(ncond,1);


for icond = FXarea.icond
    
    Chisq = [];
    Pvals = [];
    
    TableAreas{icond} = cell(2*nsubroi,2*4);
    
    % sub area table ------------------------------------------------------
    for freq = FXarea.ifreq 
        for iroi = FXarea.iroi
            freqlabel = sprintf('%.1g-%.1g Hz',cdata.ARG.freq_range(freq,:));
            freqlabel = {freqlabel,[],[],[]}';
            
            
            % CMI data
            AudCMI = sq(MIarea(icond,iroi,1,:,freq,:));
            LipCMI = sq(MIarea(icond,iroi,2,:,freq,:));
            % CMI bootstrap threshold
            AudBoot = sq(SubAreaBootstrap(icond,1,freq,:,iroi));
            LipBoot = sq(SubAreaBootstrap(icond,2,freq,:,iroi));

            % do statistics on CMI data
            [pa,anovatabA,statsA] = kruskalwallis(AudCMI,[],'off');
            [pl,anovatabV,statsL] = kruskalwallis(LipCMI,[],'off');
            
            % compute effect size as epsilon squared for non-parametric test
            H = anovatabA{2,5}; 
            k = anovatabA{2,3} + 1;
            n = anovatabA{4,3} + 1;
            epssq = (H - k + 1)/(n - k);
            EpssqA{icond,iroi,freq} = epssq;
            
            H = anovatabV{2,5}; 
            k = anovatabV{2,3} + 1;
            n = anovatabV{4,3} + 1;
            epssq = (H - k + 1)/(n - k);
            EpssqV{icond,iroi,freq} = epssq;

            if pa < 0.01
                amcomp = multcompare(statsA,'display','off');
                AMcomp{icond,iroi,freq} = amcomp;
            end
            if pl < 0.01
                lmcomp = multcompare(statsL,'display','off');
                LMcomp{icond,iroi,freq} = lmcomp;
            end

            % collect p values to correct later
            Chisq = [Chisq; anovatabA{2,5}, anovatabV{2,5}];
            Pvals = [Pvals; pa, pl];

            % mean across participants
            AudCMI = mean(AudCMI,1)';
            LipCMI = mean(LipCMI,1)';
            
            
            % fill data into cell array
            TableAreas{icond}([1:nsubroi]+(iroi-1)*4, 1+(freq-1)*4) = num2cell(AudCMI);
            TableAreas{icond}([1:nsubroi]+(iroi-1)*4, 3+(freq-1)*4) = num2cell(LipCMI);
            

        end
    end
    
    
    ChisqArea{icond} = Chisq(:);
    PvalsArea{icond} = Pvals(:);
    


    % hemisphere table ----------------------------------------------------
    dferr = nsub*nhemi - nhemi;
    dfcol = nhemi-1;

    Pvals = [];
    Zval = [];
    
    TableHemispheres{icond} = cell(2*nhemi,2*4);

    
    for freq = FXarea.ifreq 
        for iroi = FXarea.iroi 
            freqlabel = sprintf('%.1g-%.1g Hz',cdata.ARG.freq_range(freq,:));
            freqlabel = {freqlabel,[]}';
            

            % CMI data
            AudCMI = sq(MIhemi(icond,iroi,1,:,freq,:));
            LipCMI = sq(MIhemi(icond,iroi,2,:,freq,:));
            % CMI bootstrap threshold
            AudBoot = sq(HemiBootstrap(icond,1,freq,:,iroi));
            LipBoot = sq(HemiBootstrap(icond,2,freq,:,iroi));

            % do statistics
            [pa,~,statsa] = signrank(AudCMI(:,1),AudCMI(:,2));
            [pl,~,statsv] = signrank(LipCMI(:,1),LipCMI(:,2));

            % collect p values to correct later
            Zval = [Zval; statsa.zval, statsv.zval];
            Pvals = [Pvals; pa, pl];

            % mean across participants
            AudCMI = mean(AudCMI,1)';
            LipCMI = mean(LipCMI,1)';
            
            
            % fill data into cell array
            TableHemispheres{icond}([1:nhemi]+(iroi-1)*2, 1+(freq-1)*4) = num2cell(AudCMI);
            TableHemispheres{icond}([1:nhemi]+(iroi-1)*2, 3+(freq-1)*4) = num2cell(LipCMI);
            

            % format numbers wot have two sign. digits
            %C = cellfun(@(x) strip(string(num2str(x,'%.2g'))),{AudCMI,LipCMI},'UniformOutput',false);
            %[AudCMI,LipCMI] = C{:};

            
        end
    end
    
    ZvalHemi{icond} = Zval(:);
    PvalsHemi{icond} = Pvals(:);
    
end

PadjArea = padjust(cat(1,PvalsArea{:}),'benjamini-hochberg');
PadjArea = mat2cell(reshape(PadjArea,[8,2]),8,[1,1]);

PadjHemi = padjust(cat(1,PvalsHemi{:}),'benjamini-hochberg');
PadjHemi = mat2cell(reshape(PadjHemi,[8,2]),8,[1,1]);


% adjust p-values and update in tables
for icond = FXarea.icond
    % get z- and p-values
    padj = PadjArea{icond};
    chisq = ChisqArea{icond};
    
    % format data
    chisq = strip(string(num2str(chisq,'%.2f')));
    padj = strip(string(num2str(padj,'%.2g')));
    stats = join([chisq, padj],' ; ',2);
    stats = reshape(stats,[2,4]);
    % permute rows
    stats = stats(:,[1,3,2,4]);
    
    % substitute table
    TableAreas{icond}(1:nsubroi:end,2:2:end) = cellstr(stats);
    
    
    
    % get z- and p-values
    padj = PadjHemi{icond};
    zval = ZvalHemi{icond};
    
    % format data
    zval = strip(string(num2str(zval,'%.2f')));
    padj = strip(string(num2str(padj,'%.2g')));
    stats = join([zval, padj],' ; ',2);
    stats = reshape(stats,[2,4]);
    % permute rows
    stats = stats(:,[1,3,2,4]);
    
    % substitute table
    TableHemispheres{icond}(1:nhemi:end,2:2:end) = cellstr(stats);
    
    
    % create tables 
    regions = cell(8,1);
    regions([1,5]) = ROInames;
    AreaTable = cat(2,regions,[subnames{1};subnames{2}],TableAreas{icond});
    AreaTable = cell2table(AreaTable,'VariableNames',AreaColNames);
    
    regions = cell(4,1);
    regions([1,3]) = ROInames;
    HemiTable = cat(2,regions,repmat(heminames,2,1),TableHemispheres{icond});
    HemiTable = cell2table(HemiTable,'VariableNames',HemiColNames);
    
    % write table
    warning off
    snamex = '../finalfigures/SubHemiTable.xls';
    AreaName = sprintf('SubAreas_%s',roilabel{icond});
    HemiName = sprintf('Hemisphere_%s',roilabel{icond});
    writetable(AreaTable,snamex,'Sheet',AreaName);
    writetable(HemiTable,snamex,'Sheet',HemiName);
    warning on
    
end

fprintf('everything is ready \n');

