%
% statistics for CMI measure
% compute nonparametric test for each group against time shifted data and
% between uncorrected and corrected MI values
%
% data and p-values are saved in the form of the final figure, so that they
% can be loaded easily
% 
% here we take the mean of MI values across both hemispheres and test this
% mean against randomization
%

close all
clearvars

COMP = 2;
Step0_setup;

% show distributions or not
DISP = false; 

% save name
sname = sprintf('%sfinalfigures/CMI_Statistics_v3.mat',DIR.main);
testdir = 'C:\Users\fbroehl\Documents\FB02\stats\cmi\';

% directories and save name
snamebase = sprintf('%sCMI_meanlag',DIR.cmi);
conditions = {'auditory','visual'};

% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);
goodsub = find([SUBS.exclude]==0);
nsub = length(goodsub);
nfreq = 5;
nreps = 2000;
nrand = 5000; % MI cMI diff test randomization
nstim = 2; % MI( ENV ) and MI( LIP ) !
nvar = 3; % variants of CMI measures

% sizes
nrows = 4;
nhemi = 2;
nroi = 2;

blocks = [1:4; 9:12];
ncond = size(blocks,1);

% correct for multiple comparisons
multcompcorr = true;
ARG.multcompcorr = multcompcorr;

ARG.nsub = nsub;
ARG.nfreq = nfreq;
ARG.nreps = nreps;
ARG.nstim = nstim;
ARG.nvar = nvar;
ARG.nrows = nrows;

% get sourceatlas
load sourceatlas_grid.mat
load('ROIVOXELS.mat','roivoxins');
load pROI_voxel.mat
nvox = length(pROI.all.inroivoxins);
allvox = pROI.all.inroivoxins;

% unpack roi voxel indices
leftA = pROI.auditory.left.inroivoxins;
rightA = pROI.auditory.right.inroivoxins;
leftV = pROI.visual.left.inroivoxins;
rightV = pROI.visual.right.inroivoxins;
% intersect with ROI voxel mat
[~, leftA] = intersect(pROI.all.inroivoxins,leftA);
[~, rightA] = intersect(pROI.all.inroivoxins,rightA);
[~, leftV] = intersect(pROI.all.inroivoxins,leftV);
[~, rightV] = intersect(pROI.all.inroivoxins,rightV);

% combined hemisphere roi voxels
audvox = [leftA; rightA];
visvox = [leftV; rightV];
roivoxcell = {audvox, visvox};


% get template MRI
mri = ft_read_mri(templateT1);
mri = ft_convert_units(mri, 'cm');


%% load data
CMI = zeros(ncond,nstim,nvar,nsub,nfreq,nvox);
% randomized data
CMIRand = zeros(ncond,nstim,nsub,nfreq,nroi,nhemi,nreps);

fprintf('load cmi data \n');
for icond = 1:ncond
    for isub2 = 1:length(goodsub)
        isub = goodsub(isub2);
        
        %filename = sprintf('%sOptlag_window_CondMI_sub%02d_%d_%d.mat', ...
        %    testdir, isub, blocks(icond,1), blocks(icond,4) );
        
        condlabel = conditions{icond};
        filename = sprintf('%s_%s_sub%02d.mat', snamebase,condlabel,isub);
        data = load(filename);
        % ENV
        CMI(icond,1,1,isub2,:,:) = data.CMI_B1_E;
        CMI(icond,1,2,isub2,:,:) = sq(mean(data.CMI_C1_E,3));
        CMI(icond,1,3,isub2,:,:) = sq(mean(data.CMI_C2_E,3));
        % LIP
        CMI(icond,2,1,isub2,:,:) = data.CMI_B1_L;
        CMI(icond,2,2,isub2,:,:) = sq(mean(data.CMI_C1_L,3));
        CMI(icond,2,3,isub2,:,:) = sq(mean(data.CMI_C2_L,3));
        
        % store means over ROI and hemi voxels for each random draw!
        % MI( Aud ) at istim==1
        CMIRand(icond,1,isub2,:,1,1,:) = sq(mean(data.CMI_C2_E(:,leftA,:),2));
        CMIRand(icond,1,isub2,:,1,2,:) = sq(mean(data.CMI_C2_E(:,rightA,:),2));
        CMIRand(icond,1,isub2,:,2,1,:) = sq(mean(data.CMI_C2_E(:,leftV,:),2));
        CMIRand(icond,1,isub2,:,2,2,:) = sq(mean(data.CMI_C2_E(:,rightV,:),2));
        
        % MI( Vis ) at istim==2
        CMIRand(icond,2,isub2,:,1,1,:) = sq(mean(data.CMI_C2_L(:,leftA,:),2));
        CMIRand(icond,2,isub2,:,1,2,:) = sq(mean(data.CMI_C2_L(:,rightA,:),2));
        CMIRand(icond,2,isub2,:,2,1,:) = sq(mean(data.CMI_C2_L(:,leftV,:),2));
        CMIRand(icond,2,isub2,:,2,2,:) = sq(mean(data.CMI_C2_L(:,rightV,:),2));
    end
end

freqrange = data.ARG.freq_range;


% labels
cortices = {'auditory', 'visual'};
labelpart = {'left','right'}; % first left then right hemi data
stimlabel = {'Aud', 'Vis'};
Trials = {'A-only', 'V-only'};


%% create maximum distribution for each freq band 
% used to correct for multiple comparisons
fprintf('compute maximum distributions \n');
tic

% distributions against zero, mean across hemispheres
CMIRandmultcomp = permute(CMIRand,[1,2,3,5,6,4,7]);
CMIRandmultcomp = sq(median(mean(CMIRandmultcomp,5),3));
CMIRandmultcomp = reshape(CMIRandmultcomp,[ncond*nstim*nroi,nfreq,nreps]);
CMIRandmultcomp = sq(max(CMIRandmultcomp,[],1));

% get mean and std from random dist to z-score true MI values
Distrand  = sq(mean(CMIRand,6));
DistrandM = mean(Distrand,6);
DistrandS = std(Distrand,[],6);

% create max. dist. for difference between uncorrected (C1) and corrected
% (B1) MI values
randlabel = 2.*randi([0,1],nsub,nrand) - 1;
CMIpartialmultcomp = zeros(ncond,nroi,nstim,nfreq,nrand);
for icond = 1:ncond
    for iroi = 1:nroi
        for freq = 1:nfreq
            for istim = 1:nstim
                mivardiff = sq(mean(CMI(icond,istim,[1:2],:,freq,roivoxcell{iroi}),6))';
                
                for draw = 1:nrand
                    % permute group labels
                    midiff = diff(mivardiff,[],2) .* randlabel(:,draw);
                    [~,~,stats] = signrank(midiff);
                    CMIpartialmultcomp(icond,iroi,istim,freq,draw) = stats.zval;
                end
            end
        end
    end
end
CMIpartialmultcomp = reshape(CMIpartialmultcomp,[ncond*nroi*nstim*freq,nrand]);
CMIpartialmultcomp = sq(max(CMIpartialmultcomp,[],1));


toc


%% statistics

% matrix to save CMI data for plotting in another script
% size is sorted by the arrangement in the figure (2 figures for each
% cortex, 4 rows and 5 freq bands)
CMIdata = zeros(nrows,nfreq,nsub,2*nstim);
CMIdataDimOrd = {'Rows (ENV-A, ENV-V, LIPS-A, LIPS-V)', ...
    'Frequency', 'NSub','Hemisphere (left, right, left, right'};

CMIrandM = zeros(nrows,nfreq,nsub,2*nstim);
CMIrandS = zeros(nrows,nfreq,nsub,2*nstim);

% matrix variables for pvalues for tests between MI and CMI or between 
% variables and zero
PvalZero = zeros(nrows,nfreq,2*nstim);
PvalMI_CMI = zeros(nrows,nfreq,nstim);
RandPrctile = prctile(CMIRandmultcomp,99,2);
Zval = zeros(nrows,nfreq,nstim);


% ROI per row
rowroi = {'Auditory','Auditory','Visual','Visual'};
rowroivox = [1,1,2,2];
% Condition per row
rowcon = [1,2,1,2];

ARG.rowroi = rowroi;
ARG.rowcon = Trials(rowcon);

tic
for irow = 1:nrows
    dataL = []; dataR = [];
    for freq = 1:nfreq 
        % get data for left and right hemisphere
        roiname = rowroi{irow};
        roivox = roivoxcell{rowroivox(irow)};
        % get correct condition
        icond = rowcon(irow);
        
        
        
        % get model data, both hemispheres together
        MIdata = sq(mean(CMI(icond,:,[2,1],:,freq,roivox),6));
        
        % sort columns -> [full Aud, part Aud, full Vis, part Vis]
        MIdata = reshape(permute(MIdata,[2,1,3]),[nstim*(nvar-1),nsub])';
        
        % sort mean and std from random dist.
        CMIrandM(irow,freq,:,:) = sq(DistrandM(icond,[2,2,1,1],:,freq,rowroivox(irow)))';
        CMIrandS(irow,freq,:,:) = sq(DistrandS(icond,[2,2,1,1],:,freq,rowroivox(irow)))';
        
        % compare each column against time shuffled MEG data ------
        % i.e. test against zero (with bias)

        % shuffled data nsub x nreps
        if multcompcorr == true
            MIRand = CMIRandmultcomp(freq,:)';
        else
            MIRand = sq(median(CMIRand(icond,istim,:,freq,cortex,:,:),3))';
            MIRand = repmat(MIRand,[1,2]);
        end

        % compute p value and save
        pval = nansum(MIRand > median(MIdata,1))./nreps;
        pval(pval==0) = 1/nreps;
        PvalZero(irow,freq,:) = pval;
        
        
        
        
        % compare MI against conditional MI values ------------
        % uncorrected minus corrected MI values (columns already sorted
        % above)
        midiff = MIdata(:,[1:2:end]) - MIdata(:,[2:2:end]); % C1 - B1
        zval = [];
        for ii = 1:size(midiff,2)
            [~,~,stats] = signrank(midiff(:,ii));
            zval(ii) = stats.zval;
        end
        Zval(irow,freq,:) = zval;
        
        % create bootstrap difference data 
        if multcompcorr == true
            miboot = CMIpartialmultcomp';
        else
            % generate dist for each single test
            miboot = zeros(nrand,1);
            for draw = 1:nrand
                randix = randi([1,nrand],2,1);
                miboot(draw,:) = median(MIrandom(:,randix(1)) - MIrandom(:,randix(2)));
            end
        end

        % compute p value and save
        pval = nansum(miboot > zval)./nrand;
        pval(pval==0) = 1/nrand;
        PvalMI_CMI(irow,freq,:) = pval;
        
        
        % store in matrix for saving ------------------------------
        % Bias as the mean across time shifts
        CMIdata(irow,freq,:,:) = MIdata; % subtract the bias before saving

    end
end
toc

CMIdataM = mean(CMIdata,3);
CMIdataS = std(CMIdata,[],3);


% compute cohen's d with maximum distribution
Mmcomp = mean(CMIRandmultcomp,2);
Smcomp = std(CMIRandmultcomp,[],2);
% compute a z score as an effect size
z_score = (mean(CMIdata,3) - repmat(Mmcomp',[nrows,1,1,2*nstim])) ./ std(CMIdata,[],3);

% save
dataARG = data.ARG;
save(sname, 'dataARG','ARG', 'CMIdata', 'CMIdataDimOrd', 'PvalZero', ...
    'PvalMI_CMI', 'Zval', 'freqrange', 'RandPrctile', 'CMIrandM', 'CMIrandS', ...
    'CMIdataM', 'CMIdataS', 'z_score');

fprintf('%s \n', sname);




%% get peak CMI voxel in ROIs

% dimension order
% CMI = zeros(ncond,nstim,nvar,nsub,nfreq,nvox);

% peaks are [condition x stimulus]
freq = 1;
CMImap = sq(median(CMI(:,:,2,:,freq,:),4));

% find auditory and visual ROIs in left and right hemisphere
[~,peakAR] = max(CMImap(:,:,rightA),[],3);
[~,peakAL] = max(CMImap(:,:,leftA),[],3);
[~,peakVR] = max(CMImap(:,:,rightV),[],3);
[~,peakVL] = max(CMImap(:,:,leftV),[],3);

% project onto roivoxins
peakAR = roivoxins(allvox(rightA(peakAR)));
peakAL = roivoxins(allvox(leftA(peakAL)));
peakVR = roivoxins(allvox(rightV(peakVR)));
peakVL = roivoxins(allvox(leftV(peakVL)));

Peaks.sourcemodel = {peakAR, peakAL, peakVR, peakVL};


% to get these grid points in the pROI structure, use:
[~,peakARp] = ismember(peakAR,roivoxins(allvox));
[~,peakALp] = ismember(peakAL,roivoxins(allvox));
[~,peakVRp] = ismember(peakVR,roivoxins(allvox));
[~,peakVLp] = ismember(peakVL,roivoxins(allvox));


Peaks.pROI = {peakARp,peakALp,peakVRp,peakVLp};

% save to file
sname = 'peakCMIindices.mat';
save(sname,'Peaks');




%% code to check out the bootstrap distribution

if DISP 
    figure
    
    for freq = 1:nfreq
        subplot(nfreq,1,freq);
        hist(CMIdiffmultcomp(freq,:),size(CMIdiffmultcomp,2));
        header = sprintf('%.2f - %.2f Hz', freqrange(freq,:));
        title(header);
    end
    ckfiguretitle('bootstrap dist. optimal lag')
end


