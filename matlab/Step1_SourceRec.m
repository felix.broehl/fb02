%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% preprocess MEG data, cut to envelope length and source reconstruct
% then only keep roi voxels and save to file
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clearvars


% directories
COMP = 2; % set environment to local windows machine
Step0_setup;

% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);


% parameters --------------------------------------------------------------
ARG.new_fs = 50; % resample after preprocessing
ARG.lpfilt = [];
ARG.hpfilt = [];

nblockexp = 9:12; % only visual blocks
goodsub = find([SUBS.exclude]==0);
nsubs = length(goodsub);
skip = 2; % if skip is 2, then skript only creates data not already on disk

% 1) only cortical areas of interest
%    roivoxel = find(ismember(sourceatlas.tissue,roisel));
%    [~,roivoxins]=intersect(find(sourcemodel.inside),roivoxel);
% 2) all ingrid voxels excl. cerebellum (atlas 21,22, 27,28, 71-78, 91 -116) and above a certain z-threshold
load('sourceatlas_grid.mat');
load('ROIVOXELS.mat','roivoxins');
nvoxfinal=length(roivoxins);

% saving memory and disk space with only ROI voxels
[~,roiIdx] = intersect(find(sourcemodel.inside),roivoxins);


%% MANUAL TARGET PROJECTION (for all voxels)
% load all blocks from one condition/project/smooth

% get envelope data
ename = sprintf('SpeechEnvelopes_9_%dfs.mat',ARG.new_fs);
envdir = [DIR.env, ename];
if exist(envdir)==2
    load(envdir);
else
    fprintf('No Envelope data found!!! \n\n');
    return
end

ifilt = 2; % for filter loading


for isub2 = 1:nsubs
    isub = goodsub(isub2);
    for iblock = nblockexp % only auditory blocks
        % check whether file alredy exists...
        xname = sprintf('%ssourcespace_sub%d_block%d_A.mat', DIR.source, isub, iblock);
        if exist(xname)==skip
            fprintf('%s \n',xname);
            continue;
        end
        
        tic
        % load data and log files
        fprintf('loading sub %d block %d \n\n', isub, iblock);
        load(sprintf('%sA_sub%d_block%d_clean',DIR.pre,isub,iblock),'data');
        log = load(sprintf('%s%s_%s_S%d_%s.mat',DIR.log,expname,filenames{1,iblock},isub,filenames{2,iblock}));   % load log file
        filt = load(sprintf('%sfilt_lcmv%d_%d_%d',DIR.flt,ifilt,isub,iblock),'filt','filtinfo');
        
        ntrl = length(data.trial);
        
        % preprocessing ---------------------------------------------------
        % highpass filter (0.8 Hz) before source projection
        cfg             = [];
        cfg.hpfilter    = 'yes';
        cfg.hpfreq      = 0.8;
        cfg.hpfilttype  = 'but';
        cfg.hpfiltord   = 4;
        cfg.hpfiltdir   = 'twopass';
        cfg.detrend     = 'no';
        % data            = ft_preprocessing(cfg, data);
        % ARG.hpfilt = cfg.pfreq;

        % lowpass filter (30 Hz) before source projection
        cfg             = [];
        cfg.lpfilter    = 'yes';
        cfg.lpfreq      = 30;
        cfg.lpfilttype  = 'but';
        cfg.lpfiltord   = 4;
        cfg.lpfiltdir   = 'twopass';
        cfg.detrend     = 'no';
        data            = ft_preprocessing(cfg, data);
        ARG.lpfilt = cfg.lpfreq;
        
        % downsample data to new_fs
        cfg = [];
        cfg.resamplefs = ARG.new_fs;
        data = ft_resampledata(cfg, data);
        
        % cut out shorter segments
        cfg = [];
        cfg.toilim = [0 8.5]; % max stimlength 8.24 s
        data = ft_redefinetrial(cfg, data);
        
        
        % fit trials to log -----------------------------------------------
        reidx = data.trialinfo(:,4);
        cond = log.ARG.cat; % 'num' or 'adj'
        cond = find(contains(Nambase,cond)); % get conditions
        Env.Each = {SpeechEnvelopes.Each{cond}{reidx}}; % correctly sorted envelopes
        Env.Mean = {SpeechEnvelopes.Mean{cond}{reidx}};
        
        % numerically increase values for beamforming
        data2 = cat(3, data.trial{:});
        data2 = permute(data2, [3,1,2]);
        parfor k = 1:ntrl
            data2(k,:,:) = data2(k,:,:) * 1e12;
        end
        
        
        % source projection -----------------------------------------------
        % each trial has now different length!
        nvox = size(filt.filt,1);
        ntime = size(data2,3);
        source_raw = zeros(ntrl,nvox,ntime);
        weights = filt.filt;
        parfor k = 1:ntrl
            source_raw(k,:,:) = weights * sq(data2(k,:,:));
        end
        source_raw = source_raw(:,roiIdx,:);
        
        
        % 3D smoothing data -----------------------------------------------
        % maybe.
        % maybe..
        % maybe...
        
        % z-score each voxel
        for k = 1:ntrl
            for ivox = 1:nvoxfinal
                source_raw(k,ivox,:) = zscore(source_raw(k,ivox,:));
            end
        end
        
        toc
        fprintf('done with source projection! \n\n');
        
        % save one source reconstructed file per participant and block
        ARG.trialinfo = data.trialinfo;
        sname = sprintf('%ssourcespace_sub%d_block%d_A', DIR.source, isub, iblock);
        save(sname, 'source_raw', 'Env', 'ARG');
    
    end
end


