% set directories depending on computer

% get root where project folders are located
mfile = mfilename('fullpath');
[root,~,~] = fileparts(mfile);
root = root(1:end-6); % cut off '\matlab'

if COMP==1
    
    DIR.main= root;
    DIR.log = [DIR.main 'logs/'];
    DIR.tgt = [DIR.main 'targets/'];
    DIR.pgm = [DIR.main 'pgm/'];
    DIR.mat = [DIR.main 'matlab/'];
    
    DIR.pre = [DIR.main 'data/'];
    DIR.flt = [DIR.main 'filt/'];
    DIR.env = [DIR.main 'StimInfo/processed/'];
    DIR.seg = [DIR.main 'StimInfo/Segmentation/'];
    DIR.source = [DIR.main 'source_rec/'];
    DIR.allf = [DIR.main 'stats/allf/'];
    DIR.allfcf = [DIR.main 'stats/allfcf/'];
    DIR.mi = [DIR.main 'stats/mi/'];
    DIR.micf = [DIR.main 'stats/micf/'];
    DIR.strf = [DIR.main 'stats/strf_models/'];
    DIR.lipt = [DIR.main 'stats/lipt/'];
    DIR.liptcf = [DIR.main 'stats/liptcf/'];
    DIR.proi = [DIR.main 'stats/proi/'];
    DIR.cmi = [DIR.main 'stats/cmi/'];
    DIR.ecmi = [DIR.main 'stats/ecmi/'];
    DIR.cmicf = [DIR.main 'stats/cmicf/'];
    DIR.glm = [DIR.main 'stats/glmfit/'];
    DIR.dfi = [DIR.main 'stats/dfi/'];
    
    % add ck toolbox
    addpath(genpath('/home/fbroehl/FB02/toolboxes/ckmatlab/'));
    % add fieldtrip
    addpath('/home/fbroehl/FB02/toolboxes/fieldtrip-20190905/');
    ft_defaults; % -> really important!
    % add chimera
    addpath('/home/fbroehl/FB02/toolboxes/chimera')
    % add mTRF toolbox
    addpath(genpath('/home/fbroehl/FB02/toolboxes/mTRF-Toolbox-master'));
    % add utilities
    addpath(genpath('/home/fbroehl/FB02/toolboxes/fb_utils'));
    % add functions from stimanalysis
    addpath([DIR.main 'StimInfo/stimanalysis/']);

    
elseif COMP==2

    DIR.main = root;
    DIR.log = [DIR.main 'logs\'];
    DIR.tgt = [DIR.main 'targets\'];
    DIR.pgm = [DIR.main 'pgm\'];
    DIR.ft = 'Y:\Matlab\fieldtrip-20190905\';
    DIR.pre = [DIR.main 'data\'];
    DIR.flt = [DIR.main 'filt\'];
    DIR.env = [DIR.main 'StimInfo\processed\'];
    DIR.seg = [DIR.main 'StimInfo\Segmentation\'];
    DIR.source = [DIR.main 'source_rec\'];
    DIR.allf = [DIR.main 'stats\allf\'];
    DIR.allfcf = [DIR.main 'stats\allfcf\'];
    DIR.mi = [DIR.main 'stats\mi\'];
    DIR.micf = [DIR.main 'stats\micf\'];
    DIR.strf = [DIR.main 'stats\strf_models\'];
    DIR.lipt = [DIR.main 'stats\lipt\'];
    DIR.liptcf = [DIR.main 'stats\liptcf\'];
    DIR.proi = [DIR.main 'stats\proi\'];
    DIR.cmi = [DIR.main 'stats\cmi\'];
    DIR.ecmi = [DIR.main 'stats\ecmi\'];
    DIR.cmicf = [DIR.main 'stats\cmicf\'];
    DIR.glm = [DIR.main 'stats\glmfit\'];
    DIR.dfi = [DIR.main 'stats\dfi\'];
    
    sourcemod = 'Y:\Matlab\fieldtrip-20190905\template\sourcemodel\standard_sourcemodel3d6mm';
    templateT1='Y:\Matlab\fieldtrip-20190905\template\anatomy\single_subj_T1_1mm.nii';
    atlasdir ='Y:\Matlab\fieldtrip-20190905\template\atlas\aal\ROI_MNI_V4.nii';

    
    %DIR.mat = 'E:\CKDATA\Projects\projects\Multisensory_decoding\Keitel.eLife.19\matlab';
    
    addpath(genpath('Z:\FB00\fb_utils'));
    % add functions from stimanalysis
    addpath([DIR.main 'StimInfo\stimanalysis\']);
    
end

%addpath(DIR.mat)
allconditions = {'auditory' 'audiovisual' 'visual'};

