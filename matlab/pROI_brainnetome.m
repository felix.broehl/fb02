function [pROI,atlaslabel,atlastissue,intrptissue] = pROI_brainnetome()
%  use BRAINNETOME atlas to define ROIs for auditory and visual areas

% brain structure data
path_to_headmodel = 'Y:\Matlab\fieldtrip-20190905\template\headmodel\standard_mri.mat';
path_to_atlas = 'Y:\Matlab\fieldtrip-20190905\template\atlas\brainnetome\BNA_MPM_thr25_1.25mm.nii';

% our meg grid
load('sourceatlas_grid.mat')
load('ROIVOXELS.mat','roivoxins');

% define ROI structure from brainnetome atlas
pROI.auditory.right.label = [71:2:75, 79];
pROI.auditory.left.label = pROI.auditory.right.label + 1; % next in list
pROI.auditory.label = [ pROI.auditory.left.label, pROI.auditory.right.label ];

pROI.visual.right.label = [199, 203:2:207];
pROI.visual.left.label = pROI.visual.right.label + 1;
pROI.visual.label = [ pROI.visual.left.label, pROI.visual.right.label ];

primaryroilabels = [pROI.auditory.left.label pROI.auditory.right.label ...
    pROI.visual.left.label pROI.visual.right.label];


load(path_to_headmodel)
atlas1 = ft_read_atlas(path_to_atlas);
atlaslabel = atlas1.tissuelabel;
atlastissue = atlas1.tissue;

pROI.primaryroilabels = primaryroilabels;
pROI.primaryroinames = atlas1.tissuelabel(primaryroilabels)';

cfg = [];
cfg.parameter  = 'tissue';
cfg.interpmethod = 'nearest';
atlas2 = ft_sourceinterpolate(cfg, atlas1 , sourcemodel);
intrptissue = atlas2.tissue;


% auditory areas
primaryroivox = find(ismember(atlas2.tissue,pROI.auditory.left.label));
% select primary areas from constrained voxel set
[~,lACinroivoxins,~] = intersect(roivoxins, primaryroivox);

primaryroivox = find(ismember(atlas2.tissue,pROI.auditory.right.label));
% select primary areas from constrained voxel set
[~,rACinroivoxins,~] = intersect(roivoxins, primaryroivox);

% visual areas
primaryroivox = find(ismember(atlas2.tissue,pROI.visual.left.label));
% select primary areas from constrained voxel set
[~,lVCinroivoxins,~] = intersect(roivoxins, primaryroivox);

primaryroivox = find(ismember(atlas2.tissue,pROI.visual.right.label));
% select primary areas from constrained voxel set
[~,rVCinroivoxins,~] = intersect(roivoxins, primaryroivox);



% find voxel in whole brain
primaryroivox = find(ismember(atlas2.tissue,primaryroilabels));
% select primary areas from constrained voxel set
[~,primaryinroivoxins,~] = intersect(roivoxins, primaryroivox);

% collect intersect arrays in pROI structure, that we use in other scripts
pROI.auditory.left.inroivoxins = lACinroivoxins;
pROI.auditory.right.inroivoxins = rACinroivoxins;
pROI.visual.left.inroivoxins = lVCinroivoxins;
pROI.visual.right.inroivoxins = rVCinroivoxins;
pROI.all.labels = primaryroilabels;
pROI.all.inroivoxins = primaryinroivoxins;


% -> use primaryinroivoxins to select primary cortices from the roivoxins
% data

save('pROI_voxel.mat','pROI');

end
