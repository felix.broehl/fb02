function out = createFeatures_v3(cfg,DIR)
%out = createFeatures(cfg,DIR)
% this function creates on structure containing all relevant features that
% we want to track
% 
%
% and structure containing sample rate -> cfg.srate
%
% input: expects structure with paths to the directories where files are
% stored -> DIR
%
%
% output: structure containing all sorted features
%   Features
%        -  adj  -  sentence1  -  feat x time matrix
%                   sentence2
%                   ...
%        -  num  -  sentence1
%                   ...
%
%   NsingleFeat
%       number of features
%   FeatureLabels
%       names
%   featIdx
%       indexes which features are independent (some could be multidim.)
%   continuous
%       which are continuous in time and can be filtered
%   nambse
%       sorted by condition ('adj' or 'num')
%
%  features are:
%       - broadband envelope
%       - broadband envelope slope
%       - low subband envelope
%       - mid subband envelope
%       - high subband envelope
%       - pitch (f0)
%       - f1
%       - Ellipse_x
%       - Ellipse_y
%       - area
%       - dist_x
%       - dist_y
%       - Frame Distance
%

% handle input
if ~isfield(cfg,'srate') || isempty(cfg.srate)
    cfg.srate = 50;
end
if ~isfield(cfg,'ArtPCA') || isempty(cfg.ArtPCA)
    cfg.ArtPCA = false;
end
if ~isfield(cfg,'PCAminvar') || isempty(cfg.PCAminvar)
    cfg.PCAminvar = 95;
end


nambase = {'adj', 'num'};


% load visual stim data once at the beginning
envname = sprintf('%sSpeechEnvelopes_12_%dfs.mat',DIR.env,cfg.srate);
Envelopes = load(envname);

% load visual stim data once at the beginning
stimname = sprintf('%sStimMat_%d_fs.mat',DIR.env,cfg.srate);
Stimuli = load(stimname);

% create unique phoneme corpus from segmented text
corpus = PhonemeCorpus(); % pass DIR to return phoneme array

for base = 1:length(nambase)
    nbase = nambase{base};

    % load visual stim data once at the beginning
    segname = sprintf('%sSegmented_%s.mat',DIR.seg,nbase);
    SegText = load(segname);
    

    % create stim struc ---------------------------------------------------
    for k = 1:90
        % get preprocessed features
        env = Envelopes.SpeechEnvelopes.Mean{base}{k};
        out.aud_env.data{base}{k} = env;
        out.aud_env.continuous = true;
        out.aud_env.ndims = size(env,1);
        
        % envelope slope
        maxlen = length(env);
        slope = diff(env);
        slope = [slope, slope(end)]; % restore same length
        out.aud_slope.data{base}{k} = slope;
        out.aud_slope.continuous = true;
        out.aud_slope.ndims = size(slope,1);
        
        % pitch and f1
        lipfeats = Stimuli.Stimuli{base}{k}([1,2,9:12],:);
        out.aud_pitch.data{base}{k} = lipfeats(1,:);
        out.aud_pitch.continuous = true;
        out.aud_pitch.ndims = 1;
        out.aud_f1.data{base}{k} = lipfeats(2,:);
        out.aud_f1.continuous = true;
        out.aud_f1.ndims = 1;
        
        % lip features
        out.lip_area.data{base}{k} = lipfeats(3,:);
        out.lip_area.continuous = true;
        out.lip_area.ndims = 1;
        out.lip_width.data{base}{k} = lipfeats(4,:);
        out.lip_width.continuous = true;
        out.lip_width.ndims = 1;
        out.lip_height.data{base}{k} = lipfeats(5,:);
        out.lip_height.continuous = true;
        out.lip_height.ndims = 1;
        out.FrameDist.data{base}{k} = lipfeats(6,:);
        out.FrameDist.continuous = true;
        out.FrameDist.ndims = 1;
        
        liparea = out.lip_area.data{base}{k};
        slope = diff(liparea);
        slope = [slope, slope(end)];
        out.lip_slope.data{base}{k} = slope;
        out.lip_slope.continuous = true;
        out.lip_slope.ndims = 1;
        
        
        % create phoneme index array from timing --------------------------
        Phonemes = SegText.SegmentedText{k}.Phones;
        phTiming = round(SegText.SegmentedText{k}.Phone_timing .* cfg.srate);
        NPhones = length(phTiming);
        phIdx = zeros(1,maxlen);
        % create indexing vector to phoneme cell array
        for iPhone = 1:NPhones
            phIdx(1,[phTiming(iPhone,1):phTiming(iPhone,2)]) = iPhone;
        end
        
        
        % fill cell array with associated ARPAbet phoneme
        phArray = cell(1,length(phIdx));
        for point = 1:length(phIdx)
            idx = phIdx(point);
            if idx ~= 0 % if segment is not silent
                tmp = char(Phonemes(idx));
                % cut stressing from phoneme
                if length(tmp) > 2
                    tmp = tmp(1:2);
                end
                phArray{1,point} = tmp;
            end
        end
        
        
        % Phoneme information -----------------
        % transform phonemes into class labels
        phoneClass = Phone2Class(phArray,corpus,2);
        out.phoneClass.data{base}{k} = phoneClass;
        out.phoneClass.continuous = false;
        out.phoneClass.ndims = size(phoneClass,1);
        
        % Viseme information ------------------
        visemeClass = Phone2Viseme(phArray)';
        out.visemeClass.data{base}{k} = visemeClass;
        out.visemeClass.continuous = false;
        out.visemeClass.ndims = size(visemeClass,1);
        
        % transforms phoneme cell array to articulatory feature space
        atrlist = fb_attribute2phoneme([],'list');
        ndim_art = length(atrlist);
        ArtNaplib = zeros(ndim_art,length(phArray));
        atr = fb_phoneme2attribute(phArray); % faster
        for iphn = 1:length(atr)
            % convert to index
            if ~isempty(atr{iphn})
                idn = ismember(atrlist,atr{iphn});
                ArtNaplib(:,iphn) = idn;
            end 
        end
        
        out.phoneArt.data{base}{k} = ArtNaplib;
        out.phoneArt.continuous = false;
        out.phoneArt.ndims = ndim_art;
        
    end

end

% return some info
out.ncond = length(nambase);
out.nambase = nambase;
out.nfiles = [90, 90];
out.srate = cfg.srate;

% reduce dimensions of art features
if cfg.ArtPCA == true
    artdata = [out.phoneArt.data{:}];
    condlen = cellfun(@length, artdata);
    artdata = [artdata{:}]';
    
    % pca fit
    [~,score,~,~,explained] = pca(artdata);
    minvaridx = sum(cumsum(explained) <= cfg.PCAminvar) + 1;
    score = score(:,1:minvaridx)';
    score = mat2cell(score,minvaridx,condlen);
    out.phoneArt.data{1} = score(1:90);
    out.phoneArt.data{2} = score(91:end);
    out.phoneArt.ndims = minvaridx; % update n dimensions
end


end
