function [subs, strcond, filenames]=subject_database(expname)

ctrlexpname='AVentrain';
if ~strcmp(expname,ctrlexpname)
    error('FAILSAFE: Subject information does not correspond to Experiment!\n')
end
% subject database

strcond = {'aud1' 'aud2' 'aud3' 'aud4' 'av1' 'av2' 'av3' 'av4' 'vid1' 'vid2' 'vid3' 'vid4' 'N100_1' 'targ' 'N100_2' 'rest' 'empt1' 'empt2'};
filenames = {'auditory' 'auditory' 'auditory' 'auditory' 'audiovisual' 'audiovisual' 'audiovisual' 'audiovisual' 'visual' 'visual' 'visual' 'visual'; ...
    'B1' 'B2' 'B3' 'B4' 'B1' 'B2' 'B3' 'B4' 'B1' 'B2' 'B3' 'B4'};
%------------------------
% SUB 1
subidx = 1;
subs(subidx).hash = 'CVN17';
subs(subidx).gender = 'male';
subs(subidx).age = 30;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-07-26@1536' '16-07-28@0957'};
subs(subidx).mri = 'CVN17_RAW_32_ADNI_\CVN17.MR.SEQUENCEREGION_PASCAL.0002.0001.2013.10.25.16.02.08.219175.462940.IMA';
subs(subidx).comments = 'no eye tracking data; check audio timing';
subs(subidx).eye = 'no';
subs(subidx).exclude = 0;

% scan sess run session
subs(subidx).runs = [1 1 3 1; ...     % aud1
    1 1 4 1; ...                      % aud2
    1 1 5 1; ...                      % aud3
    1 1 6 1; ...                      % aud4
    1 1 7 1; ...                      % av1
    1 1 8 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4
    1 1 9 1; ...                      % N1001
    
    1 2 7 2; ...                      % targ         
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest
    
    1 1 10 NaN; ...                   % empt1
    1 2 9 NaN];                       % empt2

%------------------------
% SUB 2
subidx = 2;
subs(subidx).hash = 'JHN25';
subs(subidx).gender = 'male';
subs(subidx).age = 29;
subs(subidx).scan = {'0127a04AK' '0127b04AK' '0127c04AK'};
subs(subidx).sess = {'16-08-03@1332' '16-08-03@1458' '16-08-08@1405' '16-08-08@1634'};
subs(subidx).mri = 'JHN25_RAW_32_ADNI\JHN25.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.01.27.10.48.36.670409.9992262.IMA';
subs(subidx).comments = 'Eye not properly tracked. Check audio timing. Weird topographies (v frontal).';
subs(subidx).eye = 'no';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    2 2 1 1; ...                      % aud2
    2 2 2 1; ...                      % aud3
    2 2 3 1; ...                      % aud4
    2 2 4 1; ...                      % av1
    2 2 5 1; ...                      % av2
    
    2 3 1 2; ...                      % av3
    2 3 2 2; ...                      % av4
    2 3 3 2; ...                      % vid1
    2 3 4 2; ...                      % vid2
    2 3 5 2; ...                      % vid3
    2 3 6 2; ...                      % vid4
    2 2 6 1; ...                      % N1001
    
    3 4 1 2; ...                      % targ
    3 4 1 2; ...                      % N1002
    3 4 2 2; ...                      % rest
    
    2 2 7 NaN; ...                    % empt1
    3 4 3 NaN];                       % empt2

%------------------------
% SUB 3
subidx = 3;
subs(subidx).hash = 'MMN28';
subs(subidx).gender = 'male';
subs(subidx).age = 25;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-08-04@1111' '16-08-11@1042' '16-08-11@1229' '16-08-11@1253'};
subs(subidx).mri = 'MMN28_RAW_32_ADNI\MMN28.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.08.04.14.06.07.793656.41483334.IMA';
subs(subidx).comments = 'eye not properly tracked; check audio timing';
subs(subidx).eye = 'no';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 3 1 2; ...                      % vid4
    
    1 1 7 1; ...                      % N1001
    
    1 4 1 2; ...                      % targ
    1 4 2 2; ...                      % N1002
    1 4 3 2; ...                      % rest
    
    1 1 8 NaN; ...                    % empt1
    1 4 4 NaN];                       % empt2

%------------------------
% SUB 4
subidx = 4;
subs(subidx).hash = 'CAA04';
subs(subidx).gender = 'female';
subs(subidx).age = 32;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-08-09@1337' '16-08-16@1417' '16-08-16@1503' '16-08-16@1535'};
subs(subidx).mri = 'CAA04_RAW_32_ADNI\CAA04.MR.SEQUENCEREGION_PASCAL.0004.0001.2013.04.03.14.43.53.212722.90424414.IMA';
subs(subidx).comments = 'eye not properly tracked; check audio timing';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    1 1 7 1; ...                      % av3
    
    1 2 1 2; ...                      % av4
    1 2 2 2; ...                      % vid1
    1 2 3 2; ...                      % vid2
    1 3 1 2; ...                      % vid3
    1 3 2 2; ...                      % vid4
    
    1 1 8 2; ...                      % N1001
    
    1 4 1 2; ...                      % targ
    1 4 2 2; ...                      % N1002
    1 4 3 2; ...                      % rest
    
    1 1 9 NaN; ...                    % empt1
    1 4 4 NaN];                       % empt2    

%------------------------
% SUB 5
subidx = 5;
subs(subidx).hash = 'VWW18';
subs(subidx).gender = 'male';
subs(subidx).age = 22;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-08-23@1501' '16-08-23@1706' '16-08-25@1020'};
subs(subidx).mri = 'VWW18_RAW_32_ADNI\VWW18.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.08.26.13.03.51.170804.43087942.IMA';
subs(subidx).comments = 'all good with recording';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    
    1 3 1 2; ...                      % av2
    1 3 2 2; ...                      % av3
    1 3 3 2; ...                      % av4
    1 3 4 2; ...                      % vid1
    1 3 5 2; ...                      % vid2
    1 3 6 2; ...                      % vid3
    1 3 7 2; ...                      % vid4
    
    1 2 1 1; ...                      % N1001
    
    1 3 8 2; ...                      % targ
    1 3 8 2; ...                      % N1002
    1 3 9 2; ...                      % rest
    
    1 2 2 NaN; ...                    % empt1
    1 3 10 NaN];                      % empt2 

%------------------------
% SUB 6
subidx = 6;
subs(subidx).hash = 'AJH08';
subs(subidx).gender = 'female';
subs(subidx).age = 19;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-08-29@1127' '16-08-29@1255' '16-08-29@1344' '16-09-05@1004'};
subs(subidx).mri = 'AJH08_RAW_32_ADNI\AJH08.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.08.31.15.24.48.520799.43448390.IMA';
subs(subidx).comments = 'loads of blinks; all good with recording';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 1;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 2 1 1; ...                      % aud4
    1 2 2 1; ...                      % av1
    1 2 3 1; ...                      % av2
    
    1 4 1 2; ...                      % av3
    1 4 2 2; ...                      % av4
    1 4 3 2; ...                      % vid1
    1 4 4 2; ...                      % vid2
    1 4 5 2; ...                      % vid3
    1 4 6 2; ...                      % vid4
    
    1 3 1 1; ...                      % N1001

    1 4 7 2; ...                      % targ
    1 4 7 2; ...                      % N1002
    1 4 8 2; ...                      % rest
    1 3 2 NaN; ...                    % empt1
    1 4 9 NaN];                       % empt2 

%------------------------
% SUB 7
subidx = 7;
subs(subidx).hash = 'JBM27';
subs(subidx).gender = 'male';
subs(subidx).age = 19;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-08-30@1436' '16-09-02@0943'};
subs(subidx).mri = 'JBM27_RAW_32_ADNI\JBM27.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.09.02.13.25.04.445870.43628614.IMA';
subs(subidx).comments = 'all good with recording, beginning of each session ET data bad';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4
    
    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest
    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

%------------------------
% SUB 8
subidx = 8;
subs(subidx).hash = 'MBA30';
subs(subidx).gender = 'female';
subs(subidx).age = 21;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-09-01@0947' '16-09-02@1404'};
subs(subidx).mri = 'MBA30_RAW_32_ADNI\MBA30.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.09.06.11.33.00.589630.43907142.IMA';
subs(subidx).comments = 'moved a lot during recordings; trouble keeping awake which led to droopy eyes';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 1;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    
    1 2 9 2; ...                      % av2     % repeated in 2nd session
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4
    
    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest
    1 2 8 NaN; ...                    % empt1
    1 1 10 NaN];                      % empt2 

%------------------------
% SUB 9
subidx = 9;
subs(subidx).hash = 'MER28';
subs(subidx).gender = 'male';
subs(subidx).age = 23;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-09-06@1350' '16-09-13@1252'};
subs(subidx).mri = 'MER28_RAW_32_ADNI\MER28.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.09.14.09.30.54.874205.44415046.IMA';
subs(subidx).comments = 'weird guy, sometimes difficult ET calibration';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 1;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4
    
    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest
    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

%------------------------
% SUB 10
subidx = 10;
subs(subidx).hash = 'PBN16';
subs(subidx).gender = 'male';
subs(subidx).age = 22;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-09-15@1018' '16-09-22@1016'};
subs(subidx).mri = 'PBN16_RAW_32_ADNI\PBN16.MR.SEQUENCEREGION_ANGUS.0021.0001.2016.09.02.15.44.31.978409.43732068.IMA';
subs(subidx).comments = 'good one! little movement and few blinks generally. check for muscle artifacts.';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4

    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest
    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

%------------------------
% SUB 11
subidx = 11;
subs(subidx).hash = 'LNT20';
subs(subidx).gender = 'male';
subs(subidx).age = 26;
subs(subidx).scan = {'S41k6E2X' '0127a04AK'};
subs(subidx).sess = {'16-09-20@1442' '16-09-20@1515' '16-09-23@0929'};
subs(subidx).mri = 'LNT20_RAW_32_ADNI\LNT20.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.09.15.15.30.14.466944.44740726.IMA';
subs(subidx).comments = 'good one! little movement and few blinks generally. check for muscle artifacts.';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    2 2 1 1; ...                      % aud2
    2 2 2 1; ...                      % aud3
    2 2 3 1; ...                      % aud4
    2 2 4 1; ...                      % av1
    2 2 5 1; ...                      % av2
    
    2 3 1 2; ...                      % av3
    2 3 2 2; ...                      % av4
    2 3 3 2; ...                      % vid1
    2 3 4 2; ...                      % vid2
    2 3 5 2; ...                      % vid3
    2 3 6 2; ...                      % vid4

    2 2 6 1; ...                      % N1001

    2 3 7 2; ...                      % targ
    2 3 7 2; ...                      % N1002
    2 3 8 2; ...                      % rest
    
    2 2 7 NaN; ...                    % empt1
    2 3 8 NaN];                       % empt2 

%------------------------
% SUB 12
subidx = 12;
subs(subidx).hash = 'HKP24';
subs(subidx).gender = 'male';
subs(subidx).age = 32;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-09-26@1057' '16-09-27@1403'};
subs(subidx).mri = 'HKP24_RAW_32_ADNI\HKP24.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.09.28.15.03.37.657140.45951128.IMA';
subs(subidx).comments = 'some movement. could not fixate well.';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4

    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest
    
    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

%------------------------
% SUB 13
subidx = 13;
subs(subidx).hash = 'LPA08';
subs(subidx).gender = 'female';
subs(subidx).age = 20;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-10-19@1445' '16-10-20@1005'};
subs(subidx).mri = 'LPA08_RAW_32_ADNI\LPA08.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.10.24.16.39.06.361239.51938456.IMA';
subs(subidx).comments = 'No Eyetracking. Dizzy in first block. Movement OK. Auditory performance 56% due to bad staircase value.';
subs(subidx).eye = 'no';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4

    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest

    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

%------------------------
% SUB 14
subidx = 14;
subs(subidx).hash = 'CHY02';
subs(subidx).gender = 'female';
subs(subidx).age = 18;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-10-24@1047' '16-10-27@0957'};
subs(subidx).mri = 'CHY02_RAW_32_ADNI\CHY02.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.10.28.12.31.48.168690.52915308.IMA';
subs(subidx).comments = 'Good one. Movement OK. ET OK.';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4

    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest

    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

%------------------------
% SUB 15
subidx = 15;
subs(subidx).hash = 'MBE14';
subs(subidx).gender = 'female';
subs(subidx).age = 20;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-10-25@1422' '16-10-31@1034'};
subs(subidx).mri = 'MBE14_RAW_32_ADNI\MBE14.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.10.27.12.49.03.17762.52908132.IMA';
subs(subidx).comments = 'very still generally. loads of blinks';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4

    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest

    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

%------------------------
% SUB 16
subidx = 16;
subs(subidx).hash = 'RKY27';
subs(subidx).gender = 'male';
subs(subidx).age = 20;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-11-04@1041' '16-11-11@1443' '16-11-11@1509'};
subs(subidx).mri = 'RKY27_RAW_32_ADNI\RKY27.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.11.07.11.08.49.627733.56571974.IMA';
subs(subidx).comments = 'Very still. Very few blinks. Calibration trouble (switched to 5-p-cali after block #4).';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    
    1 2 1 2; ...                      % av2
    1 3 1 2; ...                      % av3
    1 3 2 2; ...                      % av4
    1 3 3 2; ...                      % vid1
    1 3 4 2; ...                      % vid2
    1 3 5 2; ...                      % vid3
    1 3 6 2; ...                      % vid4

    1 1 6 1; ...                      % N1001

    1 3 7 2; ...                      % targ
    1 3 7 2; ...                      % N1002
    1 3 8 2; ...                      % rest

    1 1 7 NaN; ...                    % empt1
    1 3 9 NaN];                       % empt2 

%------------------------
% SUB 17
subidx = 17;
subs(subidx).hash = 'RLK20';
subs(subidx).gender = 'male';
subs(subidx).age = 20;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-11-08@1438' '16-11-14@1509'};
subs(subidx).mri = 'RLK20_RAW_32_ADNI\RLK20.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.11.09.10.07.08.924494.57866310.IMA';
subs(subidx).comments = 'Good one. Little movement. Few blinks. Some trouble with cali but pupil should be fine.';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4

    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest

    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 


%------------------------
% SUB 18
subidx = 18;
subs(subidx).hash = 'CRH27';
subs(subidx).gender = 'female';
subs(subidx).age = 19;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-11-17@1456' '16-11-18@1426'};
subs(subidx).mri = 'CRH07_RAW_32_ADNI\CRH27.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.11.22.14.24.44.484053.62157964.IMA';     % CHECK
subs(subidx).comments = 'Cali + ET didnt work very well. Usually very still (except #4). Medium amount of blinks.';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 1;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4

    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest

    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

%------------------------
% SUB 19
subidx = 19;
subs(subidx).hash = 'YKI30';
subs(subidx).gender = 'male';
subs(subidx).age = 19;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-11-21@1454' '16-11-30@1519'};
subs(subidx).mri = 'YKI30_RAW_32_ADNI\YKI30.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.11.23.10.06.13.673292.62413910.IMA';
subs(subidx).comments = 'Stillness OK. Not many blinks generally. Swallow artifacts. Cali in 1st session bad.';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 1;

% scan sess run headshape
subs(subidx).runs = [1 1 1 1; ...     % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4

    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest

    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

%------------------------
% SUB 20
subidx = 20;
subs(subidx).hash = 'MPA20';
subs(subidx).gender = 'female';
subs(subidx).age = 19;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-11-24@1050' '16-11-28@1515'};
subs(subidx).mri = 'MPA20_RAW_32_ADNI\MPA20.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.11.28.17.40.08.892830.63606952.IMA';
subs(subidx).comments = 'Quite still. Medium amount of blinks. Good cali/ET quality.';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = ...
   [1 1 1 1; ...                      % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4

    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest

    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

%------------------------
% SUB 21
subidx = 21;
subs(subidx).hash = 'VTN13';
subs(subidx).gender = 'female';
subs(subidx).age = 19;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-12-02@1439' '16-12-05@1106'};
subs(subidx).mri = 'VTN13_RAW_32_ADNI\VTN13.MR.SEQUENCEREGION_PASCAL.0002.0002.2016.12.05.15.25.34.697426.65720466.IMA';
subs(subidx).comments = 'Medium amount of blinks. Quite still (scratched sometimes). Good ET/cali quality.';
subs(subidx).eye = 'eye';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = ...
   [1 1 1 1; ...                      % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4

    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest

    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

% ------------------------
% SUB 22
subidx = 22;
subs(subidx).hash = 'KAE22';  % check whether that's Alice
subs(subidx).gender = 'female';
subs(subidx).age = 20;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'16-12-13@1452' '16-12-20@1345'};
subs(subidx).mri = 'KAE22_RAW_32_ADNI\KAE22.MR.SEQUENCEREGION_PASCAL.0002.0001.2016.12.05.10.32.48.272636.65510486.IMA';
subs(subidx).comments = 'Relatively still. Sometimes many, sometimes few blinks. ET and cali OK.';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = ...
   [1 1 1 1; ...                      % aud1
    1 1 2 1; ...                      % aud2
    1 1 3 1; ...                      % aud3
    1 1 4 1; ...                      % aud4
    1 1 5 1; ...                      % av1
    1 1 6 1; ...                      % av2
    
    1 2 1 2; ...                      % av3
    1 2 2 2; ...                      % av4
    1 2 3 2; ...                      % vid1
    1 2 4 2; ...                      % vid2
    1 2 5 2; ...                      % vid3
    1 2 6 2; ...                      % vid4

    1 1 7 1; ...                      % N1001

    1 2 7 2; ...                      % targ
    1 2 7 2; ...                      % N1002
    1 2 8 2; ...                      % rest

    1 1 8 NaN; ...                    % empt1
    1 2 9 NaN];                       % empt2 

% ------------------------
% SUB 23
subidx = 23;
subs(subidx).hash = 'ECN13';
subs(subidx).gender = 'male';
subs(subidx).age = 39;
subs(subidx).scan = {'0127a04AK'};
subs(subidx).sess = {'17-04-24@1338' '17-04-24@1513' '17-04-25@1509'};
subs(subidx).mri = 'ECN13_RAW_32\ECN13.MR.SEQUENCEREGION_PHIL.7.1.2010.08.20.11.36.26.578125.105957448.IMA';
subs(subidx).comments = 'Stretches were he nearly fell asleep and blinked a lot. Movement OK.';
subs(subidx).eye = 'yes';
subs(subidx).exclude = 0;

% scan sess run headshape
subs(subidx).runs = ...
   [1 1 1 1; ...                      % aud1
    1 2 1 1; ...                      % aud2
    1 2 2 1; ...                      % aud3
    1 2 3 1; ...                      % aud4
    1 2 4 1; ...                      % av1
    1 2 5 1; ...                      % av2
    
    1 3 1 2; ...                      % av3
    1 3 2 2; ...                      % av4
    1 3 3 2; ...                      % vid1
    1 3 4 2; ...                      % vid2
    1 3 5 2; ...                      % vid3
    1 3 6 2; ...                      % vid4

    1 2 6 1; ...                      % N1001

    1 3 7 2; ...                      % targ
    1 3 7 2; ...                      % N1002
    1 3 8 2; ...                      % rest

    1 2 7 NaN; ...                    % empt1
    1 3 9 NaN];                       % empt2 

end