%
% take features used in analysis and compute each feature-by-feature cross
% spectrum
%
% then compute power spectrum for each individual feature
%

close all
clearvars

COMP = 2;
Step0_setup;

sname = sprintf('%sfinalfigures/StimAnalysisData.mat',DIR.main);

% parameters
fs = 150;
topFreq = 8;
corrtopFreq = [1,3];
lowfreq = 0.5;

%% load and prepare features
% choose groups of predictors
AudFeat = {'aud_env', 'aud_slope', 'aud_pitch'};
VisFeat = {'lip_area', 'lip_slope', 'lip_width'};
%ArtFeat = {'phoneArt'};
% define features to load here
AllLabels = [AudFeat, VisFeat];
ARG.AllLabelsUsed = AllLabels;

ARG.GroupNames = {'AudFeat','VisFeat'};
ARG.AudFeat = AudFeat;
ARG.VisFeat = VisFeat;

% create Features
cfg = [];
cfg.srate = fs;
tic
AllFeatures = createFeatures_v3(cfg,DIR);
toc


% sort features ----------
Features = sort_features(AllFeatures, AllLabels);
contFeat = Features.contFeat;
discFeat = Features.discFeat;
nenv = sum(Features.NDims);
nmaxcoeff = Features.nmaxcoeff;

Aud_indx = find(ismember(AllLabels,AudFeat));
Vis_indx = find(ismember(AllLabels,VisFeat));

Sentences = cat(1,Features.ChosenFeat{:});
Sentences = cellfun(@transpose,Sentences,'UniformOutput',false);
nfiles = numel(Sentences);


%%
SentenceLength = cellfun(@length,cat(2,Features.ChosenFeat{:}));
SentenceLength = SentenceLength(:) ./ fs;

mlen = mean(SentenceLength);
maxlen = max(SentenceLength);
minlen = min(SentenceLength);
stdlen = std(SentenceLength);
totallen = sum(SentenceLength);

fprintf('\nSentence Length in seconds: \n');
fprintf('mean: %.2f    std: %.2f    min: %.2f   max:%.2f    total: %.2f \n',...
    mlen, stdlen, minlen, maxlen, totallen);


%% compute cross spectra
CrossSpectra = cell(nfiles,1);
PowerSpectra = cell(nfiles,1);
[I,J] = ind2sub([nenv,nenv],[1:nenv^2]);

% create labels for feature combination
llabel = AllLabels(J); rlabel = AllLabels(I);
CoherLabels = cellfun(@(x,y) strjoin({x,y},' '),llabel,rlabel,'UniformOutput',false);


tic
% zscore Sentences before analysis
Sentences = cellfun(@zscore,Sentences,'UniformOutput',false);
for ifile = 1:nfiles
    % Cross spectrum
    [Cxy,F] = mscohere(Sentences{ifile}(:,I),Sentences{ifile}(:,J),150,75,1024,fs);
    CrossSpectra{ifile} = Cxy;
    % Power spectrum
    [Pxx,freq] = pwelch(Sentences{ifile},150,75,1024,fs);
    PowerSpectra{ifile} = Pxx;
end
CrossSpectra = cat(3,CrossSpectra{:});
PowerSpectra = cat(3,PowerSpectra{:});
toc

% frequency limits, x axis
limit = find(F >= lowfreq & F <= topFreq); % up to 8 Hz
powerlimit = find(freq >= lowfreq & freq <= topFreq);

% 1/f spectrum
fnoiseSpec = @(x) 1./x;
noisespectrum = log(fnoiseSpec(freq(powerlimit)));

% Power
Power.spectra = sq(mean(log(PowerSpectra(powerlimit,:,:)),3));
Power.freq = freq(powerlimit);
Power.noise = noisespectrum;

% Coherence
ii = repmat([1:3],3,1) + [3:nenv:3*nenv]';
CoherLabels = CoherLabels(ii)';
Coherence.spectra = sq(mean(CrossSpectra(limit,ii,:),3));
Coherence.freq = F(limit);
Coherence.label = CoherLabels(:);

% Correlation
corrlimit{1} = find(F >= lowfreq & F <= corrtopFreq(1));
corrlimit{2} = find(F >= corrtopFreq(1) & F <= corrtopFreq(2));
CorrMat = sq(mean(mean(CrossSpectra(corrlimit{1},:,:),3),1))';
Correlation.data{1} = reshape(CorrMat,[nenv,nenv]);
CorrMat = sq(mean(mean(CrossSpectra(corrlimit{2},:,:),3),1))';
Correlation.data{2} = reshape(CorrMat,[nenv,nenv]);
Correlation.range(1,:) = [lowfreq, corrtopFreq(1)];
Correlation.range(2,:) = [corrtopFreq(1), corrtopFreq(2)];


% save
save(sname,'Power','Coherence','Correlation');

