%
% load CMI data from all participants and start by correlating MI values
% between AudFeat A cortex V trials and V cortex V trials across
% participants
%
%
% ----
% to do:
% try partial correlation with auditory cortex, A-only trials, AudFeat as a
% conditional variable, because this should reflect signal-to-noise ratio
% most accurately
% ----
%

close all 
clearvars


COMP = 2;
Step0_setup;

% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);
goodsub = find([SUBS.exclude]==0);
nsub = length(goodsub);

% directories and save name
sname = sprintf('../finalfigures/Data_CMI_Hitrate.mat');
snamebase = sprintf('%sCMI_meanlag',DIR.cmi);
conditions = {'auditory','visual'};


nfreq = 5;
nvar = 3; % variants of CMI measures
nstim = 2; % MI( ENV ) and MI( LIP ) !

blocks = [1:4; 9:12];
ncond = size(blocks,1);


% load behavioral data
load('../finalfigures/Behavior.mat');


%% get sourceatlas
load sourceatlas_grid.mat
load('ROIVOXELS.mat','roivoxins');
load pROI_voxel.mat
nvox = length(pROI.all.inroivoxins);

% get template MRI
mri = ft_read_mri(templateT1);
mri = ft_convert_units(mri, 'cm');


%% roi voxel

% unpack roi voxel indices
leftA = pROI.auditory.left.inroivoxins;
rightA = pROI.auditory.right.inroivoxins;
leftV = pROI.visual.left.inroivoxins;
rightV = pROI.visual.right.inroivoxins;
% intersect with ROI voxel mat
[~, leftA] = intersect(pROI.all.inroivoxins,leftA);
[~, rightA] = intersect(pROI.all.inroivoxins,rightA);
[~, leftV] = intersect(pROI.all.inroivoxins,leftV);
[~, rightV] = intersect(pROI.all.inroivoxins,rightV);

audvox = [leftA; rightA];
visvox = [leftV; rightV];

allvox = {audvox,visvox};

% sizes
nhemi = 2;
nroi = 2;



%% load data
filename = sprintf('%s_auditory_sub01.mat',snamebase);
data = load(filename,'ARG','ARGF');
freqrange = data.ARG.freq_range;

CMI = zeros(ncond,nstim,nvar,nsub,nfreq,nvox);

for icond = 1:ncond
    for isub2 = 1:length(goodsub)
        isub = goodsub(isub2);
        
        condlabel = conditions{icond};
        filename = sprintf('%s_%s_sub%02d.mat', snamebase,condlabel,isub);
        data = load(filename,'ARG','ARGF','CMI_B1_E','CMI_C1_E',...
            'CMI_C2_E','CMI_B1_L','CMI_C1_L','CMI_C2_L');
        
        % CMI data
        CMI(icond,1,1,isub2,:,:) = data.CMI_B1_E;
        CMI(icond,1,2,isub2,:,:) = sq(mean(data.CMI_C1_E,3));
        CMI(icond,1,3,isub2,:,:) = sq(mean(data.CMI_C2_E,3));
        % LIP
        CMI(icond,2,1,isub2,:,:) = data.CMI_B1_L;
        CMI(icond,2,2,isub2,:,:) = sq(mean(data.CMI_C1_L,3));
        CMI(icond,2,3,isub2,:,:) = sq(mean(data.CMI_C2_L,3));
    end
end



%% load partialLoo data
snamebase = sprintf('%sCMI_partialLoo',DIR.cmi);
filename = sprintf('%s_visual_sub01_rand.mat',snamebase);
data = load(filename,'ARG','ARGF');
nmods = data.ARG.predlen;


CMILoo = zeros(ncond,nsub,nmods,nfreq,nvox);
SCMI = zeros(ncond,nsub,nmods,nfreq,nvox);
MILoo = zeros(ncond,nsub,nmods,nfreq,nvox);

for icond = 1:ncond
    for isub2 = 1:length(goodsub)
        isub = goodsub(isub2);
        
        condlabel = conditions{icond};
        
        % CMI loo data
        filename = sprintf('%s_%s_sub%02d_rand.mat', snamebase,condlabel,isub);
        data = load(filename,'ARG','ARGF','CMI');
        
        % CMI data
        CMILoo(icond,isub2,:,:,:) = data.CMI;
        
        
        % MI loo data
        filename = sprintf('%s_%s_sub%02d.mat', snamebase,condlabel,isub);
        data = load(filename);
        MILoo(icond,isub2,:,:,:) = data.MI;
        SCMILoo(icond,isub2,:,:,:) = data.SCMI;
    end
end




%% cmi correlation

% SNR control
% AudFeat control
Cm = sq(mean(CMI(1,1,1,:,:,audvox),6));
Cm = mean(zscore(Cm),2);
% VisFeat control
Vm = sq(mean(CMI(2,2,1,:,:,visvox),6));
Vm = mean(zscore(Vm),2);


icond = 2;
npred = 2; % AudFeat and LipFeat


% compute regression model
fprintf('Grouped features: \n \n');
for freq = 1:2
    % get data
    Xa = sq(mean(CMI(icond,1,2,:,freq,audvox),6));
    Xv = sq(mean(CMI(icond,2,2,:,freq,visvox),6));
    X = [Xa,Xv];
    Y = Behavior.Correct(:,icond);
    
    % zscore variables
    Yreg = zscore(Y); 
    X = zscore(X); Cm = zscore(Cm); Vm = zscore(Vm);
    offset = ones(nsub,1);
    
    
    [beta,Stats] = ck_stat_regress(Yreg,[offset, X, Cm, Vm]);
    fprintf('%.1g - %.1g Hz: \n',data.ARG.freq_range(freq,:));
    fprintf('regress model r = %.4f    pval = %.4f \n', ...
        sqrt(Stats.model(1)), Stats.B_P(2));
    
    % full model bayes
    BIC_f = 5*log(nsub)-2*Stats.model(end);
    
    % compute bayes factor 
    for ipred = 1:npred
        Xtar = X(:,ipred);
        Xres = X;
        Xres(:,ipred) = [];
        
        % reduced model with key predictor removed
        [betaR,StatsRed] = ck_stat_regress(Yreg,[offset, Xres, Cm, Vm]);

        % BIC values
        BIC_r = 4*log(nsub)-2*StatsRed.model(end);

        delta_bic = BIC_r - BIC_f; 
        bf = exp(delta_bic/2);

        fprintf('beta = %.4f    bayes factor: %.4f \n',beta(ipred+1),bf);
        % we expect BIC_f to be smaller if the predictor of interest adds predictive power
        % hence Delta to be positive
    end
    
    fprintf('\n \n');
end


% we have BF of 6.8 for AC and 0.24 (=-4.1) for VC So good evidence for and against correlations!

fprintf('\n \n \n');




%% single model per method and frequency

% compute models for three different MI methods
methods = {'MI','SCMI','CMI'};
nmethods = numel(methods);


% target predictors are env in audvox and pitch in visvox
icond = 2;
i_env = 1;
i_pitch = 3;
npred = 2;
topfreq = 2;

% arguments
EffectsARG.icond = icond;
EffectsARG.i_env = i_env;
EffectsARG.i_pitch = i_pitch;
EffectsARG.npred = npred;
EffectsARG.methods = methods;
EffectsARG.topfreq = 2;
EffectsARG.freqbands = data.ARG.freq_range(1:topfreq,:);


% empty variables
Beta = [];
Stats = [];
Residuals = [];
Delta_bic = [];
BF = [];

% compute models
fprintf('Individual features: \n \n');
for imethod = 1:nmethods
    % SNR control - distractors variables
    % AudFeat control
    Cm = sq(mean(CMI(1,1,1,:,:,audvox),6));
    % VisFeat control
    Vm = sq(mean(CMI(2,2,1,:,:,visvox),6));

    % define method for data
    datatype = methods{imethod};
    if strcmp(datatype,'MI')
        MIdata = MILoo;
        % take unconditional distractors instead (i.e. C1)
        % AudFeat control
        Cm = sq(mean(CMI(1,1,2,:,:,audvox),6));
        % VisFeat control
        Vm = sq(mean(CMI(2,2,2,:,:,visvox),6));
    elseif strcmp(datatype,'CMI')
        MIdata = CMILoo;
    elseif strcmp(datatype,'SCMI')
        MIdata = SCMILoo;
    end
    
    % z score distractors
    Cm = mean(zscore(Cm),2);
    Vm = mean(zscore(Vm),2);
    
    
    for freq = 1:topfreq
        Xa = sq(mean(MIdata(icond,:,i_env,freq,audvox),5))';
        Xv = sq(mean(MIdata(icond,:,i_pitch,freq,visvox),5))';
        Y = Behavior.Correct(:,icond);
        
        % zscore data
        Xvar = [Xa,Xv];
        Xvar = zscore(Xvar,1);
        Yreg = zscore(Y,1);
        offset = ones(size(Xv));
        
        % num target predictors
        ncolx = size(Xvar,2);
        
        
        % regression analysis
        [beta,stats] = ck_stat_regress(Yreg,[offset, Xvar, Cm, Vm]);
        fprintf('%.1g - %.1g Hz: \n',data.ARG.freq_range(freq,:));
        fprintf('regress model r = %.4f    pval = %.4f    beta = %.4f \n', ...
            sqrt(stats.model(1)), stats.B_P(2), beta(2));
        
        
        Stats{imethod,freq} = stats;
        Beta{imethod,freq} = beta;
        
        
        % compute residuals for both target variables
        for icol = 1:ncolx
            % leave on out index
            pind = [1:ncolx];
            pind(icol) = [];
            Xres = Xvar(:,pind);
            Xtar = Xvar(:,icol);
            % added variable residuals
            w1 = [offset, Xres, Cm, Vm]\Yreg;
            res1 = Yreg - [offset, Xres, Cm, Vm] * w1; % residual to Y
            w2 = [offset, Xres, Cm, Vm]\Xtar;
            res2 = Xtar - [offset, Xres, Cm, Vm] * w2; % residual to X
            % save residuals
            Residuals{imethod,freq}(:,[1,2],icol) = [res2, res1];
            
            
            % compute bayes factor 
            % reduced model with key predictor removed
            [beta,statsRed] = ck_stat_regress(Yreg,[offset, Xres, Cm, Vm]);

            % BIC values:
            BIC_f = 5*log(nsub)-2*stats.model(end);
            BIC_r = 4*log(nsub)-2*statsRed.model(end);

            delta_bic = BIC_r - BIC_f; 
            Delta_bic(imethod,freq,icol) = delta_bic;
            bf = exp(delta_bic/2);
            BF(imethod,freq,icol) = bf;
            
            fprintf('bayes factor: %.4f \n',bf);
        end
        
        
    end
    
    B_P = cellfun(@(x) x.B_P,Stats(imethod,:),'UniformOutput',false);
    tmp = cat(2, Beta(imethod,:), B_P);
    tmp = tmp([1,3,2,4]);
    tmp = cat(2,tmp{:});
    
    fprintf('\n \n');
    fprintf([datatype ' \n']);
    varnames = {'beta1','pval1','beta2','pval2'};
    tmp = array2table(tmp,'VariableNames',varnames)
    fprintf('\n \n');
end


Effects.Beta = Beta;
Effects.Stats = Stats;
Effects.Residuals = Residuals;
Effects.Delta_bic = Delta_bic;
Effects.BF = BF;

% save data to file
save(sname,'Effects','EffectsARG');
fprintf('everything is done \n');




return





%% run after Step8_cmi_hitrate_correlation has run through

% check out behavioral correlation with partialized individual features

% choose cond. or uncond. MI data
datatype = 'CMI';
if strcmp(datatype,'MI')
    MIdata = MILoo;
elseif strcmp(datatype,'CMI')
    MIdata = CMILoo;
elseif strcmp(datatype,'SCMI')
    MIdata = SCMILoo;
end


LooFX = [];
Pvals = [];
for freq = 1:2
    
    fprintf('compute freq band %02d \n',freq);
    
    % get data
    icond = 2;
    Xa = sq(mean(MIdata(icond,:,:,freq,audvox),5));
    Xv = sq(mean(MIdata(icond,:,:,freq,visvox),5));
    Y = Behavior.Correct(:,2);

    % zscore data
    Xa = zscore(Xa,1);
    Xv = zscore(Xv,1);
    X = {Xa,Xv};
    Yreg = zscore(Y);

    % compute regression for each individual feature
    for iroi = 1:nroi
        for imod = 1:nmods
            % individual feature
            Xvar = X{iroi}(:,imod);

            % added variable residuals
            w1 = [offset, Cm, Vm]\Yreg;
            res1 = Yreg - [offset, Cm, Vm] * w1;
            w2 = [offset, Cm, Vm]\Xvar;
            res2 = Xvar - [offset, Cm, Vm] * w2;

            % regression analysis
            [beta,Stats] = ck_stat_regress(Yreg,[offset, Xvar, Cm, Vm]);
            fprintf('regress model r = %.4f    pval = %.4f    beta = %.4f \n', ...
                sqrt(Stats.model(1)), Stats.B_P(2), beta(2));

            % two linear for partial regression line plot
            dx = [1, 1; -2, max(Xvar)*2]';
            fx = dx * beta(1:2);


            % compute bayes factor 
            % reduced model with key predictor removed
            [beta,StatsRed] = ck_stat_regress(Yreg,[offset, Cm, Vm]);

            % BIC values:
            BIC_f = 4*log(nsub)-2*Stats.model(end);
            BIC_r = 3*log(nsub)-2*StatsRed.model(end);

            Delta_bic(iter) = BIC_r - BIC_f; 
            BF = exp(Delta_bic(iter)/2);
            LooFX{freq,iroi,imod}.BF = BF;
            fprintf('bayes factor: %.4f \n',BF);
            fprintf('\n');



            LooFX{freq,iroi,imod}.X = Xvar;
            LooFX{freq,iroi,imod}.Y = Yreg;
            LooFX{freq,iroi,imod}.dx = dx;
            LooFX{freq,iroi,imod}.fx = fx;
            LooFX{freq,iroi,imod}.Residuals = [res2, res1];
            LooFX{freq,iroi,imod}.Pval = Stats.B_P;
            LooFX{freq,iroi,imod}.datatype = datatype;

            Pvals(freq,iroi,imod) = Stats.B_P(2);
        end
    end
        
end 








%% go and plot data

freqtitle = {'0.5 - 1 Hz', '1 - 3 Hz'};
stimlabel = cellfun(@(x) replace(x,'_',' '),data.ARG.AudFeat,'UniformOutput',false);


for freq = 1:2
    figure
    iter = 1;
    
    for iroi = 1:nroi
        for imod = 1:nmods
            X = LooFX{freq,iroi,imod}.Residuals(:,1);
            Y = LooFX{freq,iroi,imod}.Residuals(:,2);
            dx = LooFX{freq,iroi,imod}.dx;
            fx = LooFX{freq,iroi,imod}.fx;


            hh = subplot(nroi,nmods,iter);

            plot(dx(:,2),fx,'-','LineWidth',1,'Color',[1,1,1]*0.5);
            hold on
            plot(X,Y,'.','MarkerSize',10,'Color','b');
            
            % axes limits
            xlim([-2,2]);
            ylim([-2,2]);
            axis square
            box off
            
            header = stimlabel{imod};
            title(header);
            
            if imod == 1
                ytext = [conditions{iroi} ' cortex'];
                ylabel(ytext);
            end
            
            drawnow
            iter = iter + 1;
        end
    end
    
    ckfiguretitle([datatype ' ' freqtitle{freq}]);
end



