function Predictors = sort_predictors(FeatStruct,GroupNames,leaveoneout)
%
% Predictors = sort_predictors(FeatStruct,GroupNames,leaveoneout)
% sorts features into cell array indexing predictors from the Features
% structure.
% if leaveoneout is true, indexes all predictors with the dpecified ones
% left out.
%

% handle input
if nargin < 3 || isempty(leaveoneout)
    leaveoneout = false;
end

% get indices for each dimension from DimNames
predlen = numel(GroupNames);
for ipred = 1:predlen
    Predictors{ipred} = find(ismember(FeatStruct.DimNames,GroupNames{ipred}));
end

if leaveoneout
    LOO = {};
    for ipred = 1:predlen
        tmp = Predictors;
        tmp{ipred} = [];
        LOO{ipred} = cat(1,tmp{:});
    end
    Predictors = LOO;
end

end
