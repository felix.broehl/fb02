%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% do essentially the same as Step2_MIvoxel but do not sum over lags
% also use partial MI measure
%
% then we can have a look at the MI as a function of time
%
% options are: 
% B1_E: MI(env ; meg | lip)
% C2_E: MI(env ; meg_shuf | lip_shuf)
%
% B1_L: MI(lip ; meg | env)
% C2_L: MI(lip ; meg_shuf | env_shuf)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


close all
clearvars

% directories
COMP = 2; % set environment to local windows machine

Step0_setup;
blocks = [9:12]; 

poolobj = manageParpool(-2);
numcores = poolobj.NumWorkers;


% condition label for plot header
if blocks(1) == 1
    condlabel = 'auditory';
elseif blocks(1) == 9
    condlabel = 'visual';
end
ARG.condlabel = condlabel;


% directories and save name
snamebase = sprintf('%sMI_modalvar_optlag_%s',DIR.mi,condlabel);

% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);
goodsub = find([SUBS.exclude]==0);
nsub = length(goodsub);


%% parameters -----------------------------------------------------
ARG.freq_range = [0.5, 1; 1, 3; 2, 4; 3, 6; 4, 8]; % delta, theta and what not...
nfreq = size(ARG.freq_range,1);
nblocks = numel(blocks);
ARG.srate = 50; % in Hz

ARG.OFFSET = round(0.5 * ARG.srate); % 500 ms - cut out evoked response
envlen = round(5 * ARG.srate); 

% randomization
nrand = 1;
ARG.nrand = nrand;

% phonetic feature PCA
ARG.ArtPCA = true;
ARG.minvariance = 90;


% filter parameters
ARGF.rate = ARG.srate;
ARGF.mode = 'butter';
ARGF.order = 3; % filter order
ARGF.window = 1; % symmetric padding

ARGM.model = 'MI';
ARGM.tmin = -100;
ARGM.tmax =  500;

lags = [round(ARGM.tmin/1000*ARG.srate):round(ARGM.tmax/1000*ARG.srate)];
nlags = length(lags);

ARGM.lags = lags;
ARGM.nlags = nlags;


%% load ROI data ----------------------------------------------------------
% contains intersected voxel for primary cortices in roivoxins
load ROIVOXELS.mat
load pROI_voxel.mat % -> pROI structure
ARG.pROI = pROI;
allvox = pROI.all.inroivoxins;
nvox = numel(allvox);

[~,audvox] = intersect(allvox,[pROI.auditory.left.inroivoxins; pROI.auditory.right.inroivoxins]);
[~,visvox] = intersect(allvox,[pROI.visual.left.inroivoxins; pROI.visual.right.inroivoxins]);
roivox = {audvox, visvox};
nroi = numel(roivox); % auditory and visual cortex


%% load features ----------------------------------------------------------
% choose groups of predictors
AudFeat = {'broad_env', 'broad_slope', 'pitch'};
VisFeat = {'lip_area', 'lip_slope', 'dist_x'};
%AudFeat = {'broad_env'};
%VisFeat = {'lip_area'};
% define features to load here
AllLabels = {AudFeat{:}, VisFeat{:}};
ARG.AllLabelsUsed = AllLabels;

ARG.GroupNames = {'AudFeat','VisFeat'};
ARG.AudFeat = AudFeat;
ARG.VisFeat = VisFeat;

% create Features
cfg = [];
cfg.srate = ARG.srate;
cfg.ArtPCA = ARG.ArtPCA;
cfg.PCAminvar = ARG.minvariance;
AllFeatures = createFeatures_v3(cfg,DIR);


% sort features -----------
Features = sort_features(AllFeatures, AllLabels);
contFeat = Features.contFeat;
discFeat = Features.discFeat;
nenv = sum(Features.NDims);
nmaxcoeff = Features.nmaxcoeff;


% define Predictors ------
FullNames = {AllLabels};
FullPredictors = sort_predictors(Features,FullNames);
fullpredlen = length(FullPredictors);

% needed to assign the optimal lags for the multivariate predictors
PredNames = {AudFeat, VisFeat};
Predictors = sort_predictors(Features,PredNames);
predlen = length(PredNames);


ARG.Predictors = Predictors;
ARG.predlen = predlen;





%% main

for isub2 = 1:nsub
    isub = goodsub(isub2);
    
    % create filename and check if it already exists
    sname = sprintf('%s_sub%02d.mat', snamebase,isub);
    if exist(sname,'file')
        fprintf('file already computed \n');
        %continue;
    end
    
    % all blocks structures
    meg = []; env = [];
    
    % for each block, concat all trials and compute MI
    for iblock = blocks
        
        dataname = sprintf('%ssourcespace_sub%d_block%d_A.mat', DIR.source, isub, iblock);
        if ~exist(dataname,'file')
            fprintf('No data file found!! \n');
            continue;
        end
        
        tic
        % load data and sorted envelopes from file
        data = load(dataname);
        fprintf('loading sub %d block %d \n', isub, iblock);
        
        % sort stims and concat trials of each block
        reidx = data.ARG.trialinfo(:,4);
        cond = data.ARG.trialinfo(1,5);
        env = cat(2,env,{Features.ChosenFeat{cond}{reidx}});
        % data
        meg = cat(1,meg,data.source_raw);
    end
    clear data
    
    % STIMULI ----------------------------------
    % cut to same length
    env = cellfun(@(x) x(:,ARG.OFFSET+[1:envlen]),env,'UniformOutput',false);
    env = cat(3,env{:});
    
    % MEG ----------------------------------
    % select only primary ROIs from meg and cut to same length
    meg = meg(:,pROI.all.inroivoxins,ARG.OFFSET+[1:envlen]);
    meg = permute(meg,[2,3,1]);
    [~,meglen,ntrl] = size(meg);
    
    

    
    % filter, hilbert, MI ---------------------------------------------
    % MI [envelopes x frequencies x voxels]
    MI_E = zeros(nfreq,nlags,nvox);
    
    MI_L = zeros(nfreq,nlags,nvox);
    for freq = 1:nfreq
        tic
        fprintf('filter data into %.2f to %.2f Hz band \n', ARG.freq_range(freq,:));
        
        % FILTER stimuli
        cfg = [];
        cfg.continuous = true;
        cfg.fs = ARGF.rate;
        cfg.filter = true;
        cfg.filtertype = ARGF.mode;
        cfg.filterord = ARGF.order;
        cfg.filterwindow = ARGF.window;
        cfg.hilbert = true;
        cfg.zcopnorm = true;

        % filter and zscore features
        cfg.filterfreq = ARG.freq_range(freq,:);
        stim_filt = fb_preprocess_trials(cfg,env);
    
        % filter meg
        cfg.filterfreq = ARG.freq_range(freq,:);
        brain_filt = fb_preprocess_trials(cfg,meg);
        % split so we already copnorm and we need not copy whole matrix for
        % each worker
        brain_real = brain_filt(1:nvox,:,:);
        brain_imag = brain_filt(nvox+1:end,:,:);
        toc
        
        
        % get hilbert predictors
        E_indx = Predictors{1} + [0,nenv];
        E_hilc = reshape(stim_filt(E_indx(:),:,:),[2*length(Predictors{1}),envlen*ntrl])';
        
        L_indx = Predictors{2} + [0,nenv];
        L_hilc = reshape(stim_filt(L_indx(:),:,:),[2*length(Predictors{2}),envlen*ntrl])';
        
        fprintf('compute MI over voxels\n');
        tic
        parfor ivox = 1:nvox
            % copnorm MEG ------------
            cutbrain = cat(1,brain_real(ivox,:,:),brain_imag(ivox,:,:)); % choose vox
            cutbrain = reshape(cutbrain,[2,envlen*ntrl])';
            
            % generate lags here, then loop across them
            % invert lags, because we want to shift the meg backwards
            lagbrain = generateLags(cutbrain,-1.*lags);
            colix = [1:2:2*nlags];
            
            E_tmp = zeros(nlags,1);
            L_tmp = zeros(nlags,1);
            for lag = 1:nlags
                icol = colix(lag);
                
                % select lag
                M_hilc = lagbrain(:,[0,1]+icol);
                
                % real CMI and randomized Z with loop inside function
                % true and boot MI
                % B1_E: MI(env ; meg | lip)     and     C1_E: MI(env ; meg | lip_shuf)
                E_tmp(lag) = mi_gg(E_hilc, M_hilc);

                % B1_L: MI(lip ; meg | env)     and     C1_L: MI(lip ; meg | env_shuf)
                L_tmp(lag) = mi_gg(L_hilc, M_hilc);
            


            end
            MI_E_tmp(:,ivox) = E_tmp;
            
            MI_L_tmp(:,ivox) = L_tmp;
        end
        MI_E(freq,:,:) = MI_E_tmp;
        
        MI_L(freq,:,:) = MI_L_tmp;
        toc
    end
    % flush
    clear meg, clear env, clear brain, clear stim
    

    % save data to disk
    % sname as above
    save(sname, 'MI_E', 'MI_L', 'ARG', 'ARGF', 'ARGM');
    toc
end



% send message from server to telegram
if COMP == 1
    bot = returnBot('Hanzule');
    [~,sysname] = system('hostname');
    messagetext = sprintf('%s has finished on %s',mfilename, sysname);
    tgprintf(bot,messagetext);
end


fprintf('\n');
fprintf('Everything has finished successfully! \n');

