function SortedFeatures = sort_features(FeatStruct,Labels)
%
% SortedFeatures = sort_features(FeatStruct,Labels)
% sorts Features into a structure with only the features that are given in
% Labels
%
% Parameters
% ----------
% FeatStruct : structure
%       data structure returned by createFeature_v*
% Labels : cell 
%       contains str with names of features that you want
%
% Returns
% -------
% SortedFeatures : structure
%       contains features and details. Features are in order given by
%       Labels.
%

% chose features and sort them together in a nested cell array
ChosenFeat = cell(1,2);
for icond = 1:FeatStruct.ncond
    ChosenFeat{icond} = cell(FeatStruct.nfiles(icond),1);
    for ifile = 1:FeatStruct.nfiles(icond)
        ChosenFeat{icond}{ifile} = [];
        for istim = 1:length(Labels)
            ChosenFeat{icond}{ifile} = [ChosenFeat{icond}{ifile}; ...
                FeatStruct.(Labels{istim}).data{icond}{ifile}];
        end
    end
end

% feature dimension size, continuous
contFeat = [];
DimNames = {};
for istim = 1:length(Labels)
    ndim = FeatStruct.(Labels{istim}).ndims;
    NDims(istim) = ndim;
    DimNames = cat(2,DimNames{:}, repmat(Labels(istim),1,ndim));
    cont = FeatStruct.(Labels{istim}).continuous;
    contFeat = [contFeat; ones(ndim,1).*cont];
end
contFeat = logical(contFeat);
discFeat = (contFeat==0);

nmaxcoeff = sum(NDims) + 1; % two times because we use real and imag spearately


% output
SortedFeatures.ChosenFeat = ChosenFeat;
SortedFeatures.DimNames = DimNames';
SortedFeatures.NDims = NDims;
SortedFeatures.nmaxcoeff = nmaxcoeff;
SortedFeatures.contFeat = contFeat;
SortedFeatures.discFeat = discFeat;

end
