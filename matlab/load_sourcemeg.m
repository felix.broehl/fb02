function [meg,cond,reidx] = load_sourcemeg(Csys,blocks,isub)
%
% [meg,cond,reidx] = load_sourcemeg(Csys,blocks)
% load source meg data for 
%
% Parameters
% ----------
%   Csys : 1 or 2, indicating linux or windows system 
%   blocks : whcih blocks to load
%   isub : subject index
%
% Returns
% -------
%   meg : cell array with condition and trial wise meg data
%   cond : index for condition sequence
%   reidx : trial index for each condition
%

COMP = Csys; % either 1 or two for this specific dataset
Step0_setup

% empty index
reidx = [];
cond = {};
% empty meg
meg = [];

% for each block, concat all trials and compute MI
iter = 1;
for iblock = blocks

    dataname = sprintf('%ssourcespace_sub%d_block%d_A.mat', DIR.source, isub, iblock);
    if ~exist(dataname,'file')
        fprintf('No data file found!! \n');
        continue;
    end

    tic
    % load data and sorted envelopes from file
    data = load(dataname);
    fprintf('loading sub %d block %d \n', isub, iblock);
    
    % individual condition and trial order
    reidx{iter} = data.ARG.trialinfo(:,4);
    cond{iter} = data.ARG.trialinfo(1,5);
    % data
    meg = cat(1,meg,data.source_raw);
    
    iter = iter + 1;
end

end
