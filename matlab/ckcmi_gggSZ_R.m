function [Itrue,Iboot] = ckcmi_gggSZ_R(x, y, z, Nboot)

% HACKED FUNCTION SHUFFLING variable z in time!
% 
% CMI_GGG Conditional Mutual information (CMI) between two Gaussian variables
%        conditioned on a third
%
%   I = cmi_ggg(x,y,z) returns the CMI between two (possibly multidimensional)
%   Gassian variables, x and y, conditioned on a third, z, with bias correction.
%   If x / y / z are multivariate rows must correspond to samples, columns
%   to dimensions/variables. (Samples first axis)
%
%   biascorrect : true / false option (default true) which specifies whether
%   bias correction should be applied to the esimtated MI.
%   demeaned : false / true option (default false) which specifies whether the
%   input data already has zero mean (true if it has been copula-normalized)
%   Nboot : number of permutations

% ensure samples first axis for vectors

ln2 = log(2);
Ntrl = size(x,1);
Nvarx = size(x,2);
Nvary = size(y,2);
Nvarz = size(z,2);


x = bsxfun(@minus,x,sum(x,1)/Ntrl);
y = bsxfun(@minus,y,sum(y,1)/Ntrl);
z = bsxfun(@minus,z,sum(z,1)/Ntrl);

% joint variable
xyz = [x y z];
Cxyz = (xyz'*xyz) / (Ntrl - 1);
% submatrices of joint covariance
Nvaryz = Nvary + Nvarz;
Nvarxyz = Nvarx + Nvaryz;
zidx = (Nvarx + Nvary + 1):Nvarxyz;
Cz = Cxyz(zidx,zidx);

idx = (Nvarx + 1):Nvarxyz;
Cyz = Cxyz(idx, idx);

Nvarxz = Nvarx + Nvarz;
Cxz = zeros(Nvarxz);
xidx = 1:Nvarx;
Cxz(xidx,xidx) = Cxyz(xidx,xidx);
zidxxz = (Nvarx+1):Nvarxz;
Cxz(xidx,zidxxz) = Cxyz(xidx,zidx);
Cxz(zidxxz,xidx) = Cxyz(zidx,xidx);
Cxz(zidxxz,zidxxz) = Cxyz(zidx,zidx);

chCz = chol(Cz);
chCxz = chol(Cxz);
chCyz = chol(Cyz);
chCxyz = chol(Cxyz);

% entropies in nats
% normalisations cancel for cmi
HZ = sum(log(diag(chCz))); % + 0.5*Nvarz*log(2*pi*exp(1));
HXZ = sum(log(diag(chCxz))); % + 0.5*(Nvarx+Nvarz)*log(2*pi*exp(1));
HYZ = sum(log(diag(chCyz))); % + 0.5*(Nvary+Nvarz)*log(2*pi*exp(1));
HXYZ = sum(log(diag(chCxyz))); % + 0.5*(Nvarx+Nvary+Nvarz)*log(2*pi*exp(1));



% convert to bits
Itrue = (HXZ + HYZ - HXYZ - HZ) / ln2;


%--------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bootstrap

Iboot = zeros(1,Nboot);

% for indexing z in joint variable
zcol = Nvarx + Nvary + 1;

if Nboot
  off = floor(Ntrl/100/2);
  for r=1:Nboot
    % random shift variable z in time
    rdelay  = ceil(rand*(Ntrl-off))+off;
    
    % joint variable
    % xyz = [x y z_shifted];
    xyz(:,[zcol:zcol+Nvarz-1]) =  z([[rdelay:Ntrl],[1:rdelay-1]],:);
    
    Cxyz = (xyz'*xyz) / (Ntrl - 1);
    % submatrices of joint covariance
    Nvaryz = Nvary + Nvarz;
    Nvarxyz = Nvarx + Nvaryz;
    zidx = (Nvarx + Nvary + 1):Nvarxyz;
    Cz = Cxyz(zidx,zidx);
    
    idx = (Nvarx + 1):Nvarxyz;
    Cyz = Cxyz(idx, idx);
    
    Nvarxz = Nvarx + Nvarz;
    Cxz = zeros(Nvarxz);
    xidx = 1:Nvarx;
    Cxz(xidx,xidx) = Cxyz(xidx,xidx);
    zidxxz = (Nvarx+1):Nvarxz;
    Cxz(xidx,zidxxz) = Cxyz(xidx,zidx);
    Cxz(zidxxz,xidx) = Cxyz(zidx,xidx);
    Cxz(zidxxz,zidxxz) = Cxyz(zidx,zidx);
    
    chCz = chol(Cz);
    chCxz = chol(Cxz);
    chCyz = chol(Cyz);
    chCxyz = chol(Cxyz);
    
    % entropies in nats
    % normalisations cancel for cmi
    HZ = sum(log(diag(chCz))); % + 0.5*Nvarz*log(2*pi*exp(1));
    HXZ = sum(log(diag(chCxz))); % + 0.5*(Nvarx+Nvarz)*log(2*pi*exp(1));
    HYZ = sum(log(diag(chCyz))); % + 0.5*(Nvary+Nvarz)*log(2*pi*exp(1));
    HXYZ = sum(log(diag(chCxyz))); % + 0.5*(Nvarx+Nvary+Nvarz)*log(2*pi*exp(1));
    
    
    
    % convert to bits
    Iboot(r) = (HXZ + HYZ - HXYZ - HZ) / ln2;
    
    
  end
  
end

