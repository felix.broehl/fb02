%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% go through selected main results (based on analysis without
% bootstrapping) and generate a randomization distribution only for the 
% features relevant for these effects.
%
% assign target and condition indices for stimulus matrix
% assign correct optimal lag for target and condition features
%
% we are interested only in the unheard AudFeat tracking during V-only
% trials!
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clearvars

% directories
COMP = 2; % set environment to local windows machine
Step0_setup;

% only compute something for V-only trials
blocks = [9:12]; 

poolobj = manageParpool(-2);
numcores = poolobj.NumWorkers;


% condition label for plot header
if blocks(1) == 1
    condlabel = 'auditory';
    PredOrder = [2,1]; % target: VisFeats
elseif blocks(1) == 9
    condlabel = 'visual';
    PredOrder = [1,2]; % target: AudFeats
end
ARG.condlabel = condlabel;

% directories and save name
snamebase = sprintf('%sCMI_partialLoo_%s',DIR.cmi,condlabel);


% data dir
datafolder = dir(sprintf('%s*.mat',DIR.source));

% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);

% parameters --------------------------------------------------------------
ARG.freq_range = [0.5, 1; 1, 3; 2, 4; 3, 6; 4, 8]; % delta, theta and what not...
ARG.srate = 50; % in Hz

ARG.OFFSET = round(1.5 * ARG.srate); % 500 ms - cut out evoked response
ARG.MAXL = round(4 * ARG.srate); % max length of 4.5 s (shortest env: 5.52 s)
envlen = ARG.MAXL;
ARG.nrand = 20;
nrand = ARG.nrand;

ARG.lagwindow = round([-0.06:0.02:0.06] * ARG.srate);
lagwindow = ARG.lagwindow;
nlags = length(lagwindow);

goodsub = find([SUBS.exclude]==0);
nsubs = length(goodsub);
nfreq = size(ARG.freq_range,1);


% Filter parameters
ARGF.rate = ARG.srate;
ARGF.mode = 'butter';
ARGF.order = 3; % filter order
ARGF.window = 1; % symmetric padding


%% load ROI data ----------------------------------------------------------
% contains intersected voxel for primary cortices in roivoxins
load ROIVOXELS.mat
load pROI_voxel.mat % -> pROI structure
ARG.pROI = pROI;
allvox = pROI.all.inroivoxins;
nvox = numel(allvox);

[~,audvox] = intersect(allvox,[pROI.auditory.left.inroivoxins; pROI.auditory.right.inroivoxins]);
[~,visvox] = intersect(allvox,[pROI.visual.left.inroivoxins; pROI.visual.right.inroivoxins]);
roivox = {audvox, visvox};
nroi = numel(roivox); % auditory and visual cortex

% load voxel-wise optlag data ---------------------------------------------
load Optlags_GLM.mat
ARG.Optlags = Optlags;

ngrouppred = size(Optlags,2);





%% load features ----------------------------------------------------------
% choose groups of predictors
AudFeat = {'aud_env', 'aud_slope', 'aud_pitch'};
VisFeat = {'lip_area', 'lip_slope', 'lip_width'};
% define features to load here
AllLabels = {AudFeat{:}, VisFeat{:}};
ARG.AllLabelsUsed = AllLabels;

ARG.GroupNames = {'AudFeat','VisFeat'};
ARG.AudFeat = AudFeat;
ARG.VisFeat = VisFeat;

% create Features
cfg = [];
cfg.srate = ARG.srate;
AllFeatures = createFeatures_v3(cfg,DIR);


% sort features -----------
Features = sort_features(AllFeatures, AllLabels);
contFeat = Features.contFeat;
discFeat = Features.discFeat;
nenv = sum(Features.NDims);
nmaxcoeff = Features.nmaxcoeff;


% define Predictors ------
% needed to assign the optimal lags for the multivariate predictors
PredNames = AllLabels;
Predictors = sort_predictors(Features,PredNames);
predcoeff = cellfun(@length, Predictors,'UniformOutput',false);

% leave-one-out predictors
LooPredictors = sort_predictors(Features,PredNames,true);


Aud_indx = find(ismember(AllLabels,AudFeat)) + [0;nenv];
Vis_indx = find(ismember(AllLabels,VisFeat)) + [0;nenv];
Aud_indx = Aud_indx(:);
Vis_indx = Vis_indx(:);


ARG.Predictors = Predictors;
ARG.predcoeff = predcoeff;


%% define main effects

% the two effects run over all roi voxels, i.e. include 4 effects in total
Effects{1}.freqband = 1;
Effects{1}.predictors = [1,2,3];
Effects{1}.loopredictors = LooPredictors([1,2,3]);

Effects{2}.freqband = 2;
Effects{2}.predictors = [1,2,3];
Effects{2}.loopredictors = LooPredictors([1,2,3]);


nEffects = length(Effects);
predlen = length(PredNames)/2;
loopredlen = predlen;
ARG.predlen = predlen;


%% load voxel-wise optlag data
load Optlags_MI.mat % -> OptlagsRoivox 
OptlagsRoivox = round(OptlagsRoivox ./ 1000 * ARG.srate); % convert to samples


% warn if any of the optimal lags in lower than the min of the lagwindow
if any(OptlagsRoivox(:) < min(lagwindow))
    fprintf('Optimal lag is to small for lag window!! \n');
    error('Optimal lag is to small for lag window!! \n');
end



%% main

for isub2 = 1:nsubs
    isub = goodsub(isub2);
    
    % create filename and check if it already exists
    sname = sprintf('%s_sub%02d_rand.mat', snamebase,isub);
    if exist(sname,'file')
        fprintf('file already computed \n');
        %continue;
    end
    
    % all blocks structures
    meg = []; env = [];
    
    % for each block, concat all trials and compute MI
    for iblock = blocks
        
        dataname = sprintf('%ssourcespace_sub%d_block%d_A.mat', DIR.source, isub, iblock);
        if ~exist(dataname,'file')
            fprintf('No data file found!! \n');
            continue;
        end
        
        tic
        % load data and sorted envelopes from file
        data = load(dataname);
        fprintf('loading sub %d block %d \n', isub, iblock);
        
        % sort stims and concat trials of each block
        reidx = data.ARG.trialinfo(:,4);
        cond = data.ARG.trialinfo(1,5);
        env = cat(2,env,{Features.ChosenFeat{cond}{reidx}});
        % data
        meg = cat(1,meg,data.source_raw);
    end
    clear data
    
    % STIMULI ----------------------------------
    % cut to same length
    env = cellfun(@(x) x(:,ARG.OFFSET+[1:envlen]),env,'UniformOutput',false);
    env = cat(3,env{:});
    
    % MEG ----------------------------------
    % select only primary ROIs from meg and cut to same length
    meg = meg(:,pROI.all.inroivoxins,ARG.OFFSET+[1:envlen]);
    meg = permute(meg,[2,3,1]);
    [~,meglen,ntrl] = size(meg);

    
    
    
    % filter, hilbert, MI ---------------------------------------------
    % MI [envelopes x frequencies x voxels]
    CMI = zeros(predlen,nfreq,nvox);
    CMIboot = zeros(predlen,nfreq,nvox,nrand);
    
    for ieff = 1:nEffects
        % get effect parameters
        freq = Effects{ieff}.freqband;
        predictors = Effects{ieff}.predictors;
        loopredictors = Effects{ieff}.loopredictors;
        
        tic
        fprintf('filter data into %.2f to %.2f Hz band \n', ARG.freq_range(freq,:));
        
        % FILTER stimuli
        cfg = [];
        cfg.continuous = true;
        cfg.fs = ARGF.rate;
        cfg.filter = true;
        cfg.filtertype = ARGF.mode;
        cfg.filterord = ARGF.order;
        cfg.filterwindow = ARGF.window;
        cfg.hilbert = true;
        cfg.zcopnorm = true;

        % filter and zscore features
        cfg.filterfreq = ARG.freq_range(freq,:);
        stim_filt = fb_preprocess_trials(cfg,env);
    
        % filter meg
        cfg.filterfreq = ARG.freq_range(freq,:);
        brain_filt = fb_preprocess_trials(cfg,meg);
        % split so we already copnorm and we need not copy whole matrix for
        % each worker
        brain_real = brain_filt(1:nvox,:,:);
        brain_imag = brain_filt(nvox+1:end,:,:);
        toc
        
        
        
        % concatenate trials
        
        stim_hilc = reshape(stim_filt,[2*nenv,envlen*ntrl])';
        
        
        fprintf('compute MI over voxels\n');
        tic
        CMIvox = zeros(predlen,nlags,nvox);
        CMIvoxboot = zeros(predlen,nlags,nvox,nrand);
        
        parfor ivox = 1:nvox
            % index MEG ------------
            cutbrain = cat(1,brain_real(ivox,:,:),brain_imag(ivox,:,:)); % choose vox
            M_hilc = reshape(cutbrain,[2,envlen*ntrl])';
            
            % fetch stim, voxel and freq dependent lag
            voxlags = OptlagsRoivox(:,freq,ivox);
            meglag = max(voxlags);
            stimlag = abs(voxlags - max(voxlags));
            
            % align stim specific lags
            expandlags = zeros(2*nenv,1);
            expandlags(Aud_indx) = deal(stimlag(1));
            expandlags(Vis_indx) = deal(stimlag(2));
            
            % create lag matrix
            S_hilc = timealignPredictors(stim_hilc,expandlags);
            S_hilc = generateLags(S_hilc,lagwindow);
            
            colix = [1:2*nenv:2*nenv*nlags];
            
            
            % allocate empty lag variables
            CMItmp = zeros(predlen,nlags);
            CMItmpboot = zeros(predlen,nlags,nrand);
            
            % loop over single features ---------------
            for imod = 1:predlen
                ipred = predictors(imod);
                loopred = loopredictors{imod};
                % loop over time window
                for lag = 1:nlags
                    % index target predictor and all others
                    tcol = [0,nenv] + ipred + colix(lag) - 1;
                    ccol = [0,nenv] + loopred + colix(lag) - 1;
                    ccol = ccol(:);

                    % real CMI and randomized Z with loop inside function
                    % B1_E: MI(env ; meg | lip)     and     C1_E: MI(env ; meg | lip_shuf)
                    [CMItmp(imod,lag), CMItmpboot(imod,lag,:)] = ckcmi_gggSYSZ_R(S_hilc(:,tcol), M_hilc, S_hilc(:,ccol), nrand);
                    
                end
            end
            
            
            % index to voxels
            CMIvox(:,:,ivox) = CMItmp;
            CMIvoxboot(:,:,ivox,:) = CMItmpboot;

        end
        % sum over lags
        CMI(:,freq,:) = sum(CMIvox,2);
        CMIboot(:,freq,:,:) = sum(CMIvoxboot,2);
        toc
    end
    % flush
    clear meg, clear env, clear brain, clear stim
    

    % save data to disk
    % sname as above
    save(sname, 'CMI', 'CMIboot', 'ARG', 'ARGF');
    toc
end


% send message from server to telegram
if COMP == 1
    bot = returnBot('Hanzule');
    [~,sysname] = system('hostname');
    messagetext = sprintf('%s has finished on %s',mfilename, sysname);
    tgprintf(bot,messagetext);
end



fprintf('\n');
fprintf('Everything has finished successfully! \n');

