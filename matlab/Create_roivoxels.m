

% generate voxel list within INGRID and ROIS

load('Y:\Matlab\fieldtrip-20190905\template\headmodel\standard_mri.mat')


load('sourceatlas_grid.mat')

% use AAL atlas to find only cortical areas of interest, that is, without subcortical areas
% the following regions can be excluded
RoiOut = [ 21,22, 27,28,37:40, 71:78,91:116];
roivoxout = find(ismember(sourceatlas.tissue,RoiOut));

sourceX = sourcemodel;
sourceX.inside(roivoxout) = 0;

% remove edges of volume. These can often induce artifacts
INSIDECUBE = reshape(sourceX.inside,[sourceX.dim]);
INSIDECUBE2 = INSIDECUBE;
j = find(INSIDECUBE(:));
retain = zeros(length(j),1);
[Xi,Yi,Zi] = ind2sub(sourceX.dim,j);
nb= 0;
for l=1:length(j)
  rx = Xi(l)+[-1:1];
  rx = rx(find(rx>0));
  rx = rx(find(rx<=sourceX.dim(1)));
  
  ry = Yi(l)+[-1:1];
  ry = ry(find(ry>0));
  ry = ry(find(ry<=sourceX.dim(2)));
  
  rz = Zi(l)+[-1:1];
  rz = rz(find(rz>0));
  rz = rz(find(rz<=sourceX.dim(3)));
  
  tmp = INSIDECUBE(rx,ry,rz);
  
  if sum(tmp(:))<27
    INSIDECUBE2(Xi(l),Yi(l),Zi(l)) = 0;
    nb  =nb+1;
  else
    retain(l) = 1;
  end
end
% remove brainstem
INSIDECUBE2([10:18],[12:22],[1:12]) = 0;
sourceX.inside = INSIDECUBE2(:);

% add back cortical ROIs to ensure we fully cover these
RoiUse = ones(1,118);
RoiUse(RoiOut) = 0;
RoiUse = find(RoiUse);
roivoxin = find(ismember(sourceatlas.tissue,RoiUse));
sourceX.inside(roivoxin) = 1;

roivoxins = find(sourceX.inside);

save('ROIVOXELS.mat','roivoxins');


cfg            = [];
cfg.parameter  = 'inside';
cfg.interpmethod = 'nearest';
source2  = ft_sourceinterpolate(cfg, sourceX , mri);


cfg.method        = 'ortho';
cfg.interactive   = 'yes';
cfg.funparameter  = 'inside';
cfg.atlas        = 'D:\CKDATA\MatBox\toolboxes\fieldtrip-20150318\template\atlas\aal\ROI_MNI_V4.nii';
cfg.maskparameter = cfg.funparameter;
ft_sourceplot(cfg, source2);
