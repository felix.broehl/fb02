%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% preprocess MEG data, cut to envelope length and source reconstruct
% then only keep cortical voxels and save to file
%
% take two reference features (broadband env, lip area) and track both in
% all voxels
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clearvars

% directories
COMP = 2; % set environment to local windows machine
Step0_setup;

blocks = [1:4]; 

poolobj = manageParpool(-2);
numcores = poolobj.NumWorkers;


% condition label for plot header
if blocks(1) == 1
    condlabel = 'auditory';
    PredOrder = [2,1]; % target: VisFeats
elseif blocks(1) == 9
    condlabel = 'visual';
    PredOrder = [1,2]; % target: AudFeats
end
ARG.condlabel = condlabel;

% directories and save name
snamebase = sprintf('%sMI_CoreFeatures_%s',DIR.mi,condlabel);


% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);

% parameters --------------------------------------------------------------
ARG.freq_range = [0.5, 2; 1, 4; 2, 4; 3, 6; 4, 8; 0.5, 8]; % delta, theta and what not...
ARG.srate = 50; % in Hz
ARG.nreps = 100; % N samplings with replacement for bootstrapping
nreps = ARG.nreps;

ARG.OFFSET = round(1.5 * ARG.srate); % 500 ms - cut out evoked response
ARG.MAXL = round(4 * ARG.srate); % max length of 4.5 s (shortest env: 5.52 s)
envlen = ARG.MAXL;

ARG.lagwindow = round([0.06:0.02:0.14] * ARG.srate);
lagwindow = ARG.lagwindow;
nlags = length(lagwindow);

goodsub = find([SUBS.exclude]==0);
nsubs = length(goodsub);
nfreq = size(ARG.freq_range,1);

% Filter parameters
ARGF.rate = ARG.srate;
ARGF.mode = 'butter';
ARGF.order = 3; % filter order
ARGF.window = 1; % symmetric padding


%% load features ----------------------------------------------------------
% choose groups of predictors
AudFeat = {'aud_env','aud_slope','aud_pitch'};
VisFeat = {'lip_area','lip_slope','lip_width'};
% define features to load here
AllLabels = {AudFeat{:}, VisFeat{:}};
ARG.AllLabelsUsed = AllLabels;

ARG.GroupNames = {'AudFeat','VisFeat'};
ARG.AudFeat = AudFeat;
ARG.VisFeat = VisFeat;

% create Features
cfg = [];
cfg.srate = ARG.srate;
AllFeatures = createFeatures_v3(cfg,DIR);


% sort features -----------
Features = sort_features(AllFeatures, AllLabels);
contFeat = Features.contFeat;
discFeat = Features.discFeat;
nenv = sum(Features.NDims);
nmaxcoeff = Features.nmaxcoeff;


% define Predictors ------
% needed to assign the optimal lags for the multivariate predictors
PredNames = {AudFeat, VisFeat};
Predictors = sort_predictors(Features,PredNames);
predlen = length(PredNames);
predcoeff = cellfun(@length, Predictors,'UniformOutput',false);


%% main

for isub2 = 1:nsubs
    isub = goodsub(isub2);
    
    % create filename and check if it already exists
    sname = sprintf('%s_sub%02d.mat', snamebase, isub);
    if exist(sname,'file')
        fprintf('file already computed \n');
        %continue;
    end
    
    % all blocks structures
    meg = []; env = [];
    
    % for each block, concat all trials and compute MI
    for iblock = blocks
        
        dataname = sprintf('%ssourcespace_sub%d_block%d_A.mat', DIR.source, isub, iblock);
        if ~exist(dataname,'file')
            fprintf('No data file found!! \n');
            continue;
        end
        
        % load data and sorted envelopes from file
        data = load(dataname);
        fprintf('loading sub %d block %d \n', isub, iblock);
        
        % sort stims and concat trials of each block
        reidx = data.ARG.trialinfo(:,4);
        cond = data.ARG.trialinfo(1,5);
        env = cat(2,env,{Features.ChosenFeat{cond}{reidx}});
        % data
        meg = cat(1,meg,data.source_raw);
    end
    clear data
    
    % STIMULI ----------------------------------
    % cut to same length
    env = cellfun(@(x) x(:,ARG.OFFSET+[1:envlen]),env,'UniformOutput',false);
    env = cat(3,env{:});
    
    % MEG ----------------------------------
    % select only primary ROIs from meg and cut to same length
    meg = meg(:,:,ARG.OFFSET+[1:envlen]);
    meg = permute(meg,[2,3,1]);
    [nvox,meglen,ntrl] = size(meg);
    

    % filter, hilbert, MI ---------------------------------------------
    % MI [envelopes x frequencies x voxels]
    MI = zeros(nfreq,predlen,nvox);
    MIRand = zeros(nfreq,predlen,nvox,nreps);
    for freq = 1:nfreq
        tic
        fprintf('filter data into %.2f to %.2f Hz band \n', ARG.freq_range(freq,:));
        
        % FILTER stimuli
        cfg = [];
        cfg.continuous = true;
        cfg.fs = ARGF.rate;
        cfg.filter = true;
        cfg.filtertype = ARGF.mode;
        cfg.filterord = ARGF.order;
        cfg.filterwindow = ARGF.window;
        cfg.hilbert = true;
        cfg.zcopnorm = true;

        % filter and zscore features
        cfg.filterfreq = ARG.freq_range(freq,:);
        stim_filt = fb_preprocess_trials(cfg,env);
    
        % filter meg
        cfg.filterfreq = ARG.freq_range(freq,:);
        brain_filt = fb_preprocess_trials(cfg,meg);
        % split so we already copnorm and we need not copy whole matrix for
        % each worker
        brain_real = brain_filt(1:nvox,:,:);
        brain_imag = brain_filt(nvox+1:end,:,:);
        toc
        
        % concatenate stim trials
        stim_hilc = reshape(stim_filt,[2*nenv,envlen*ntrl])';
        
        
        MItmp = zeros(nlags,predlen,nvox);
        MItmpRand = zeros(nlags,predlen,nvox,nreps);
        fprintf('compute MI over voxels\n');
        parfor ivox = 1:nvox
            % index MEG ------------
            cutbrain = cat(1,brain_real(ivox,:,:),brain_imag(ivox,:,:)); % choose vox
            M_hilc = reshape(cutbrain,[2,envlen*ntrl])';
            
            % create lag matrix
            S_hilc = generateLags(stim_hilc,lagwindow);
            colix = [1:2*nenv:2*nenv*nlags];
            
            
            tmp = zeros(nlags,predlen);
            tmpRand = zeros(nlags,predlen,nreps);
            for s = 1:predlen
                for lag = 1:nlags
                    ipred = reshape([0,nenv]+Predictors{s},nenv,1) + colix(lag) - 1;
                    
                    % true and random MI-----------------------------------
                    [tmp(lag,s), tmpRand(lag,s,:)] = mi_ggSY_R(S_hilc(:,ipred),M_hilc,nreps);
                    
                end
            end
            MItmp(:,:,ivox) = tmp;
            MItmpRand(:,:,ivox,:) = tmpRand;
        end
        MI(freq,:,:) = sum(MItmp,1);
        MIRand(freq,:,:,:) = sum(MItmpRand,1);
        
        
    end
    % flush
    clear meg, clear env, clear brain, clear stim

    % save data to disk
    % sname as above
    save(sname, 'MI', 'MIRand', 'ARG', 'ARGF');
    toc
end


% send message from server to telegram
if COMP == 2
    bot = returnBot('Hanzule');
    [~,sysname] = system('hostname');
    messagetext = sprintf('%s has finished on %s',mfilename, sysname);
    tgprintf(bot,messagetext);
end


fprintf('\n');
fprintf('Everything has finished successfully! \n');
