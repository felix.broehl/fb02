function stim = order_features(Features,cond,reidx)
%
% stim = order_features(Features,cond,reidx)
% take participant specific order indices and sort stimuli accordingly
%

% empty stim
stim = [];

% check input for block length
if iscell(cond)
    nblocks = length(cond);
else
    nblocks = 1;
end

for iblock = 1:nblocks
    ic = cond{iblock};
    it = reidx{iblock};
    stim = cat(2,stim,{Features.ChosenFeat{ic}{it}});
end

end
