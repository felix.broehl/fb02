%
% take cmi data and plot the ratio between AudFeat tracking and tracking of
% the principal feature in the specific cortex
%
% then save data to file for final figure
%

close all
clearvars

COMP = 2;
Step0_setup;

load('C:\Users\fbroehl\Documents\FB02\finalfigures\CMI_Statistics_v3.mat');

[nrows,nfreq,nsub,ncol] = size(CMIdata);

figure('Position',[680 309 800 650]);
iter = 1;
for irow = 1:nrows
    fraction = zeros(nsub,nfreq);
    YLim = [];
    for freq = 1:nfreq
        % partial MI - auditory divided by visual
        midata = sq(CMIdata(irow,freq,:,[2,4]));
        
        fraction = midata(:,1) ./ midata(:,2);
    
        hh(freq) = subplot(nrows,nfreq,iter);
        axis square
        pos = hh(freq).Position;
        barwithdots(fraction);
        YLim(freq) = hh(freq).YLim(2);

        hold on
        plot([0.5,nfreq+0.5],[1,1],'k--','LineWidth',0.1);
        
        % text median value
        mediantext = sprintf('%.2f',string(median(fraction)));
        text(0.75,YLim(1)*0.8,mediantext,'HorizontalAlignment','center','Color',[0.5,0,0]);
        
        if irow == 1
            header = sprintf('%.1f - %.1f Hz', freqrange(freq,:));
            title(header);
        end
        
        if freq == 1
            labeltext = sprintf(['%s cortex' newline '%s trials'], ...
                ARG.rowroi{irow},ARG.rowcon{irow});
            ylabel(labeltext);
        end
        
        hh(freq).Position = pos;
        iter = iter + 1;
    
    end
    [hh(:).YLim] = deal([0, max(YLim)]);
end

header = 'partial MI - AudFeat / VisFeat';
ckfiguretitle(header);

% save figure
snamef = sprintf('%sPartialMIRatio',DIR.cmi);
print('-dpng',snamef);





%% pre define effects we want to look at closer and save to file

% AC: MI(AudFeat) V-only ./ MI(AudFeat) A-only
% VC: MI(AudFeat) V-only ./ MI(LipFeat) V-only
% in the lowest two freq bands -> 4 comps

Ratio.cond = [2,2,2,2; 1,2,1,2]';
Ratio.stim = [1,1,1,1; 1,2,1,2]';
Ratio.freq = [1,1,2,2];
Ratio.iroi = [1,2,1,2; 1,2,1,2]';
Ratio.neffects = numel(Ratio.freq);
Ratio.freqrange = dataARG.freq_range;

for ieff = 1:Ratio.neffects
    icond = Ratio.cond(ieff,:);
    istim = Ratio.stim(ieff,:) .* 2;
    freq = Ratio.freq(ieff);
    iroi = Ratio.iroi(ieff,:);
    % get row and column index
    irow = icond + (iroi-1) .* 2;
    Ratio.data{ieff} = sq(CMIdata(irow(1),freq,:,istim(1))) ./ sq(CMIdata(irow(2),freq,:,istim(2)));
end

sname = sprintf('../finalfigures/Data_CMI_Ratio.mat');
save(sname,'Ratio');



%% test some stuff
rowidx = [1,2,4];
colidx = [1,1,2];

figure
hold on
for iter = 1:3
    ratio = log(sq(CMIdata(rowidx(iter),colidx(iter),:,[2,4])));
    plot(ratio(:,1),ratio(:,2),'.','MarkerSize',12);
end
axis square


%% test if ratio correlates with behavior
load('../finalfigures/Behavior.mat');
HR = Behavior.Correct(:,2);

figure
for iter = 1:4
    mi = Ratio.data{iter};
    hr = HR;
    offset = ones(size(mi));
    w = [offset, mi]\hr;
    
    dx = [1, 1; 0, 1.5]';
    fx = dx * w;
    
    rho = corr(mi,hr);
    
    hh = subplot(2,2,iter);
    plot(mi,hr,'o');
    limx = hh.XAxis.Limits;
    hold on
    plot(dx(:,2),fx,'k-');
    hh.XAxis.Limits = limx;
    
    header = sprintf(['r=%.3f' newline 'column %d'],rho, iter);
    title(header);
end
