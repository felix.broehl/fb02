clear all

COMP=2;
Step0_setup; % setup directories
sourcemod = [DIR.ft 'template\sourcemodel\standard_sourcemodel3d6mm'];
templateT1=[DIR.ft 'template\anatomy\single_subj_T1_1mm.nii'];
atlasdir = [DIR.ft 'template\atlas\aal\ROI_MNI_V4.nii'];

expname = 'AVentrain';
[SUBS, strcond, filenames]=subject_database(expname);

nblockexp = 1:12; % experimental blocks across both sessions
goodsub = find([SUBS.exclude]==0);


load('sourceatlas_grid.mat');

% 1) only cortical areas of interest
%roivoxel = find(ismember(sourceatlas.tissue,roisel));
%[~,roivoxins]=intersect(find(sourcemodel.inside),roivoxel);
% 2) all ingrid voxels excl. cerebellum (atlas 21,22, 27,28, 71-78, 91 -116) and above a certain z-threshold
load('ROIVOXELS.mat','roivoxins');
nvoxfinal=length(roivoxins);

%% MANUAL TARGET PROJECTION (for all voxels)
% load all blocks from one condition/project/smooth

load('sourceatlas_grid.mat');

allconditions = {'auditory' 'audiovisual' 'visual'};
idx=2;                      % indexes spectral filtering and cut out timings
load('target_onsets.mat');  % [target_onset; target_str]



for isub2=1:18
  isub=goodsub(isub2);
  for iblock =1:12
    
    xname = sprintf('%ssub%d_block%d_targets%d_smooth_norm%d.mat',DIR.tgt,isub,iblock,2,33);
    if exist(xname)==2
      xname
      continue;
end
    tic
    load(sprintf('%sA_sub%d_block%d_clean',DIR.pre,isub,iblock),'data');
    
    % highpass filter (0.8 Hz) before source projection
    cfg             = [];
    cfg.hpfilter    = 'yes';
    cfg.hpfreq      = 0.8;
    cfg.hpfilttype  = 'but';
    cfg.hpfiltord   = 4;
    cfg.hpfiltdir   = 'twopass';
    cfg.detrend     = 'no';
    data            = ft_preprocessing(cfg, data);
    
    % lowpass filter (30 Hz) before source projection
    cfg             = [];
    cfg.lpfilter    = 'yes';
    cfg.lpfreq      = 30;
    cfg.lpfilttype  = 'but';
    cfg.lpfiltord   = 4;
    cfg.lpfiltdir   = 'twopass';
    cfg.detrend     = 'no';
    data            = ft_preprocessing(cfg, data);
    
    % cut out shorter segments
    cfg=[];
    cfg.toilim    = [0 8];
    data = ft_redefinetrial(cfg, data);
    
    ntrial = length(data.trial);
    trialinf = data.trialinfo;
    
    ntime=size(data.trial{1},2);
    timeinf = data.time{1};
    
    % numerically increase values
    for itrial=1:length(data.trial)
      data.trial{itrial}=data.trial{itrial}*1e12;
    end
    
    % get data out of fieldtrip structure
    data2=NaN([length(data.trial),size(data.trial{1})]);
    for itrial=1:length(data.trial)
      data2(itrial,:,:) = data.trial{itrial};
    end
    data=[];
    
    % cut out targets ------------------------------------------
    % target type of block
    icat=trialinf(1,5);
    trial_ons = NaN(ntrial,1);
    % get target onsets for each trial
    for itrial = 1:ntrial
      trial_ons(itrial) = target_onset(trialinf(itrial,4),icat);
    end
    % find sampling point corresponding to each target onset
    onsample = NaN(ntrial,1);
    for itrial=1:ntrial
      onsample(itrial) = nearest(timeinf,trial_ons(itrial),1);
    end
    % cut out windows around targets
    ntarg = 271;    % 900 ms
    
    
    s = size(data2);
    data3 = zeros(s(1),s(2),ntarg);
    for itrial=1:ntrial
      data3(itrial,:,:) = sq(data2(itrial,:,onsample(itrial):onsample(itrial)+ntarg-1));
    end
    data2=[];
    
    % load filters
    load(sprintf('%sfilt_lcmv%d_%d_%d',DIR.flt,idx,isub,iblock),'filt','filtinfo');
    nvox=size(filt,1);
    
    % project data for all voxels and all trials
    source_raw=NaN(nvox,ntarg,ntrial);
    for k=1:size(data3,1),
      source_raw(:,:,k)=filt*squeeze(data3(k,:,:));
    end
    filt=[];
    
    addpath('D:\CKDATA\MatBox\toolboxes\fieldtrip-20171001\external\spm8')
    
    fprintf('\n Smoothing data in source space: sub %d block %d\n',isub,iblock);
    
    source = zeros(length(roivoxins),ntarg,ntrial);
    tmp = zeros(sourcemodel.dim);
    for itrial = 1:ntrial
      for itime = 1:ntarg
        tmp(sourcemodel.inside)=source_raw(:,itime,itrial);
        tmpsm = volumesmooth(tmp,3,'all');    % takes 432 sec (7.2 min)
        source(:,itime,itrial)=tmpsm(roivoxins);
      end
    end
    % norm2: zscore per channel
    for ivox=1:size(source,1)
      source(ivox,:,:) = zscore(source(ivox,:,:));
    end
    inorm = 33;
    source = single(source);
    save(sprintf('%ssub%d_block%d_targets%d_smooth_norm%d',DIR.tgt,isub,iblock,idx,inorm),'source','ntarg','trialinf');
    fprintf('\n*** Sub %d Block #%d done ***\n\n',isub, iblock);
    toc
    
  end
  
end