%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% takes MEG data and computes the conditional mutual information between
% MEG and broadband envelope with respect to the lip area
%
% this in done only for primary auditory and visual voxels to find whether
% acoustic features are actually tracked or redundant information from
% visual features are encoded
%
% and vice-versa: MI env conditioned with lips and MI lips conditioned with
% env
%
% options are: 
% B1_E: MI(env ; meg | lip)
% C1_E: MI(env ; meg | lip_shuf)
% C2_E: MI(env ; meg_shuf | lip_shuf)
%
% B1_L: MI(lip ; meg | env)
% C1_L: MI(lip ; meg | env_shuf)
% C2_L: MI(lip ; meg_shuf | env_shuf)
%
% do this smart by reusing the shuffled arrays, but consider different
% shuffling when using more than one at once. otherwise the meg and
% conditional variable are not independent!
%
%
% here we load the optimal lag for each stimulus, frequency and
% voxel and compute the CMI lags in a window centered around the optimal
% lag
%
% we therefore have the advantage of computing robust MI by sum over lags,
% while also considering different time lags for stimuli
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clearvars

% directories
COMP = 2; % set environment to local windows machine
Step0_setup;

blocks = [9:12]; % 4 auditory only blocks

poolobj = manageParpool(-2);
numcores = poolobj.NumWorkers;


% condition label for plot header
if blocks(1) == 1
    condlabel = 'auditory';
elseif blocks(1) == 9
    condlabel = 'visual';
end
ARG.condlabel = condlabel;

% directories and save name
snamebase = sprintf('%sCMI_meanlag_%s',DIR.cmi,condlabel);


% data dir
datafolder = dir(sprintf('%s*.mat',DIR.source));

% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);

% parameters --------------------------------------------------------------
ARG.freq_range = [0.5, 1; 1, 3; 2, 4; 3, 6; 4, 8]; % delta, theta and what not...
ARG.srate = 50; % in Hz
ARG.nreps = 30; % N samplings with replacement for bootstrapping
nreps = ARG.nreps;

ARG.OFFSET = round(1.5 * ARG.srate); % 500 ms - cut out evoked response
ARG.MAXL = round(4 * ARG.srate); % max length of 4.5 s (shortest env: 5.52 s)
envlen = ARG.MAXL;

ARG.lagwindow = round([-0.06:0.02:0.06] * ARG.srate);
lagwindow = ARG.lagwindow;
nlags = length(lagwindow);

goodsub = find([SUBS.exclude]==0);
nsubs = length(goodsub);
nfreq = size(ARG.freq_range,1);


% Filter parameters
ARGF.rate = ARG.srate;
ARGF.mode = 'butter';
ARGF.order = 3; % filter order
ARGF.window = 1; % symmetric padding


%% load ROI data ----------------------------------------------------------
% contains intersected voxel for primary cortices in roivoxins
load ROIVOXELS.mat
load pROI_voxel.mat % -> pROI structure
ARG.pROI = pROI;
allvox = pROI.all.inroivoxins;
nvox = numel(allvox);

[~,audvox] = intersect(allvox,[pROI.auditory.left.inroivoxins; pROI.auditory.right.inroivoxins]);
[~,visvox] = intersect(allvox,[pROI.visual.left.inroivoxins; pROI.visual.right.inroivoxins]);
roivox = {audvox, visvox};
nroi = numel(roivox); % auditory and visual cortex

% load voxel-wise optlag data ---------------------------------------------
load Optlags_GLM.mat
ARG.Optlags = Optlags;

ngrouppred = size(Optlags,2);





%% load features ----------------------------------------------------------
% choose groups of predictors
AudFeat = {'broad_env', 'broad_slope', 'pitch'};
VisFeat = {'lip_area', 'lip_slope', 'dist_x'};
% define features to load here
AllLabels = {AudFeat{:}, VisFeat{:}};
ARG.AllLabelsUsed = AllLabels;

ARG.GroupNames = {'AudFeat','VisFeat'};
ARG.AudFeat = AudFeat;
ARG.VisFeat = VisFeat;

% create Features
cfg = [];
cfg.srate = ARG.srate;
AllFeatures = createFeatures_v3(cfg,DIR);


% sort features -----------
Features = sort_features(AllFeatures, AllLabels);
contFeat = Features.contFeat;
discFeat = Features.discFeat;
nenv = sum(Features.NDims);
nmaxcoeff = Features.nmaxcoeff;


% define Predictors ------
FullNames = {AllLabels};
FullPredictors = sort_predictors(Features,FullNames);
fullpredlen = length(FullPredictors);

% needed to assign the optimal lags for the multivariate predictors
PredNames = {AudFeat, VisFeat};
Predictors = sort_predictors(Features,PredNames);
predlen = length(PredNames);


ARG.Predictors = Predictors;
ARG.predlen = predlen;



%% load voxel-wise optlag data
load Optlags_MI.mat % -> OptlagsRoivox 
OptlagsRoivox = round(OptlagsRoivox ./ 1000 * ARG.srate); % convert to samples


% warn if any of the optimal lags in lower than the min of the lagwindow
if any(OptlagsRoivox(:) < min(lagwindow))
    fprintf('Optimal lag is to small for lag window!! \n');
    error('Optimal lag is to small for lag window!! \n');
end



%% main

for isub2 = 1:nsubs
    isub = goodsub(isub2);
    
    % create filename and check if it already exists
    sname = sprintf('%s_sub%02d.mat', snamebase,isub);
    if exist(sname,'file')
        fprintf('file already computed \n');
        continue;
    end
    
    % all blocks structures
    meg = []; env = [];
    
    % for each block, concat all trials and compute MI
    for iblock = blocks
        
        dataname = sprintf('%ssourcespace_sub%d_block%d_A.mat', DIR.source, isub, iblock);
        if ~exist(dataname,'file')
            fprintf('No data file found!! \n');
            continue;
        end
        
        tic
        % load data and sorted envelopes from file
        data = load(dataname);
        fprintf('loading sub %d block %d \n', isub, iblock);
        
        % sort stims and concat trials of each block
        reidx = data.ARG.trialinfo(:,4);
        cond = data.ARG.trialinfo(1,5);
        env = cat(2,env,{Features.ChosenFeat{cond}{reidx}});
        % data
        meg = cat(1,meg,data.source_raw);
    end
    clear data
    
    % STIMULI ----------------------------------
    % cut to same length
    env = cellfun(@(x) x(:,ARG.OFFSET+[1:envlen]),env,'UniformOutput',false);
    env = cat(3,env{:});
    
    % MEG ----------------------------------
    % select only primary ROIs from meg and cut to same length
    meg = meg(:,pROI.all.inroivoxins,ARG.OFFSET+[1:envlen]);
    meg = permute(meg,[2,3,1]);
    [~,meglen,ntrl] = size(meg);

    
    
    
    % filter, hilbert, MI ---------------------------------------------
    % MI [envelopes x frequencies x voxels]
    CMI_B1_E = zeros(nfreq,nvox);
    CMI_C1_E = zeros(nfreq,nvox,nreps);
    CMI_C2_E = zeros(nfreq,nvox,nreps);
    
    CMI_B1_L = zeros(nfreq,nvox);
    CMI_C1_L = zeros(nfreq,nvox,nreps);
    CMI_C2_L = zeros(nfreq,nvox,nreps);
    for freq = 1:nfreq
        tic
        fprintf('filter data into %.2f to %.2f Hz band \n', ARG.freq_range(freq,:));
        
        % FILTER stimuli
        cfg = [];
        cfg.continuous = true;
        cfg.fs = ARGF.rate;
        cfg.filter = true;
        cfg.filtertype = ARGF.mode;
        cfg.filterord = ARGF.order;
        cfg.filterwindow = ARGF.window;
        cfg.hilbert = true;
        cfg.zcopnorm = true;

        % filter and zscore features
        cfg.filterfreq = ARG.freq_range(freq,:);
        stim_filt = fb_preprocess_trials(cfg,env);
    
        % filter meg
        cfg.filterfreq = ARG.freq_range(freq,:);
        brain_filt = fb_preprocess_trials(cfg,meg);
        % split so we already copnorm and we need not copy whole matrix for
        % each worker
        brain_real = brain_filt(1:nvox,:,:);
        brain_imag = brain_filt(nvox+1:end,:,:);
        toc
        
        % concatenate trials
        % get hilbert predictors
        E_indx = Predictors{1} + [0,nenv];
        Audstim = reshape(stim_filt(E_indx(:),:,:),[2*length(Predictors{1}),envlen*ntrl])';
        
        L_indx = Predictors{2} + [0,nenv];
        Visstim = reshape(stim_filt(L_indx(:),:,:),[2*length(Predictors{2}),envlen*ntrl])';
        
        
        fprintf('compute MI over voxels\n');
        tic
        parfor ivox = 1:nvox
            % index MEG ------------
            cutbrain = cat(1,brain_real(ivox,:,:),brain_imag(ivox,:,:)); % choose vox
            M_hilc = reshape(cutbrain,[2,envlen*ntrl])';
            
            % fetch stim, voxel and freq dependent lag
            voxlags = OptlagsRoivox(:,freq,ivox);
            meglag = max(voxlags);
            stimlag = abs(voxlags - max(voxlags));
            
            
            % here we cut stimuli again, because both might have different
            % lags 
            E_hilc = generateLags(Audstim,lagwindow+voxlags(1));
            L_hilc = generateLags(Visstim,lagwindow+voxlags(2));
            colix = [1:nenv:nenv*nlags];
            
            
            % allocate empty lag variables
            B1_E_tmp = zeros(nlags,1);
            C1_E_tmp = zeros(nlags,nreps);
            C2_E_tmp = zeros(nlags,nreps);
            
            B1_L_tmp = zeros(nlags,1);
            C1_L_tmp = zeros(nlags,nreps);
            C2_L_tmp = zeros(nlags,nreps);
            
            for lag = 1:nlags
                icol = [0:nenv-1] + colix(lag);

                % real CMI and randomized Z with loop inside function
                % B1_E: MI(env ; meg | lip)     and     C1_E: MI(env ; meg | lip_shuf)
                [B1_E_tmp(lag), C1_E_tmp(lag,:)] = ckcmi_gggSZ_R(E_hilc(:,icol), M_hilc, L_hilc(:,icol), nreps);

                % B1_L: MI(lip ; meg | env)     and     C1_L: MI(lip ; meg | env_shuf)
                [B1_L_tmp(lag), C1_L_tmp(lag,:)] = ckcmi_gggSZ_R(L_hilc(:,icol), M_hilc, E_hilc(:,icol), nreps);



                % C2_E: MI(env ; meg_shuf | lip_shuf)
                [~, C2_E_tmp(lag,:)] = ckcmi_gggSYSZ_R(E_hilc(:,icol), M_hilc, L_hilc(:,icol), nreps);

                % C2_L: MI(lip ; meg_shuf | env_shuf)
                [~, C2_L_tmp(lag,:)] = ckcmi_gggSYSZ_R(L_hilc(:,icol), M_hilc, E_hilc(:,icol), nreps);
            end
            % index to voxels
            CMI_B1_E_tmp(:,ivox) = B1_E_tmp;
            CMI_C1_E_tmp(:,ivox,:) = C1_E_tmp;
            CMI_C2_E_tmp(:,ivox,:) = C2_E_tmp;
            
            CMI_B1_L_tmp(:,ivox) = B1_L_tmp;
            CMI_C1_L_tmp(:,ivox,:) = C1_L_tmp;
            CMI_C2_L_tmp(:,ivox,:) = C2_L_tmp;

        end
        % sum over lags
        CMI_B1_E(freq,:) = sum(CMI_B1_E_tmp,1);
        CMI_C1_E(freq,:,:) = sum(CMI_C1_E_tmp,1);
        CMI_C2_E(freq,:,:) = sum(CMI_C2_E_tmp,1);
        
        CMI_B1_L(freq,:) = sum(CMI_B1_L_tmp,1);
        CMI_C1_L(freq,:,:) = sum(CMI_C1_L_tmp,1);
        CMI_C2_L(freq,:,:) = sum(CMI_C2_L_tmp,1);
        toc
    end
    % flush
    clear meg, clear env, clear brain, clear stim
    

    % save data to disk
    % sname as above
    save(sname, 'CMI_B1_E', 'CMI_C1_E', 'CMI_C2_E', 'CMI_B1_L', 'CMI_C1_L', 'CMI_C2_L', 'ARG', 'ARGF');
    toc
end



% send message from server to telegram
if COMP == 2
    bot = returnBot('Hanzule');
    [~,sysname] = system('hostname');
    messagetext = sprintf('%s has finished on %s',mfilename, sysname);
    tgprintf(bot,messagetext);
end


fprintf('\n');
fprintf('Everything has finished successfully! \n');

