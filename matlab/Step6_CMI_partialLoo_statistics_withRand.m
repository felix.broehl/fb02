%
% plot data from StepX_CMI_partialSingle
% 
% single non-presented features partialized with all three features of the
% presented modality
% 
% e.g. in V-only trials, each single auditory features partialized with all
% the visual features (lip area, dist x, dist y)
%

close all
clearvars

COMP = 2;
Step0_setup;

% get subject data
expname = 'AVentrain';
[SUBS, strcond, filenames] = subject_database(expname);
goodsub = find([SUBS.exclude]==0);
nsub = length(goodsub);

% directories and save name
snamebase = sprintf('%sCMI_partialLoo',DIR.cmi);
conditions = {'auditory','visual'};


nfreq = 5;
nvar = 3; % variants of CMI measures

blocks = [1:4; 9:12];
ncond = size(blocks,1);



%% get sourceatlas
load sourceatlas_grid.mat
load('ROIVOXELS.mat','roivoxins');
load pROI_voxel.mat
nvox = length(pROI.all.inroivoxins);

% get template MRI
mri = ft_read_mri(templateT1);
mri = ft_convert_units(mri, 'cm');


%% roi voxel

% unpack roi voxel indices
leftA = pROI.auditory.left.inroivoxins;
rightA = pROI.auditory.right.inroivoxins;
leftV = pROI.visual.left.inroivoxins;
rightV = pROI.visual.right.inroivoxins;
% intersect with ROI voxel mat
[~, leftA] = intersect(pROI.all.inroivoxins,leftA);
[~, rightA] = intersect(pROI.all.inroivoxins,rightA);
[~, leftV] = intersect(pROI.all.inroivoxins,leftV);
[~, rightV] = intersect(pROI.all.inroivoxins,rightV);

audvox = [leftA; rightA];
visvox = [leftV; rightV];
allvox = {audvox,visvox};

% sizes
nhemi = 2;
nroi = 2;


%% pre define effects we want to look at closer
Effects.cond = [2,2,2,2];
Effects.freq = [1,2,1,2];
Effects.iroi = [1,1,2,2];
Effects.rois = {audvox,audvox,visvox,visvox};
Effects.neffects = numel(Effects.cond);



%% load data
filename = sprintf('%s_visual_sub01_rand.mat',snamebase);
data = load(filename,'ARG','ARGF');
freqrange = data.ARG.freq_range;
nmods = data.ARG.predlen;
nrand = data.ARG.nrand;


CMI = zeros(ncond,nsub,nmods,nfreq,nvox);
CMIboot = zeros(ncond,nsub,nmods,nfreq,nvox,nrand);

for icond = 1:ncond
    for isub2 = 1:length(goodsub)
        isub = goodsub(isub2);
        
        condlabel = conditions{icond};
        filename = sprintf('%s_%s_sub%02d_rand.mat', snamebase,condlabel,isub);
        data = load(filename,'ARG','ARGF','CMI','CMIboot');
        
        % CMI data
        CMI(icond,isub2,:,:,:) = data.CMI;
        CMIboot(icond,isub2,:,:,:,:) = data.CMIboot;
    end
end




%% prepare data to save for final figure

AudFeat = {'aud_env','aud_slope','aud_pitch'};
VisFeat = {'lip_area','lip_slope','lip_width'};
featNames = {AudFeat, VisFeat};
predidx = -Effects.cond + 3; % invert
Effects.featLabels = featNames(predidx);
Effects.mcomp = cell(1,Effects.neffects);
Effects.freqrange = freqrange;
Effects.roinames = {'Auditory','Visual'};
Effects.condition = {'A-only','V-only'};

for ieff = 1:Effects.neffects
    icond = Effects.cond(ieff);
    roivox = Effects.rois{ieff};
    freq = Effects.freq(ieff);
    midata = sq(mean(CMI(icond,:,:,freq,roivox),5));
    
    % take randomized data and create a single baseline dist. per effect
    % average across roi, median across participants
    miboot = sq(median(mean(CMIboot(icond,:,:,freq,roivox,:),5),2));
    miboot = max(miboot,[],1)';
    bootprc = prctile(miboot,99);
    % compute random test p-value
    pval = nansum(miboot > median(midata,1)) ./ nrand;
    pval(pval==0) = 1/nrand;
    
    
    % do anova and post-hoc test if needed
    [p,anovatab,stats] = kruskalwallis(midata,[],'off');
    if p <= 0.01
        mcomp = multcompare(stats,'Display','off');
        Effects.mcomp{ieff} = mcomp;
    end
    
    % compute effect size as epsilon squared for non-parametric test
    H = anovatab{2,5}; 
    k = anovatab{2,3} + 1;
    n = anovatab{4,3} + 1;
    epssq = (H - k + 1)/(n - k);
    
    % save stats
    Effects.data{ieff} = midata;
    Effects.bootprc{ieff} = bootprc;
    Effects.P{ieff} = p;
    Effects.Pboot{ieff} = pval;
    Effects.Chisq{ieff} = H;
    Effects.epsilonsq{ieff} = epssq;
    Effects.Anovatab{ieff} = anovatab;
    Effects.Stats{ieff} = stats;
end

% save to file
sname = sprintf('../finalfigures/Data_PartialLoo_analysis');
save(sname,'Effects');


return




%% analyse neighboring freq bands for AudFeat v-only v-cortex

neighcond = 2;
neighfreq = [1,2];
neighroi = visvox;

fprintf('compute neighboring bands ANOVA \n');
for ifreq = neighfreq
    midata = sq(mean(CMI(neighcond,:,:,ifreq,neighroi),5));
    
    % anova and tukey-kramer
    [p,anovatab,stats] = kruskalwallis(midata,[],'off');
    if p <= 0.01
        mcomp = multcompare(stats,'Display','off');
    end
    
    % print statstics to command line
    fprintf('%.1g - %.1g Hz band: \n',freqrange(ifreq,:));
    disp(anovatab);
    etasq = anovatab{2,2}/anovatab{end,2};
    fprintf('eta squared: %.3f \n',etasq);
    if p <= 0.05
        fprintf('tukey-kramer: \n');
        disp(mcomp);
    end
    fprintf('\n');
    
end
