%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load all stimuli, resamples if necessary and saves as one file.
% can be used to compute MI in MEG trials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clearvars

DIR.main = 'C:\Users\fbroehl\Documents\FB02\StimInfo\';
DIR.env = 'C:\Users\fbroehl\Documents\FB02\StimInfo\processed\';
DIR.stimdir = [DIR.main 'processed\'];
DIR.pitchdir = [DIR.main 'pitch_data\'];
DIR.formantdir = [DIR.main 'formant_data\'];

% for correct order of stim file loading
Nambase{1} = 'adj';
Nambase{2} = 'num';
Nambase{3} = 'rand';

ncon = 2;
nfiles = 90;
nfeats = 13;

% parameters
ARG.StimOrder{1,1} = 'pitch';
ARG.StimOrder{2,1} = 'f1';
ARG.StimOrder{3,1} = 'f2';
ARG.StimOrder{4,1} = 'f3';
ARG.StimOrder{5,1} = 'f4';
ARG.StimOrder{6,1} = 'f5';
ARG.StimOrder{7,1} = 'Ellipse_x';
ARG.StimOrder{8,1} = 'Ellipse_y';
ARG.StimOrder{9,1} = 'area';
ARG.StimOrder{10,1} = 'dist_x';
ARG.StimOrder{11,1} = 'dist_y';
ARG.StimOrder{12,1} = 'cluster';
ARG.StimOrder{13,1} = 'FrameDist';
ARG.Nambase = Nambase;
ARG.pitchThrs = [20, 250]; % in Hz
ARG.fs = 50;
pitch_fs = 500; % 0.002 in praat script

% set bandwidth for pitch (i looked at the data)
pitchTHRS = ARG.pitchThrs;


%% main
% load visual stim data once at the beginning
envname = sprintf('%sSpeechEnvelopes_3_%dfs.mat',DIR.env,ARG.fs);
Envelopes = load(envname);

framename = sprintf('%sFaceFrameDistance.mat',DIR.env);
FrameDist = load(framename);
    
    
Stimuli = [];
minlen = zeros(3,nfiles,ncon);

for c = 1:ncon % only adj and num
    % load lip track data ----
    filename = sprintf('%s/%s_LipTrack.mat',DIR.stimdir,Nambase{c});
    lipsdata = load(filename, 'TRACKING');
    
    
    for i = 1:nfiles % 90 files per condition
        % verbose 
        if mod(i,5) == 0
            fprintf('.');
        end
        
        % get envelope length to resample other features to same length
        maxlen = length(Envelopes.SpeechEnvelopes.Mean{c}{i});
        
        % read pitch data ----
        filename = dir(sprintf('%s/%s_%d_*.Pitch',DIR.pitchdir,Nambase{c},i));
        filename = [filename.folder '/' filename.name];
        pt = pitchRead(filename);
        for k = 1:pt.nx
            pitch(k) = pt.frame{k}.frequency(1);
        end
        % sample to same length as corresponding envelope !
        pitch = resample(pitch,maxlen,length(pitch));
        % find voiced/unvoiced segments
        voiced = find(pitch>pitchTHRS(1) & pitch<pitchTHRS(2));
        unvoiced = find(pitch<pitchTHRS(1) | pitch>pitchTHRS(2));
        pitch(unvoiced) = 0;
        VoicedSeg{c}{i} = voiced;
        
        % read formants data ----
        filename = dir(sprintf('%s/%s_%d_*.Formant',DIR.formantdir,Nambase{c},i));
        filename = [filename.folder '/' filename.name];
        ft = formantRead(filename);
        formants = zeros(ft.maxnFormants,ft.nx);
        for k = 1:ft.nx
            tmp = ft.frame{k}.frequency;
            nf = ft.frame{k}.nFormants;
            formants(1:nf,k) = tmp;
        end
        formants = resample(formants',maxlen,length(formants))';
        formants(:,unvoiced) = 0;
        
        % process lip track data ----
        mouth = lipsdata.TRACKING{i}.Mouth;
        mouth(isnan(mouth)) = 0; % delete NaN values
        mouth = resample(mouth,maxlen,length(mouth))';
        
        % process Frame Distance measure ----
        frdist = FrameDist.Distances{c}{i};
        frdist = resample(frdist,maxlen,length(frdist))';
        
        % book keeping min vector length
        minlen(1,i,c) = length(pitch);
        minlen(2,i,c) = length(formants);
        minlen(3,i,c) = length(mouth);
        L = [1:min(minlen(:,i,c))];
        
        % final assignment
        Stimuli{c}{i} = [pitch(:,L); formants(:,L); mouth(:,L); frdist];
    end
    fprintf('\n');
end


% save to file ------------------
sname = sprintf('%sStimMat_%d_fs.mat',DIR.stimdir,ARG.fs)
save(sname,'ARG','Stimuli','VoicedSeg');
