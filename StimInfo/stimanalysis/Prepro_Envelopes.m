%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute speech envelope following Gross/Ghazanfar 
% Use 8 bands, save each band's and overall envelope at fs_new Hz
% 
% saves all in one file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clearvars;

COMP = 2;
addpath('C:\Users\fbroehl\Documents\FB02\matlab');
Step0_setup; % create directories

%AVPATH = 'b:/AVstimuli/';
%chimpath = [AVPATH 'stimuli/stimanalysis/envelope/chimera'];
%addpath(chimpath)

Stimdir = [DIR.main, 'StimInfo\matsounds\'];
savedir = [DIR.main, 'StimInfo\processed\'];


Nambase{1} = 'adj';
Nambase{2} = 'num';
Nambase{3} = 'rand';

% take 9 envelopes and compute the total envelope
% also take every 3 envelopes and average them to low, mid and high env
fs_new = 50;
fs = 22050;
nbands = 12;
fco = equal_xbm_bands(100,10000,nbands); % N bands between 100hz and 8kHz

%---------------------------------------------------
% compute filters
for k = 1:length(fco)-1
  % filter
  [b,a] = butter(3,2*[fco(k) fco(k+1)]/fs);
  Filt{k}.a = a;
  Filt{k}.b = b;
  
end
%---------------------------------------------------------------- 
% loop sounds

for base = 1:3
  for file = 1:90
      filename = sprintf('%s%s_%d.mat', Stimdir, Nambase{base}, file);
      fprintf('%s \n',filename);
      X = load(filename);
      if X.Fs~=fs
          error('sample rate mismatch');
          return;
      end
    
      % 1D
      Sound = mean(X.sound,2); % take channel average
      % get size
      L = length(resample(Sound,fs_new,X.Fs));
      Envelopes = zeros(L,length(fco)-1);
      for k = 1:length(fco)-1
          % filter
          tmp = filtfilt(Filt{k}.b,Filt{k}.a,Sound);
          % envelope
          tmp2 = abs(hilbert(tmp));
          % resample
          tmp2 = resample(tmp2,fs_new,X.Fs);
          Envelopes(:,k) = tmp2;
      end
    
      % some baseline adjustment for background hiss
      Envelopes = Envelopes-repmat(mean(Envelopes([1:180],:),1),[size(Envelopes,1),1]);
      
      
      SpeechEnvelopes.Mean{base}{file} = mean(Envelopes,2)';
      
      % if divisible by 3, average into low, mid and high env
      % otherwise leave as is
      if mod(nbands,3) == 0
          % index
          step = nbands / 3;
          step = [1:step:nbands];
          % define bands
          envlow = mean(Envelopes(:,1:step(2)-1),2);
          envmid = mean(Envelopes(:,step(2):step(3)-1),2);
          envhigh = mean(Envelopes(:,step(3):nbands),2);
          % assign
          SpeechEnvelopes.Each{base}{file} = cat(2,envlow, envmid, envhigh)';
      else
          SpeechEnvelopes.Each{base}{file} = Envelopes';
      end
      
  end
end

% sname = sprintf('%sSpeechEnvelopes.mat',Stimdir)
sname = sprintf('%sSpeechEnvelopes_%d_%dfs.mat', savedir, nbands, fs_new)
save(sname,'Nambase','SpeechEnvelopes');

    
  