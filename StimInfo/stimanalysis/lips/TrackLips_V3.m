%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tracking of lips and opening.
% First run Find_Mouth.m !
%
% mouthopening is defined and fit with an ellipes
%
%  TRACKING{n}.Mouth = MouthTrack;        
%  with [ELLIPSE-X ELLIPSE-Y sum(mouth(:)) area distance_x distance_y  # cluster];
%  # cluster is perhaps a quality measure. If there are > 1 cluster, the algorithm may have problems
%

clear; close all;

base = 3 % 1/2 for adj or num

DISP =1; % do display
%-----------------------------------------------------------------------------
AVPATH = 'Z:/old/Keitel_Glasgow_speech/StimInfo';
AVPATH = 'I:/Keitel/Stimuli';
Stimdir = [AVPATH '/cut/'];
Savedir = [AVPATH '/processed/'];
Nambase{1} = 'adj';
Nambase{2} = 'num';
Nambase{3} = 'rand';

savename = sprintf('%s%s_Mouth_box.mat',Savedir,Nambase{base});
load(savename,'Boundix_box_mouth');

%------------------------------------------
% paramters

% how much the mouth opening should be darker than the lips
MOUTH_DARK = 25; % 30
MOUTH_HSV= 0.45; % 0.46

% min areas for pixel clusters
MIN_AREA_MOUTH = 30;
% intensity range of lips around median value
MIN_CLUSTER_MOUTH_RATIO = 0.15;  % fraction of max  cluster to remove
MIN_CLUSTER_MOUTH_left = 180;
CLUSTER_FROM_BORDER = 22;

%------------------------------------------------------------------
% find video names
for n=1:90
  close all
  if DISP
    f2 = ckfigure( [  -1709          68        1678        1054]);
  end
  cf=1;
  MouthTrack=[];
  LipTrack=[];
  %--------------------------------------------------
  % load video
  
  Names = dir(sprintf('%s%s_%d_*.avi',Stimdir,Nambase{base},n));
  filename = sprintf('%s%s',Stimdir,Names(1).name)
  obj = VideoReader(filename);
  nframes = obj.NumberOfFrames;
  % read video frames
  vidFrames = read(obj,[1 nframes]);
  %----------------------------------------------------
  % cut area around face/mouth
  vidFrames = vidFrames([300:700],[400:900],:,:);
  Gray=zeros(401,501,nframes);
  HSV =zeros(401,501,nframes);
  for f=1:nframes
    I = vidFrames(:,:,:,f);
    % convert images to HSV and grayscale
    tmp = double(rgb2hsv(I));;
    HSV(:,:,f)= tmp(:,:,3);
    Gray(:,:,f)= double(rgb2gray(I));
  end
  clear vidFrames
  % reduce to mouth px
  %----------------------------------------------------
  HSV = HSV(Boundix_box_mouth{n}{1},Boundix_box_mouth{n}{2},:);
  Gray = Gray(Boundix_box_mouth{n}{1},Boundix_box_mouth{n}{2},:);
  
  %--------------------------------------------------------------
  % estimate values of lips from fist / last image in Grayscale
  lip_gray = Gray([-10:10]+58,[-20:20]+95,nframes+[-10:0]);
  LIP_GRAY = median(lip_gray(:));
  % worst case default 
  if abs(LIP_GRAY-110)>10
    LIP_GRAY = 110;
  end
  
  RANGE_MOUTH = LIP_GRAY-MOUTH_DARK;
  mouth_hsv = sq(HSV([-20:20]+60,[-40:40]+95,:));
  mouth_gray = sq(Gray([-20:20]+60,[-40:40]+95,:));
  
  %------------------------------------------------------------------------------------------
  % process single frames
  %------------------------------------------------------------------------------------------
  box_x = [8:size(Gray,2)-8];
  
  MouthTrack=zeros(nframes,6);
  for f=1:nframes
    % track mouth opening based on hray value
    tmp = Gray(:,box_x,f);
    mouth = double( (tmp<(RANGE_MOUTH)));
   %mouth = imclearborder(mouth,8);
    % fill holes
    mouth = imfill(mouth,'holes');
    
    % based on HSV
    mouth2 = double(HSV(:,box_x,f)<MOUTH_HSV);
   %  mouth2 = imclearborder(mouth2,8);
    % fill holes
    mouth2 = imfill(mouth2,'holes');
    % combine
    mouth = double( (mouth>0)+(mouth2>0));
    
    % remove smaller clusters
    CC = bwconncomp(mouth, 8);
    S = regionprops(CC, 'Area');
    L = labelmatrix(CC);
    %-----------------------------------------------------
    % fine tuning
    % if there are multiple, and if one is much larger than others, remove small ones
    if length(S)>1
      % compute size ratiosn
      Rel_size = [S.Area]/max([S.Area]);
      mouth = ismember(L, find(Rel_size >= MIN_CLUSTER_MOUTH_RATIO));
    end
    % remove clusters too close to border
    CC = bwconncomp(mouth, 8);
    S = regionprops(CC, 'Area');
    L = labelmatrix(CC);
    Keep = ones(1,length(S));
    if length(S)>1
      for l=1:length(S)
        [y,x] = find(L==(l));
        y = median(y); x = median(x);
        dist =   [min(y,size(mouth,1)-y),min(x,size(mouth,2))];
        if min(dist)<CLUSTER_FROM_BORDER 
          Keep((l)) = 0;
        end
      end
    end
    mouth = ismember(L, find(Keep));
    % remove notorious left / right clusters near lips
    [y,x] = find(mouth);
    Med_pos = [median(y),median(x)];
    
    CC = bwconncomp(mouth, 8);
    S = regionprops(CC, 'Area');
    L = labelmatrix(CC);
    Keep = ones(1,length(S));
    if length(S)>1
      ji = find([S.Area]<MIN_CLUSTER_MOUTH_left);
      for l=1:length(ji)
        % check whetehr small cluster is left of median position
        [y,x] = find(L==ji(l));
        if (median(y)-Med_pos(1))<20 && (median(x)>(Med_pos(2)-20))
          % remove this id
          Keep(ji(l)) = 0;
        end
        if (median(y)-Med_pos(1))<20 && (median(x)>(Med_pos(2)+20))
          % remove this id
          Keep(ji(l)) = 0;
        end
      end
    end
    mouth = ismember(L, find(Keep));
    CC = bwconncomp(mouth, 8);
    S = regionprops(CC, 'Area');
    
    [y,x] = find(mouth);
    diam_x = prctile(x,95)-prctile(x,5);
    diam_y = prctile(y,95)-prctile(y,5);

    % fit ellipse
    [~, threshold] = edge(mouth, 'sobel');
    fudgeFactor = .2;
    M2 = edge(mouth,'sobel', threshold * fudgeFactor);
    [y,x] = find(M2);
    warning off
    test = fit_ellipse(x,y);
    warning on
    elM=[];
    a =0; b=0;
    if ~isempty(test)
      if ~isempty(test.a)
        ELLIPSE_MOUTH= test;
        elM = calculateEllipse(ELLIPSE_MOUTH.X0_in, ELLIPSE_MOUTH.Y0_in, ELLIPSE_MOUTH.a, ELLIPSE_MOUTH.b, rad2deg(ELLIPSE_MOUTH.phi));
        a = ELLIPSE_MOUTH.a;
        b = ELLIPSE_MOUTH.b;
      end
    end
    % [Ellipse_X Ellipse_y, Area, diam-x, diam-y]
    MouthTrack(f,:) = [a b sum(mouth(:)) diam_x diam_y length([S.Area])];
    
    if DISP && rem(f,2)==0 && cf<=90
      cksubplot(9,10,cf); colormap gray
      imagesc(Gray(:,box_x,f));
      hold on
      if sum(mouth(:))
        contour(mouth,1,'Color','w','LineWidth',1); % detected mouth opening
      end
      if ~isempty(elM)
        plot(elM(:,1), elM(:,2), 'g','LineWidth',1) % ellpise
      end
      axis off;
      if length([S.Area])>1
        title(f,'Color','r')
      else
        title(f)
      end
      cf=cf+1;
      drawnow
      
    end
  end % frames
  
  
  xdir = 'I:/Keitel/Stimuli/test';
  snamef = sprintf('%s/trackLip_%d_%d.png',xdir,base,n);
  drawnow
  set(gcf,'PaperPositionMode','auto')
  set(gcf,'PaperOrientation','portrait');
  print('-dpng',snamef);
  
 
  
  TRACKING{n}.Mouth = MouthTrack;
  

end

ARG.MOUTH_DARK =MOUTH_DARK;
ARG.MIN_CLUSTER_MOUTH_RATIO =MIN_CLUSTER_MOUTH_RATIO;
ARG.MIN_CLUSTER_MOUTH_left =MIN_CLUSTER_MOUTH_left;
ARG.MOUTH_HSV = MOUTH_HSV;


savename = sprintf('%s%s_LipTrack.mat',Savedir,Nambase{base})
save(savename,'TRACKING','ARG')


return;


%--------------------------------------------------------------------------
clear
base = 3  % 1/2 for adj or num

AVPATH = 'Z:/old/Keitel_Glasgow_speech/StimInfo';
AVPATH = 'I:/Keitel/Stimuli';
Stimdir = [AVPATH '/cut/'];
Savedir = [AVPATH '/processed/'];
Nambase{1} = 'adj';
Nambase{2} = 'num';
Nambase{3} = 'rand';

savename = sprintf('%s%s_LipTrack.mat',Savedir,Nambase{base})
load(savename,'TRACKING','ARG')

clear cc
for e=1:2
  for s=1:90
    a  = TRACKING{s}.Mouth(:,[e]);
    cc(s,e) = corr(a,medfilt1(a,3))
  end
end



%--------------------------------------------------------------------------
clear

% fix indiv. errors
base = 3  % 1/2 for adj or num

AVPATH = 'Z:/old/Keitel_Glasgow_speech/StimInfo';
AVPATH = 'I:/Keitel/Stimuli';
Stimdir = [AVPATH '/cut/'];
Savedir = [AVPATH '/processed/'];
Nambase{1} = 'adj';
Nambase{2} = 'num';
Nambase{3} = 'rand';

savename = sprintf('%s%s_LipTrack.mat',Savedir,Nambase{base})
load(savename,'TRACKING','ARG')
TRACKING{24}.Mouth(2,[1])=40;
save(savename,'TRACKING','ARG')





