function f = ckfigure(POS,TITLE,titlecolor,titlesize)

% function fig = ckfigure(POS,TITLE,titlecolor,titlesize)
%
% general figure function. Creates a new figure window at given position
% with given title. Sets the basic parameters for the figure.
% all figures used routinely should be initalized with this function.

if nargin < 4,
  titlesize = 9; 
end;
if nargin < 3,
  titlesize = 9;
  titlecolor = 'k';
end;
if nargin < 2,
	TITLE = '';
end;
f = figure('position',POS);
set(gcf,'DefaultAxesBox',		'off')
set(gcf,'DefaultAxesfontsize',	8);
set(gcf, 'DefaultAxesFontName', 'Arial');
set(gcf,'InvertHardCopy',		'off');

set(gcf,'PaperUnits',			'centimeters');
set(gcf,'PaperSize',[29.68 20.98])
set(gcf,'PaperType','A4');
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperOrientation','landscape');
set(gcf,'PaperUnits','normalized');
set(gcf,'PaperPosition', [0 0 1 1]);
set(gcf,'Renderer','painters');

set(gcf,'BackingStore','on','DoubleBuffer','on');
set(gcf,'Color',[1,1,1]);

ckfiguretitle(TITLE,titlecolor,titlesize)



return;

