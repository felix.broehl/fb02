%
% characterize lip movement by the distance between any two adjacent frames
% of the recorded speakers face are and compare to lip area, dist_x/dist_y
% features
%

close all
clearvars

base = [1,2] % 1/2 for adj or num
basename = {'adj','num'};

DISP = false; % do display

%-----------------------------------------------------------------------------
AVPATH = 'C:\Users\fbroehl\Documents\FB02\StimInfo';
%AVPATH = 'I:/Keitel/Stimuli';
Stimdir = [AVPATH '/cut/'];
Savedir = [AVPATH '/processed/'];
Nambase{1} = 'adj';
Nambase{2} = 'num';
Nambase{3} = 'rand';

% save name
sname = sprintf('%sFaceFrameDistance',Savedir);


%------------------------------------------
% paramters
srate = 25; % fps
mouthbox_y = [410:510];
mouthbox_x = [540:750];
boxcenter = round([length(mouthbox_x)/2, length(mouthbox_y)/2]);

% peripheral blur filter
periblur = true;
sigma = 16;
filtstrength = 50;
gaussweights = fspecial('gaussian', [length(mouthbox_y), length(mouthbox_x)], filtstrength);
gaussweights = (gaussweights - min(gaussweights(:))) ./ (max(gaussweights(:)) - min(gaussweights(:)));

ifedge = true; % if edge detection should be applied before distance
emethod = 'Canny';
threshold = 0.3;
cannysigma = 0.01;


%------------------------------------------------------------------
%% find videos and load
irand = randi([1,90],1,1)
Distances = cell(2,1);

for ibase = base
    for n = 1:90 % irand
        close all

        cf = 1;
        MouthTrack=[];
        LipTrack=[];

        % load video
        Names = dir(sprintf('%s%s_%d_*.avi',Stimdir,Nambase{ibase},n));
        filename = sprintf('%s%s',Stimdir,Names(1).name)
        obj = VideoReader(filename);
        nframes = obj.NumberOfFrames;
        % read video frames
        vidFrames = read(obj,[1 nframes]);



        % cut area around face/mouth
        vidFrames = vidFrames(mouthbox_y,mouthbox_x,:,:);
        % convert to bw image
        bwFrames = sq(mean(vidFrames,3));

        for iframe = 1:nframes
            % peripheral blur
            if periblur
                Iblurred = imgaussfilt(sq(bwFrames(:,:,iframe)),sigma);
                bwFrames(:,:,iframe) = bwFrames(:,:,iframe).*gaussweights + Iblurred.*(1-gaussweights);
            end

            % edge detection
            if ifedge
                bwFrames(:,:,iframe) = edge(sq(bwFrames(:,:,iframe)), emethod, threshold);
            end
        end

        
        if DISP
            figure
            colormap gray
            ratio = [length(mouthbox_x), length(mouthbox_y), 1];
            for iframe = 1:nframes
                subplot(211);
                imagesc(sq(mean(vidFrames(:,:,:,iframe),3)));
                pbaspect(ratio);
                axis off
                
                subplot(212);
                imagesc(sq(bwFrames(:,:,iframe)));
                pbaspect(ratio);
                axis off
                pause(1/srate);
            end
        end

        % flatten frames, compute distances
        bwFrames = reshape(bwFrames,[length(mouthbox_x)*length(mouthbox_y),nframes])';
        D = adjdist(bwFrames,'euclidean');
        Distances{ibase}{n} = D;


    end
end

fprintf('done with dist measure \n');

% save to file
save(sname, 'basename','Distances');




%% analyze metrics
PSpect = cell(2,1);
for ibase = base
    for i = 1:length(Distances{ibase})
        tmp = Distances{ibase}{i};

        % subtract mean, normalize
        tmp = tmp-mean(tmp,1);
        tmp = tmp./std(tmp,[],1);

        [pxx,freq] = pwelch(tmp,25,12,200,srate);
        PSpect{ibase}(i,:) = pxx;
    end
end


figure
for ibase = base
    subplot(2,1,ibase);
    plot(freq,log(PSpect{ibase}'),'-','Color',[0,0,0,0.2]);
end




%%
function D = adjdist(X,distance)
%
% takes the mean of the distances between every element Xn in X and its neighbors Xn-1
% and Xn+1
%

% handle input
if ~ismatrix(X)
    error('input must be 2d vector');
end

% check distance method
if strcmp(distance,'euclidean')
    distfunc = @(a,b) sqrt(sum((a-b).^2));
elseif strcmp(distance,'pearson')
    distfunc = @(a,b) corr(a,b);
else
    error('unknwon method');
end

% get input size
[nrows, ncols] = size(X);

% allocate space
D = zeros(nrows,1);

% create empty row at start and end
emptyrow = zeros(1,ncols);
X = [emptyrow; X; emptyrow];

for i = 1:nrows
    index = i + 1;
    leftdist = distfunc(X(index-1,:),X(index,:));
    rightdist = distfunc(X(index,:),X(index+1,:));
    D(i) = mean([leftdist rightdist]);
end

end

